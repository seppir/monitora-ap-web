package br.ufsc.monitoraap.exception;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(value = { "stackTrace", "cause", "suppressed", "localizedMessage" })
public class ValidationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	protected ValidationException(final String message) {
		super(message);
	}

}
