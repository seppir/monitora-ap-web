package br.ufsc.monitoraap.exception.impl;

import br.ufsc.monitoraap.exception.ValidationException;

public class FacebookJaVinculadoException extends ValidationException {

	private static final long serialVersionUID = 1L;

	public FacebookJaVinculadoException() {
		super("Esta conta Facebook já está vinculada a um usuário!");
	}

}
