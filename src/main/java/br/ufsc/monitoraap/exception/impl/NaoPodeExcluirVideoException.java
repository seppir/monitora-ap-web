package br.ufsc.monitoraap.exception.impl;

import br.ufsc.monitoraap.exception.ValidationException;

public class NaoPodeExcluirVideoException extends ValidationException {

	private static final long serialVersionUID = 1L;

	public NaoPodeExcluirVideoException(final Integer id, final String caminho) {
		super(String.format("Problema ao excluir o vídeo [%d] localizado em [%s]", id, caminho));
	}

}
