package br.ufsc.monitoraap.exception.impl;

import br.ufsc.monitoraap.exception.ValidationException;

public class SenhaAtualIncorretaException extends ValidationException {

	private static final long serialVersionUID = 7277871856255823593L;

	public SenhaAtualIncorretaException() {
		super("A senha informada não combina com a senha atual!");
	}

}
