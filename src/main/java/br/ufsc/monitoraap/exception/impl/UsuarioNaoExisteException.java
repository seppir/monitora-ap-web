package br.ufsc.monitoraap.exception.impl;

import br.ufsc.monitoraap.exception.ValidationException;

public class UsuarioNaoExisteException extends ValidationException {

	private static final long serialVersionUID = 1L;

	public UsuarioNaoExisteException() {
		super("Usuário não existe!");
	}

}
