package br.ufsc.monitoraap.exception.impl;

import br.ufsc.monitoraap.exception.ValidationException;
import br.ufsc.monitoraap.model.Video;

public class NaoPodeProcessarVideoException extends ValidationException {

	private static final long serialVersionUID = 1L;

	private final Video video;

	private final Exception exception;

	public NaoPodeProcessarVideoException(final Video video, final Exception exception) {
		super("Problema ao processar vídeo!");
		this.video = video;
		this.exception = exception;
	}

	public Video getVideo() {
		return video;
	}

	public Exception getException() {
		return exception;
	}

}
