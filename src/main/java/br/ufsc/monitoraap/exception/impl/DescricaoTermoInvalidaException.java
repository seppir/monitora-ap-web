package br.ufsc.monitoraap.exception.impl;

import br.ufsc.monitoraap.exception.ValidationException;

public class DescricaoTermoInvalidaException extends ValidationException {

	private static final long serialVersionUID = 1L;

	public DescricaoTermoInvalidaException() {
		super("A descrição definida para o termo é inválida!");
	}

}
