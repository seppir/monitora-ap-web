package br.ufsc.monitoraap.exception.impl;

import br.ufsc.monitoraap.exception.ValidationException;

public class EntidadeNaoEncontradaException extends ValidationException {

	private static final long serialVersionUID = 1L;

	public EntidadeNaoEncontradaException(final Class<?> entidade, final Object id) {
		super(String.format("Entidade não encontrada: %s [%s]", entidade.getSimpleName(), id));
	}

}
