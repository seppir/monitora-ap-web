package br.ufsc.monitoraap.exception.impl;

import br.ufsc.monitoraap.exception.ValidationException;

public class CpfInvalidoException extends ValidationException {

	private static final long serialVersionUID = 1L;

	public CpfInvalidoException() {
		super("CPF inválido!");
	}

}
