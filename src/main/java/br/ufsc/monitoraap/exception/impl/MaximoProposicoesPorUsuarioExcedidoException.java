package br.ufsc.monitoraap.exception.impl;

import br.ufsc.monitoraap.exception.ValidationException;

public class MaximoProposicoesPorUsuarioExcedidoException extends ValidationException {

	private static final long serialVersionUID = 2928367326866054457L;

	public MaximoProposicoesPorUsuarioExcedidoException() {
		super("Número máximo de proposições excedido!");
	}

}
