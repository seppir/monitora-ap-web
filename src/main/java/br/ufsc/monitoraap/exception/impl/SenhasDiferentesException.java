package br.ufsc.monitoraap.exception.impl;

import br.ufsc.monitoraap.exception.ValidationException;

public class SenhasDiferentesException extends ValidationException {

	public SenhasDiferentesException() {
		super("As senhas informadas não são iguais!");
	}

	private static final long serialVersionUID = 1L;

}
