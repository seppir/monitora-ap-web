package br.ufsc.monitoraap.exception.impl;

import br.ufsc.monitoraap.exception.ValidationException;

public class UsuarioNaoIdentificadoException extends ValidationException {

	private static final long serialVersionUID = 1L;

	public UsuarioNaoIdentificadoException() {
		super("Esta Audiência Pública não permite proposições de usuários não identificados!");
	}

}
