package br.ufsc.monitoraap.exception.impl;

import br.ufsc.monitoraap.exception.ValidationException;

public class ImportacaoExcelException extends ValidationException {

	private static final long serialVersionUID = 1L;

	public String urlDownloadFile;

	public ImportacaoExcelException(final String urlDocumentoPendente) {
		super("Erro na importação do documento");
		urlDownloadFile = urlDocumentoPendente;
	}

}
