package br.ufsc.monitoraap.dto;

import java.util.List;

public class ExportacaoAudienciaPublicaDTO {

	private String codigo;

	private List<String> respostas;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(final String codigo) {
		this.codigo = codigo;
	}

	public List<String> getRespostas() {
		return respostas;
	}

	public void setRespostas(final List<String> respostas) {
		this.respostas = respostas;
	}

}
