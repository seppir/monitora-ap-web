package br.ufsc.monitoraap.dto;

public class SistemaDTO {

	private String versao;

	private String caminhoArquivos;

	public String getVersao() {
		return versao;
	}

	public void setVersao(final String versao) {
		this.versao = versao;
	}

	public String getCaminhoArquivos() {
		return caminhoArquivos;
	}

	public void setCaminhoArquivos(final String caminhoArquivos) {
		this.caminhoArquivos = caminhoArquivos;
	}

}
