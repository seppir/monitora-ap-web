package br.ufsc.monitoraap.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.google.common.base.Preconditions;

import br.ufsc.monitoraap.util.mail.MailProperties;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = { "br.ufsc.monitoraap.repository" })
@EnableSpringConfigured
@ComponentScan("br.ufsc.monitoraap")
@PropertySource({ "classpath:application.properties" })
public class AppConfig {

	private static final String ENTITYMANAGER_PACKAGES_TO_SCAN = "br.ufsc.monitoraap.model";

	@Autowired
	private Environment env;

	@Bean
	public DataSource dataSource() {
		final DriverManagerDataSource dataSource = new DriverManagerDataSource();

		dataSource.setDriverClassName(Preconditions.checkNotNull(env.getProperty("db.driver")));
		dataSource.setUrl(Preconditions.checkNotNull(env.getProperty("db.url")));
		dataSource.setUsername(Preconditions.checkNotNull(env.getProperty("db.username")));
		dataSource.setPassword(env.getProperty("db.password"));

		return dataSource;
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		vendorAdapter.setDatabase(Database.MYSQL);
		vendorAdapter.setGenerateDdl(true);
		vendorAdapter.setShowSql(true);

		final LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setJpaVendorAdapter(vendorAdapter);
		factory.setDataSource(dataSource());
		factory.setPackagesToScan(ENTITYMANAGER_PACKAGES_TO_SCAN);

		return factory;
	}

	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		final LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
		sessionFactoryBean.setDataSource(dataSource());
		sessionFactoryBean.setPackagesToScan(ENTITYMANAGER_PACKAGES_TO_SCAN);
		sessionFactoryBean.setHibernateProperties(hibernateProperties());
		return sessionFactoryBean;
	}

	private Properties hibernateProperties() {
		final Properties properties = new Properties();

		properties.setProperty("hibernate.hbm2ddl.auto", Preconditions.checkNotNull(env.getProperty("hibernate.hbm2ddl.auto")));
		properties.setProperty("hibernate.dialect", Preconditions.checkNotNull(env.getProperty("hibernate.dialect")));

		return properties;
	}

	@Bean
	public PlatformTransactionManager transactionManager() {
		final JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
		return transactionManager;
	}

	@Bean
	public MailProperties mailProperties() {
		final MailProperties mailProperties = new MailProperties();
		mailProperties.setHostName(env.getProperty("mail.host_name"));
		mailProperties.setSmtpPort(Integer.parseInt(env.getProperty("mail.smtp_port")));
		mailProperties.setFrom(env.getProperty("mail.from"));
		mailProperties.setUsername(env.getProperty("mail.username"));
		mailProperties.setPassword(env.getProperty("mail.password"));
		return mailProperties;
	}

	@Bean
	public MailProperties mailPropertiesFlorianopolisMelhor() {
		final MailProperties mailProperties = new MailProperties();
		mailProperties.setHostName(env.getProperty("mail2.host_name"));
		mailProperties.setSmtpPort(Integer.parseInt(env.getProperty("mail2.smtp_port")));
		mailProperties.setFrom(env.getProperty("mail2.from"));
		mailProperties.setUsername(env.getProperty("mail2.username"));
		mailProperties.setPassword(env.getProperty("mail2.password"));
		return mailProperties;
	}

	@Bean
	public MultipartResolver multipartResolver() {
		final CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
		multipartResolver.setDefaultEncoding("UTF-8");
		multipartResolver.setMaxUploadSize(FileUtils.ONE_GB * 2);
		return multipartResolver;
	}

}
