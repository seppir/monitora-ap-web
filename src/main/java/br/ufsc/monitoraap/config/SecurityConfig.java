package br.ufsc.monitoraap.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	private static final String ROLE_ADMIN = "ROLE_ADMIN";
	private static final String ROLE_POLITICO = "ROLE_POLITICO";
	private static final String ROLE_PROPONENTE = "ROLE_PROPONENTE";

	private static final String USER_QUERY = "select u.email username, u.senha password, u.habilitado enabled "
		+ "from usuario u "
		+ "where u.email = ?";
	private static final String USER_ROLE_QUERY = "select u.email username, concat('ROLE_', p.nome) role "
		+ "from assoc_usuario_permissao a "
		+ "join usuario u on u.id = a.id_usuario "
		+ "join permissao p on p.id = a.id_permissao "
		+ "where u.email = ?";

	private static final String FACEBOOK_USER_QUERY = "select u.email username, u.facebook_id password, u.habilitado enabled "
		+ "from usuario u "
		+ "where u.email = ?";

	@Autowired
	private DataSource dataSource;

	@Autowired
	private PasswordEncoder passwordEncoder;

	/**
	 * @formatter:off
	 */
	@Override
	public void configure(final AuthenticationManagerBuilder auth) throws Exception {
		// @formatter:off
		auth
			.jdbcAuthentication().dataSource(dataSource)
			.passwordEncoder(passwordEncoder)
			.usersByUsernameQuery(USER_QUERY)
			.authoritiesByUsernameQuery(USER_ROLE_QUERY)
			.and()
			.jdbcAuthentication().dataSource(dataSource)
			.usersByUsernameQuery(FACEBOOK_USER_QUERY)
			.authoritiesByUsernameQuery(USER_ROLE_QUERY);
		// @formatter:on
	}

	@Override
	protected void configure(final HttpSecurity http) throws Exception {
		// @formatter:off
		http

			// Recursos e telas
			.authorizeRequests().antMatchers(HttpMethod.GET,
				"/webjars/**",
				"/css/**",
				"/js/**",
				"/images/**",
				"/login",
				"/home",
				"/novousuario")
			.permitAll()

			.and().formLogin()
			.loginPage("/")
			.permitAll()
			.and().logout()
			.logoutUrl("/logout")
			.logoutSuccessUrl("/")
			.permitAll()
			.and().authorizeRequests().antMatchers("/usuarios/authenticate").authenticated()

			// Pesquisa externa
			.and().authorizeRequests().antMatchers(
				"/proposicao/exporttoexcel",
				"/documento-pendente/download/**")
			.permitAll()

			// Home
			.and().authorizeRequests().antMatchers(HttpMethod.GET,
				"/ap/allwithcreator")
			.permitAll()

			// Cadastro de usuarios
			.and().authorizeRequests().antMatchers(HttpMethod.GET,
				"/estado/**",
				"/cidade/**",
				"/bairro/**",
				"/tempodomicilio/**",
				"/tipoimovel/**",
				"/estadocivil/**",
				"/escolaridade/**",
				"/idade/**",
				"/renda/**",
				"/status/**",
				"/domicilioeleitoral/**",
				"/usuarios/facebook/*")
			.permitAll()
			.and().authorizeRequests().antMatchers(HttpMethod.POST, "/proponentes/add")
			.permitAll()

			// Admin
			.and().authorizeRequests()
			.antMatchers(HttpMethod.POST,
				"/estado/**",
				"/cidade/**",
				"/bairro/**",
				"/tempodomicilio/**",
				"/tipoimovel/**",
				"/estadocivil/**",
				"/escolaridade/**",
				"/idade/**",
				"/renda/**",
				"/status/**",
				"/domicilioeleitoral/**",
				"/proposicao/savefromexcel/**",
				"/documento-pendente/**")
			.hasAuthority(ROLE_ADMIN)
			.antMatchers(HttpMethod.PUT,
				"/estado/**",
				"/cidade/**",
				"/bairro/**",
				"/tempodomicilio/**",
				"/tipoimovel/**",
				"/estadocivil/**",
				"/escolaridade/**",
				"/idade/**",
				"/renda/**",
				"/status/**",
				"/domicilioeleitoral/**")
			.hasAuthority(ROLE_ADMIN)
			.antMatchers(HttpMethod.DELETE,
				"/estado/**",
				"/cidade/**",
				"/bairro/**",
				"/tempodomicilio/**",
				"/tipoimovel/**",
				"/estadocivil/**",
				"/escolaridade/**",
				"/idade/**",
				"/renda/**",
				"/status/**",
				"/domicilioeleitoral/**")
			.hasAuthority(ROLE_ADMIN)
			.antMatchers(HttpMethod.PATCH,
				"/log/**")
			.hasAuthority(ROLE_ADMIN)

			// Importa Dados Excel
			.antMatchers(HttpMethod.GET,
				"/importaproposicoes")
			.hasAuthority(ROLE_ADMIN)

			// Dicionario de Termos
			.antMatchers("/termo/ap/**")
			.hasAuthority(ROLE_POLITICO)
			.antMatchers("/termo/**")
			.hasAuthority(ROLE_ADMIN)

			// Politico
			.and().authorizeRequests()
			.antMatchers(HttpMethod.POST,
				"/ap/**",
				"/video/**")
			.hasAuthority(ROLE_POLITICO)
			.antMatchers(HttpMethod.PUT,
				"/ap/**")
			.hasAuthority(ROLE_POLITICO)
			.antMatchers(HttpMethod.DELETE,
				"/ap/**")
			.hasAuthority(ROLE_POLITICO)

			// Proponente
			.and().authorizeRequests()
			.antMatchers(HttpMethod.POST,
				"/proposicao/**")
			.hasAuthority(ROLE_PROPONENTE)
			.antMatchers(HttpMethod.PUT,
				"/proposicao/**")
			.hasAuthority(ROLE_PROPONENTE)
			.antMatchers(HttpMethod.DELETE,
				"/proposicao/**")
			.hasAuthority(ROLE_PROPONENTE)

			.and().authorizeRequests().antMatchers(HttpMethod.GET,
				"/ap/**",
				"/proposicao/**",
				"/video/**")
			.hasAnyAuthority(ROLE_POLITICO, ROLE_PROPONENTE)

			.and().httpBasic()
			.and().csrf().disable();
		// @formatter:on
	}

	@Override
	public void configure(final WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/static/**", "/status");
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

}
