package br.ufsc.monitoraap.config;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

public class WebAppInitializer extends AbstractSecurityWebApplicationInitializer {

	private static String URL_BASE;

	public WebAppInitializer() {
		super(SecurityConfig.class, AppConfig.class, WebConfig.class);
	}

	public static String getUrlBase() {
		return URL_BASE;
	}

	public static void setUrlBase(final String urlBase) {
		if(StringUtils.isBlank(URL_BASE))
			URL_BASE = urlBase;
	}

}
