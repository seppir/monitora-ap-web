package br.ufsc.monitoraap.service;

import br.ufsc.monitoraap.model.Estado;

public interface EstadoService extends AbstractService<Estado, Integer> {

	Estado findByCity(Integer idCidade);
	
	Estado findBySigla (String siglaEstado);
}
