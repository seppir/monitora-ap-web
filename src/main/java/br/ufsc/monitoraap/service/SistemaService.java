package br.ufsc.monitoraap.service;

public interface SistemaService {

	public String getVersao();

	public String getCaminhoArquivos();

}
