package br.ufsc.monitoraap.service;

import java.util.List;

import br.ufsc.monitoraap.model.Permissao;

public interface PermissaoService extends AbstractService<Permissao, Integer> {

	public static enum Permissao {

		ADMINISTRADOR(1), POLITICO(2), PROPONENTE(3);

		private final Integer id;

		private Permissao(final Integer id) {
			this.id = id;
		}

		public Integer getId() {
			return id;
		}

	}

	List<br.ufsc.monitoraap.model.Permissao> getIds(Permissao... permissoes);

}
