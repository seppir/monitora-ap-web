package br.ufsc.monitoraap.service;

import br.ufsc.monitoraap.model.DomicilioEleitoral;

public interface DomicilioEleitoralService extends AbstractService<DomicilioEleitoral, Integer> {

	Iterable<DomicilioEleitoral> findByCity(Integer cityId);
	
	DomicilioEleitoral findByZona(final String zona, final Integer idCidade);

}
