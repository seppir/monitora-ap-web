package br.ufsc.monitoraap.service;

import br.ufsc.monitoraap.model.DocumentoPendente;

public interface DocumentoPendenteService extends AbstractService<DocumentoPendente, Integer> {}
