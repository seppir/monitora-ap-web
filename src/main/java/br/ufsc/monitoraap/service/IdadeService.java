package br.ufsc.monitoraap.service;

import br.ufsc.monitoraap.model.Idade;

public interface IdadeService extends AbstractService<Idade, Integer> {}
