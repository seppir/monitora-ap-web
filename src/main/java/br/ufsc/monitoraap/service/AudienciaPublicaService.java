package br.ufsc.monitoraap.service;

import br.ufsc.monitoraap.model.AudienciaPublica;
import br.ufsc.monitoraap.model.Usuario;

public interface AudienciaPublicaService extends AbstractService<AudienciaPublica, Integer> {

	AudienciaPublica save(Usuario usuarioCadastrador, AudienciaPublica audienciaPublica);

	Iterable<AudienciaPublica> findAllWithCreator();

	AudienciaPublica findOneWithAllData(Integer id);

	AudienciaPublica findOneWithCreator(final Integer id);

	AudienciaPublica findByCodAP(String codAp);

	Iterable<AudienciaPublica> findFromCreator(Integer idCriador);

}
