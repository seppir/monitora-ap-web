package br.ufsc.monitoraap.service;

import br.ufsc.monitoraap.model.Cidade;

public interface CidadeService extends AbstractService<Cidade, Integer> {

	Iterable<Cidade> findByState(Integer idEstado);

	Cidade findByNameAndState(final String nomeCidade, final Integer idEstado);

	Cidade findByNome(String nome);

}
