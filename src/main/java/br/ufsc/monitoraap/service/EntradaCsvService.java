package br.ufsc.monitoraap.service;

import java.io.File;

import br.ufsc.monitoraap.model.Arquivo;
import br.ufsc.monitoraap.model.EntradaCsv;

public interface EntradaCsvService extends AbstractService<EntradaCsv, Integer> {

	Arquivo gerarExcel() throws Exception;

	File gerarPdf() throws Exception;

}
