package br.ufsc.monitoraap.service;

import org.springframework.web.multipart.MultipartFile;

import br.ufsc.monitoraap.model.Video;

public interface VideoService extends AbstractService<Video, Integer> {

	Video save(Video entity, MultipartFile file);

}
