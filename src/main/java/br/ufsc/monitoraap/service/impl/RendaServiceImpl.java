package br.ufsc.monitoraap.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufsc.monitoraap.model.Renda;
import br.ufsc.monitoraap.repository.RendaRepository;
import br.ufsc.monitoraap.service.RendaService;

@Service
@Transactional
public class RendaServiceImpl extends AbstractServiceImpl<Renda, Integer> implements RendaService {

	@Autowired
	public RendaServiceImpl(final RendaRepository repository) {
		super(repository);
	}

}
