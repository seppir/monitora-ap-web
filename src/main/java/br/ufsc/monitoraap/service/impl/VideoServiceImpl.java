package br.ufsc.monitoraap.service.impl;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import br.ufsc.monitoraap.exception.impl.NaoPodeExcluirVideoException;
import br.ufsc.monitoraap.exception.impl.NaoPodeProcessarVideoException;
import br.ufsc.monitoraap.model.Video;
import br.ufsc.monitoraap.repository.VideoRepository;
import br.ufsc.monitoraap.service.VideoService;
import br.ufsc.monitoraap.util.persistence.fileresource.FileResourceManager;

@Service
@Transactional
public class VideoServiceImpl extends AbstractServiceImpl<Video, Integer> implements VideoService {

	@Autowired
	private FileResourceManager fileResourceManager;

	@Autowired
	public VideoServiceImpl(final VideoRepository repository) {
		super(repository);
	}

	@Override
	public Video save(final Video entity, final MultipartFile file) {
		entity.setNome(file.getOriginalFilename());
		save(entity);
		try {
			log.debug("Persisting Video[" + entity.getId() + "]");
			final File destFile = fileResourceManager.save(entity, file,
				String.valueOf(entity.getId()).concat(".").concat(FileResourceManager.getExtension(entity.getNome())));
			final String destPath = destFile.getAbsolutePath();
			((VideoRepository) repository).updateFilePath(entity.getId(), destPath);
		} catch(final IOException e) {
			log.error("Error while persisting video!", e);
			throw new NaoPodeProcessarVideoException(entity, e);
		}
		return entity;
	}

	@Override
	public void delete(final Integer id) {
		final String fileName = findOne(id).getCaminhoArquivo();
		super.delete(id);
		try {
			if(!FileResourceManager.retrieveFile(fileName).delete())
				throw new Exception();
		} catch(final Exception e) {
			log.error("Error while deleting video!", e);
			throw new NaoPodeExcluirVideoException(id, fileName);
		}
	}

}
