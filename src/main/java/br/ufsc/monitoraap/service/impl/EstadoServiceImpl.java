package br.ufsc.monitoraap.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufsc.monitoraap.model.Estado;
import br.ufsc.monitoraap.repository.EstadoRepository;
import br.ufsc.monitoraap.service.EstadoService;

@Service
@Transactional
public class EstadoServiceImpl extends AbstractServiceImpl<Estado, Integer> implements EstadoService {

	@Autowired
	public EstadoServiceImpl(final EstadoRepository repository) {
		super(repository);
	}
	
	@Override
	public Estado findBySigla (final String siglaEstado){
		return ((EstadoRepository)repository).findBySigla(siglaEstado);
	}

	@Override
	public Estado findByCity(final Integer idCidade) {
		return ((EstadoRepository) repository).findByCity(idCidade);
	}

}
