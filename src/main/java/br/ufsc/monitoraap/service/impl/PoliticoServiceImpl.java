package br.ufsc.monitoraap.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufsc.monitoraap.model.Politico;
import br.ufsc.monitoraap.repository.PoliticoRepository;
import br.ufsc.monitoraap.service.PermissaoService;
import br.ufsc.monitoraap.service.PermissaoService.Permissao;
import br.ufsc.monitoraap.service.PoliticoService;

@Service
@Transactional
public class PoliticoServiceImpl extends AbstractServiceImpl<Politico, Integer> implements PoliticoService {

	@Autowired
	private PermissaoService permissaoService;

	@Autowired
	public PoliticoServiceImpl(final PoliticoRepository repository) {
		super(repository);
	}

	@Override
	public Politico findOne(final Integer id) {
		return repository.findOne(id);
	}

	@Override
	public Politico save(final Politico entity) {
		if(entity.getId() == null)
			entity.setPermissoes(permissaoService.getIds(Permissao.POLITICO));
		return super.save(entity);
	}

	@Override
	public void delete(final Integer id) {
		repository.delete(id);
	}

}
