package br.ufsc.monitoraap.service.impl;

import java.util.Calendar;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufsc.monitoraap.exception.impl.FacebookJaVinculadoException;
import br.ufsc.monitoraap.exception.impl.SenhaAtualIncorretaException;
import br.ufsc.monitoraap.exception.impl.SenhasDiferentesException;
import br.ufsc.monitoraap.exception.impl.UsuarioNaoExisteException;
import br.ufsc.monitoraap.model.Administrador;
import br.ufsc.monitoraap.model.Politico;
import br.ufsc.monitoraap.model.Proponente;
import br.ufsc.monitoraap.model.Usuario;
import br.ufsc.monitoraap.repository.UsuarioRepository;
import br.ufsc.monitoraap.service.AdministradorService;
import br.ufsc.monitoraap.service.PoliticoService;
import br.ufsc.monitoraap.service.ProponenteService;
import br.ufsc.monitoraap.service.UsuarioService;

@Service
@Transactional
public class UsuarioServiceImpl extends AbstractServiceImpl<Usuario, Integer> implements UsuarioService {

	@Autowired
	private AdministradorService administradorService;

	@Autowired
	private PoliticoService politicoService;

	@Autowired
	private ProponenteService proponenteService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	public UsuarioServiceImpl(final UsuarioRepository repository) {
		super(repository);
	}

	@Override
	public Usuario findByEmail(final String email) {
		return ((UsuarioRepository) repository).findByEmail(email);
	}

	@Override
	public Usuario authenticate(final String email) {
		return findByEmail(email);
	}

	@Override
	public Usuario save(final Usuario entity) {
		if(entity.getId() == null) {
			if(!StringUtils.equals(entity.getSenha(), entity.getSenha2()))
				throw new SenhasDiferentesException();
			entity.setHabilitado(true);
			entity.setSenha(passwordEncoder.encode(entity.getSenha()));
		} else {
			if(StringUtils.isNotEmpty(entity.getSenhaAtualTroca()) && StringUtils.isNotEmpty(entity.getSenhaTroca1())
				&& StringUtils.isNotEmpty(entity.getSenhaTroca2())) {
				if(!passwordEncoder.matches(entity.getSenhaAtualTroca(), entity.getSenha()))
					throw new SenhaAtualIncorretaException();
				if(!StringUtils.equals(entity.getSenhaTroca1(), entity.getSenhaTroca2()))
					throw new SenhasDiferentesException();
				entity.setSenha(passwordEncoder.encode(entity.getSenhaTroca1()));
			}
		}

		entity.setDataUltimaAtualizacao(Calendar.getInstance().getTime());

		if(entity instanceof Administrador)
			return administradorService.save((Administrador) entity);
		else if(entity instanceof Politico)
			return politicoService.save((Politico) entity);
		else
			return proponenteService.save((Proponente) entity);
	}

	@Override
	public Usuario findByFacebookId(final String facebookId) {
		return ((UsuarioRepository) repository).findByFacebookId(facebookId);
	}

	@Override
	public void bindFacebookId(final Integer id, final String facebookId) {
		if(!repository.exists(id))
			throw new UsuarioNaoExisteException();
		if(((UsuarioRepository) repository).existsFacebookId(facebookId))
			throw new FacebookJaVinculadoException();
		((UsuarioRepository) repository).bindFacebookId(id, facebookId);
	}

}
