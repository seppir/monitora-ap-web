package br.ufsc.monitoraap.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufsc.monitoraap.model.DomicilioEleitoral;
import br.ufsc.monitoraap.repository.DomicilioEleitoralRepository;
import br.ufsc.monitoraap.service.DomicilioEleitoralService;

@Service
@Transactional
public class DomicilioEleitoralServiceImpl extends AbstractServiceImpl<DomicilioEleitoral, Integer> implements DomicilioEleitoralService {

	@Autowired
	public DomicilioEleitoralServiceImpl(final DomicilioEleitoralRepository repository) {
		super(repository);
	}

	@Override
	public Iterable<DomicilioEleitoral> findByCity(final Integer cityId) {
		return ((DomicilioEleitoralRepository) repository).findByCity(cityId);
	}
	
	@Override
	public DomicilioEleitoral findByZona(final String zona, final Integer idCidade){
		return ((DomicilioEleitoralRepository) repository).findByZona(zona, idCidade);
	}

}
