package br.ufsc.monitoraap.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufsc.monitoraap.model.Tematica;
import br.ufsc.monitoraap.repository.TematicaRepository;
import br.ufsc.monitoraap.service.TematicaService;

@Service
@Transactional
public class TematicaServiceImpl extends AbstractServiceImpl<Tematica, Integer> implements TematicaService {

	@Autowired
	public TematicaServiceImpl(final TematicaRepository repository) {
		super(repository);
	}

	@Override
	public Tematica findByNome(final String nome) {
		return ((TematicaRepository) repository).findByNome(nome);
	}

}
