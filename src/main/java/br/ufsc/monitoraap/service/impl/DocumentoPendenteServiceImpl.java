package br.ufsc.monitoraap.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufsc.monitoraap.model.DocumentoPendente;
import br.ufsc.monitoraap.repository.DocumentoPendenteRepository;
import br.ufsc.monitoraap.service.DocumentoPendenteService;

@Service
@Transactional
public class DocumentoPendenteServiceImpl extends AbstractServiceImpl<DocumentoPendente, Integer> implements DocumentoPendenteService {

	@Autowired
	public DocumentoPendenteServiceImpl(final DocumentoPendenteRepository repository) {
		super(repository);
	}

}
