package br.ufsc.monitoraap.service.impl;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;

import br.ufsc.monitoraap.exception.impl.MaximoProposicoesPorUsuarioExcedidoException;
import br.ufsc.monitoraap.model.Arquivo;
import br.ufsc.monitoraap.model.EntradaCsv;
import br.ufsc.monitoraap.repository.EntradaCsvRepository;
import br.ufsc.monitoraap.service.EntradaCsvService;
import br.ufsc.monitoraap.service.MailerService;
import br.ufsc.monitoraap.util.pdf.ListaEntradasCsv;
import br.ufsc.monitoraap.util.pdf.PDFUtils;
import br.ufsc.monitoraap.util.xls.XLSUtils;

@Service
@Transactional
public class EntradaCsvServiceImpl extends AbstractServiceImpl<EntradaCsv, Integer> implements EntradaCsvService {

	private static final int LIMITE_RESPOSTAS_POR_USUARIO = 10;

	private static final String NOME = "Nome";
	private static final String BAIRRO = "Bairro";
	private static final String FAIXA_ETARIA = "Faixa Etária";
	private static final String RESPOSTA_1 = "Resposta 1";
	private static final String RESPOSTA_2 = "Resposta 2";
	private static final String EMAIL = "Email";
	private static final String WHATSAPP = "WhatsApp";

	@Autowired
	@Qualifier("florianopolisMelhorMailerService")
	private MailerService mailerService;

	@Autowired
	public EntradaCsvServiceImpl(final EntradaCsvRepository repository) {
		super(repository);
	}

	@Override
	public EntradaCsv save(final EntradaCsv entity) {
		final int qtdRespostas = ((EntradaCsvRepository) repository).countByEmail(entity.getEmail());
		if(qtdRespostas >= LIMITE_RESPOSTAS_POR_USUARIO)
			throw new MaximoProposicoesPorUsuarioExcedidoException();

		try {
			super.save(entity);
			mailerService.send(
				"Pesquisa de opinião \"Florianópolis Melhor\"",
				String.format("<p>Olá, %s!</p><p>Obrigado por participar da pesquisa <b>Florianópolis Melhor</b>.</p>", entity.getNome()),
				entity.getEmail());
		} catch(final Exception e) {
			e.printStackTrace();
		}
		return entity;
	}

	@Override
	public Arquivo gerarExcel() throws Exception {
		final List<String> campos = Arrays.asList(NOME, BAIRRO, FAIXA_ETARIA, RESPOSTA_1, RESPOSTA_2, EMAIL, WHATSAPP);
		final Iterable<EntradaCsv> entradas = getAll();
		final Map<String, List<String>> data = new LinkedHashMap<>(campos.size());

		for(final String campo : campos) {
			data.put(campo, new ArrayList<String>());
		}

		for(final EntradaCsv entrada : entradas) {
			data.get(NOME).add(entrada.getNome());
			data.get(BAIRRO).add(entrada.getBairro().getNomeApresentacao());
			data.get(FAIXA_ETARIA).add(entrada.getIdade().getDescricao());
			data.get(RESPOSTA_1).add(entrada.getResposta1());
			data.get(RESPOSTA_2).add(entrada.getResposta2());
			data.get(EMAIL).add(entrada.getEmail());
			data.get(WHATSAPP).add(entrada.getWhatsapp());
		}

		return XLSUtils.createDocument(
			"Entradas_".concat(new SimpleDateFormat("dd-MM-yyyy'T'HH-mm-ss-SSS").format(Calendar.getInstance().getTime())).concat(".xls"),
			data,
			true);
	}

	@Override
	public File gerarPdf() throws Exception {
		final List<EntradaCsv> entradas = (List<EntradaCsv>) getAll();
		final File file = File.createTempFile("Entradas_".concat(new SimpleDateFormat("dd-MM-yyyy'T'HH-mm-ss-SSS").format(Calendar.getInstance().getTime())),
			".pdf");
		final Document pdf = PDFUtils.createDocument(file);

		pdf.open();

		pdf.add(new Paragraph(PDFUtils.write("Respostas", PDFUtils.FONTE_TITULO)));
		pdf.add(new Paragraph(PDFUtils.write(String.format("Total: %d", entradas.size()), PDFUtils.FONTE_NEGRITO)));
		pdf.add(Chunk.NEWLINE);

		pdf.add(new ListaEntradasCsv(entradas));

		pdf.close();

		return file;
	}

}
