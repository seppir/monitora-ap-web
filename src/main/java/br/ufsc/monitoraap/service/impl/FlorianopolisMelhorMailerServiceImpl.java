package br.ufsc.monitoraap.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufsc.monitoraap.util.mail.MailProperties;

@Service("florianopolisMelhorMailerService")
@Transactional
public class FlorianopolisMelhorMailerServiceImpl extends AbstractMailerServiceImpl {

	@Autowired
	protected FlorianopolisMelhorMailerServiceImpl(final MailProperties mailPropertiesFlorianopolisMelhor) {
		super(mailPropertiesFlorianopolisMelhor);
	}

	@Override
	public void send(final String subject, final String message, final String to) throws Exception {
		sendEmail(subject, message, to);
	}

}
