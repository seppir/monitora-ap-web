package br.ufsc.monitoraap.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufsc.monitoraap.util.mail.MailProperties;

@Service
@Primary
@Transactional
public class MailerServiceImpl extends AbstractMailerServiceImpl {

	@Autowired
	protected MailerServiceImpl(final MailProperties mailProperties) {
		super(mailProperties);
	}

	@Override
	public void send(final String subject, final String message, final String to) throws Exception {
		sendEmail(subject, message, to);
	}

}
