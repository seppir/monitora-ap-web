package br.ufsc.monitoraap.service.impl;

import java.io.IOException;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import br.ufsc.monitoraap.dto.SistemaDTO;
import br.ufsc.monitoraap.service.SistemaService;

@Service
public class SistemaServiceImpl implements SistemaService {

	private final Logger log = Logger.getLogger(getClass());

	private final SistemaDTO sistema = new SistemaDTO();

	@PostConstruct
	public void init() {
		try {
			final Resource r = new ClassPathResource("application.properties");
			final Properties p = new Properties();
			p.load(r.getInputStream());
			sistema.setVersao(p.getProperty("build.version"));
			sistema.setCaminhoArquivos(p.getProperty("fileresources.destinationpath"));
		} catch(final IOException e) {
			log.error("Could not load properties!", e);
		}
	}

	@Override
	public String getVersao() {
		return sistema.getVersao();
	}

	@Override
	public String getCaminhoArquivos() {
		return sistema.getCaminhoArquivos();
	}

}
