package br.ufsc.monitoraap.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufsc.monitoraap.model.EstadoCivil;
import br.ufsc.monitoraap.repository.EstadoCivilRepository;
import br.ufsc.monitoraap.service.EstadoCivilService;

@Service
@Transactional
public class EstadoCivilServiceImpl extends AbstractServiceImpl<EstadoCivil, Integer> implements EstadoCivilService {

	@Autowired
	public EstadoCivilServiceImpl(final EstadoCivilRepository repository) {
		super(repository);
	}

	@Override
	public EstadoCivil findByNome(String nomeEstadoCivil) {
		return ((EstadoCivilRepository) repository).findByNome(nomeEstadoCivil);
	}

}
