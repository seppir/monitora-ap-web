package br.ufsc.monitoraap.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufsc.monitoraap.dto.ExportacaoAudienciaPublicaDTO;
import br.ufsc.monitoraap.exception.impl.ImportacaoExcelException;
import br.ufsc.monitoraap.exception.impl.MaximoProposicoesPorUsuarioExcedidoException;
import br.ufsc.monitoraap.exception.impl.UsuarioNaoIdentificadoException;
import br.ufsc.monitoraap.model.Arquivo;
import br.ufsc.monitoraap.model.AudienciaPublica;
import br.ufsc.monitoraap.model.Documento;
import br.ufsc.monitoraap.model.DocumentoPendente;
import br.ufsc.monitoraap.model.Proponente;
import br.ufsc.monitoraap.model.Proposicao;
import br.ufsc.monitoraap.model.Tematica;
import br.ufsc.monitoraap.repository.ProposicaoRepository;
import br.ufsc.monitoraap.service.AudienciaPublicaService;
import br.ufsc.monitoraap.service.DocumentoPendenteService;
import br.ufsc.monitoraap.service.ProponenteService;
import br.ufsc.monitoraap.service.ProposicaoService;
import br.ufsc.monitoraap.service.SolrService;
import br.ufsc.monitoraap.service.TematicaService;
import br.ufsc.monitoraap.util.persistence.aftercommit.AfterCommitExecutor;
import br.ufsc.monitoraap.util.xls.XLSUtils;

@Service
@Transactional(noRollbackFor = ImportacaoExcelException.class)
public class ProposicaoServiceImpl extends AbstractServiceImpl<Proposicao, Integer> implements ProposicaoService {

	@Autowired
	private TematicaService tematicaService;

	@Autowired
	private AudienciaPublicaService audienciaPublicaService;

	@Autowired
	private ProponenteService proponenteService;

	@Autowired
	private DocumentoPendenteService documentoPendenteService;

	@Autowired
	private AfterCommitExecutor afterCommitExecutor;

	@Autowired
	private SolrService solrService;

	@Autowired
	public ProposicaoServiceImpl(final ProposicaoRepository repository) {
		super(repository);
	}

	@Override
	public Iterable<Proposicao> findByAp(final Integer idAp) {
		return ((ProposicaoRepository) repository).findByAp(idAp);
	}

	@Override
	public Iterable<Proposicao> findByUserAndAp(final Integer idProponente, final Integer idAp) {
		return ((ProposicaoRepository) repository).findByUserAndAp(idProponente, idAp);
	}

	@Override
	public Proposicao save(final Proponente proponente, final Proposicao proposicao) {
		if(proposicao.getId() == null)
			proposicao.setProponente(proponente);
		save(proposicao);
		return proposicao;
	}

	@Override
	public Proposicao save(final Proposicao entity) {
		final AudienciaPublica ap = audienciaPublicaService.findOne(entity.getIdAudienciaPublica());

		if(ap.isUsuariosIdentificados() && StringUtils.isEmpty(entity.getProponente().getCpf()))
			throw new UsuarioNaoIdentificadoException();

		if(entity.getId() == null) {
			final Integer numProps = ((ProposicaoRepository) repository).countByUserAndAp(entity.getProponente().getId(), ap.getId());
			if(ap.getMaxProposicoesUsuario() != null && numProps >= ap.getMaxProposicoesUsuario())
				throw new MaximoProposicoesPorUsuarioExcedidoException();
			entity.setDataCadastro(Calendar.getInstance().getTime());
		}

		final int qtdTematicas = CollectionUtils.size(entity.getSugestaoTematicas());
		if(qtdTematicas > 0) {
			final List<Tematica> doppelgangers = new ArrayList<>(qtdTematicas);
			for(final Iterator<Tematica> iterator = entity.getSugestaoTematicas().iterator(); iterator.hasNext();) {
				final Tematica tematica = iterator.next();
				if(tematica.getId() == null) {
					final Tematica doppelganger = tematicaService.findByNome(tematica.getNome());
					if(doppelganger != null) {
						iterator.remove();
						doppelgangers.add(doppelganger);
						continue;
					} else {
						tematicaService.save(tematica);
					}
				}
			}
			if(CollectionUtils.isNotEmpty(doppelgangers))
				entity.getSugestaoTematicas().addAll(doppelgangers);
		}

		super.save(entity);

		afterCommitExecutor.execute(new Runnable() {
			@Override
			public void run() {
				solrService.executeIndexing(entity.getId());
			}
		});

		return entity;
	}

	@Override
	public void importaProposicoesExcel(final Integer idProponente, final Date dataCadastro, final Documento documento) throws Exception {
		final Map<String, List<String>> proposicoesInvalidas = new HashMap<String, List<String>>();

		final Proponente proponente = proponenteService.findOne(idProponente);

		final List<AudienciaPublica> audiencias = new ArrayList<AudienciaPublica>();
		int numPropInvalidas = 0;

		final Sheet sheet = XLSUtils.getWorkbook(documento).getSheetAt(0);

		try {
			final Iterator<Row> rowIterator = sheet.iterator();
			while(rowIterator.hasNext()) {
				final Row row = rowIterator.next();
				final Iterator<Cell> cellIterator = row.cellIterator();
				while(cellIterator.hasNext()) {
					AudienciaPublica ap;
					final Cell cell = cellIterator.next();

					if(cell.getRowIndex() == 0) {
						final String codAud = cell.getCellType() == Cell.CELL_TYPE_NUMERIC ? String.valueOf(cell.getNumericCellValue())
							: cell.getStringCellValue();
						ap = audienciaPublicaService.findByCodAP(codAud);

						if(ap == null) {
							ap = new AudienciaPublica();
							ap.setCodigo(codAud);
							proposicoesInvalidas.put(codAud, new ArrayList<String>());
						}
						audiencias.add(cell.getColumnIndex(), ap);
					} else if(StringUtils.isNotBlank(cell.getStringCellValue())) {
						ap = audiencias.get(cell.getColumnIndex());

						if(ap.getId() != null) {
							final Proposicao proposicao = new Proposicao();
							proposicao.setProponente(proponente);
							proposicao.setDataCadastro(dataCadastro);
							proposicao.setTexto(cell.getStringCellValue());
							proposicao.setIdAudienciaPublica(ap.getId());
							proposicao.setOpiniaoConteudoAp(" --- ");
							save(proposicao);
						} else {
							final List<String> listProposicoes = proposicoesInvalidas.get(ap.getCodigo());
							if(listProposicoes.isEmpty())
								listProposicoes.add(ap.getCodigo());
							listProposicoes.add(cell.getStringCellValue());
							proposicoesInvalidas.put(ap.getCodigo(), listProposicoes);
							numPropInvalidas++;
						}
					}
				}
			}

		} catch(final Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			sheet.getWorkbook().close();
		}

		if(numPropInvalidas > 0) {
			final DocumentoPendente documentoPendente = mountFileProposicoesInvalidas(proposicoesInvalidas);
			throw new ImportacaoExcelException("documento-pendente/download/" + documentoPendente.getId());
		}
	}

	@Override
	public DocumentoPendente exportarExcelParaImportacao(final Iterable<ExportacaoAudienciaPublicaDTO> proposicoes) throws Exception {
		final Map<String, List<String>> data = new HashMap<>();
		for(final ExportacaoAudienciaPublicaDTO proposicao : proposicoes) {
			data.put(proposicao.getCodigo(), proposicao.getRespostas());
		}
		return createFileExportacao(data);
	}

	private DocumentoPendente createFileExportacao(final Map<String, List<String>> data) throws Exception {
		return createPendingExcelDocument(
			"Exportacao_".concat(new SimpleDateFormat("dd-MM-yyyy'T'HH-mm-ss-SSS").format(Calendar.getInstance().getTime())),
			data,
			true);
	}

	private DocumentoPendente mountFileProposicoesInvalidas(final Map<String, List<String>> data) throws Exception {
		return createPendingExcelDocument("Proposições inválidas", data, true);
	}

	private DocumentoPendente createPendingExcelDocument(final String name, final Map<String, List<String>> data, final boolean insertKeyOnTop)
		throws Exception {
		final Arquivo arquivo = XLSUtils.createDocument(name, data, insertKeyOnTop);

		final DocumentoPendente documentoPendente = new DocumentoPendente();
		documentoPendente.setNome(arquivo.getNome());
		documentoPendente.setConteudo(arquivo.getConteudo());
		return documentoPendenteService.save(documentoPendente);
	}

}
