package br.ufsc.monitoraap.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufsc.monitoraap.model.Permissao;
import br.ufsc.monitoraap.repository.PermissaoRepository;
import br.ufsc.monitoraap.service.PermissaoService;

@Service
@Transactional
public class PermissaoServiceImpl extends AbstractServiceImpl<Permissao, Integer> implements PermissaoService {

	@Autowired
	public PermissaoServiceImpl(final PermissaoRepository repository) {
		super(repository);
	}

	@Override
	public List<br.ufsc.monitoraap.model.Permissao> getIds(final br.ufsc.monitoraap.service.PermissaoService.Permissao... permissoes) {
		final List<Integer> ids = new ArrayList<>();
		for(final Permissao permissao : permissoes)
			ids.add(permissao.getId());
		return repository.findAll(ids);
	}

}
