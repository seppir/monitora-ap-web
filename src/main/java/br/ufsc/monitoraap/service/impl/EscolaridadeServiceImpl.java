package br.ufsc.monitoraap.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufsc.monitoraap.model.Escolaridade;
import br.ufsc.monitoraap.repository.EscolaridadeRepository;
import br.ufsc.monitoraap.service.EscolaridadeSerivce;

@Service
@Transactional
public class EscolaridadeServiceImpl extends AbstractServiceImpl<Escolaridade, Integer> implements EscolaridadeSerivce {

	@Autowired
	public EscolaridadeServiceImpl(final EscolaridadeRepository repository) {
		super(repository);
	}

}
