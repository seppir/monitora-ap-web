package br.ufsc.monitoraap.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufsc.monitoraap.model.Idade;
import br.ufsc.monitoraap.repository.IdadeRepository;
import br.ufsc.monitoraap.service.IdadeService;

@Service
@Transactional
public class IdadeServiceImpl extends AbstractServiceImpl<Idade, Integer> implements IdadeService {

	@Autowired
	public IdadeServiceImpl(final IdadeRepository repository) {
		super(repository);
	}

}
