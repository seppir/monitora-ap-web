package br.ufsc.monitoraap.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufsc.monitoraap.model.Documento;
import br.ufsc.monitoraap.repository.DocumentoRepository;
import br.ufsc.monitoraap.service.DocumentoService;

@Service
@Transactional
public class DocumentoServiceImpl extends AbstractServiceImpl<Documento, Integer> implements DocumentoService {

	@Autowired
	public DocumentoServiceImpl(final DocumentoRepository repository) {
		super(repository);
	}

}
