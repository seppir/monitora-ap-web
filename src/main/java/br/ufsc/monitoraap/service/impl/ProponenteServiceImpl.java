package br.ufsc.monitoraap.service.impl;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.swing.text.MaskFormatter;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

import br.ufsc.monitoraap.enumeration.Sexo;
import br.ufsc.monitoraap.exception.impl.CpfInvalidoException;
import br.ufsc.monitoraap.exception.impl.ImportacaoExcelException;
import br.ufsc.monitoraap.model.Bairro;
import br.ufsc.monitoraap.model.Cidade;
import br.ufsc.monitoraap.model.Documento;
import br.ufsc.monitoraap.model.DocumentoPendente;
import br.ufsc.monitoraap.model.DomicilioEleitoral;
import br.ufsc.monitoraap.model.Escolaridade;
import br.ufsc.monitoraap.model.Estado;
import br.ufsc.monitoraap.model.EstadoCivil;
import br.ufsc.monitoraap.model.Idade;
import br.ufsc.monitoraap.model.Proponente;
import br.ufsc.monitoraap.model.Renda;
import br.ufsc.monitoraap.model.TempoDomicilio;
import br.ufsc.monitoraap.model.TipoImovel;
import br.ufsc.monitoraap.repository.ProponenteRepository;
import br.ufsc.monitoraap.service.BairroService;
import br.ufsc.monitoraap.service.CidadeService;
import br.ufsc.monitoraap.service.DocumentoPendenteService;
import br.ufsc.monitoraap.service.DomicilioEleitoralService;
import br.ufsc.monitoraap.service.EscolaridadeSerivce;
import br.ufsc.monitoraap.service.EstadoCivilService;
import br.ufsc.monitoraap.service.EstadoService;
import br.ufsc.monitoraap.service.IdadeService;
import br.ufsc.monitoraap.service.MailerService;
import br.ufsc.monitoraap.service.PermissaoService;
import br.ufsc.monitoraap.service.PermissaoService.Permissao;
import br.ufsc.monitoraap.service.ProponenteService;
import br.ufsc.monitoraap.service.RendaService;
import br.ufsc.monitoraap.service.TempoDomicilioService;
import br.ufsc.monitoraap.service.TipoImovelService;
import br.ufsc.monitoraap.util.xls.XLSUtils;

@Service
@Transactional
public class ProponenteServiceImpl extends AbstractServiceImpl<Proponente, Integer> implements ProponenteService {

	@Autowired
	BairroService bairroService;

	@Autowired
	CidadeService cidadeService;

	@Autowired
	EstadoService estadoService;

	@Autowired
	EscolaridadeSerivce escolaridadeService;

	@Autowired
	TipoImovelService tipoImovelService;

	@Autowired
	EstadoCivilService estadoCivilService;

	@Autowired
	TempoDomicilioService tempoDomicilioService;

	@Autowired
	IdadeService idadeService;

	@Autowired
	RendaService rendaService;

	@Autowired
	DomicilioEleitoralService domicilioEleitoralService;

	@Autowired
	DocumentoPendenteService documentoPendenteService;

	@Autowired
	MailerService mailerService;

	private static boolean isCpfValido(final String testCpf) {
		final String cpf = testCpf.replaceAll("\\D+", "");
		if(cpf == null || cpf.length() != 11)
			return false;

		final int[] pesoCPF = { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };

		final Integer digito1 = calcularDigito(cpf.substring(0, 9), pesoCPF);
		final Integer digito2 = calcularDigito(cpf.substring(0, 9) + digito1, pesoCPF);
		return cpf.equals(cpf.substring(0, 9) + digito1.toString() + digito2.toString());
	}

	private static String format(final String pattern, final Object value) {
		MaskFormatter mask;
		try {
			mask = new MaskFormatter(pattern);
			mask.setValueContainsLiteralCharacters(false);
			return mask.valueToString(value);
		} catch(final ParseException e) {
			throw new RuntimeException(e);
		}
	}

	private static int calcularDigito(final String str, final int[] peso) {
		int soma = 0;
		for(int indice = str.length() - 1, digito; indice >= 0; indice--) {
			digito = Integer.parseInt(str.substring(indice, indice + 1));
			soma += digito * peso[peso.length - str.length() + indice];
		}
		soma = 11 - soma % 11;
		return soma > 9 ? 0 : soma;
	}

	@Autowired
	private PermissaoService permissaoService;

	@Autowired
	public ProponenteServiceImpl(final ProponenteRepository repository) {
		super(repository);
	}

	@Override
	public Proponente save(final Proponente entity) {
		if(entity.getId() == null) {
			if(StringUtils.isNotEmpty(entity.getCpf()) && !isCpfValido(entity.getCpf()))
				throw new CpfInvalidoException();
			entity.setPermissoes(permissaoService.getIds(Permissao.PROPONENTE));
		}
		return super.save(entity);
	}

	@Override
	public void importaProponentesExcel(final Documento documento) throws Exception {
		final Sheet sheet = XLSUtils.getWorkbook(documento).getSheetAt(0);
		final List<Row> invalidRows = new ArrayList<Row>();
		final Iterator<Row> rowIterator = sheet.iterator();

		final Iterable<TempoDomicilio> faixasTempoDomicilio = tempoDomicilioService.getAll();
		final Iterable<Idade> faixasIdade = idadeService.getAll();
		final Iterable<Renda> faixasRenda = rendaService.getAll();
		final Iterable<Escolaridade> escolaridades = escolaridadeService.getAll();

		try {
			while(rowIterator.hasNext()) {
				final Row row = rowIterator.next();

				fillEmptyCells(row);

				final Iterator<Cell> cellIterator = row.cellIterator();
				final Proponente proponente = new Proponente();
				Estado estadoResidencia = null;
				Cidade cidadeResidencia = null;
				Estado estadoNaturalidade = null;
				Estado estadoDomicilioEleitoral = null;
				Cidade cidadeDomicilioEleitoral = null;
				final StringBuilder erros = new StringBuilder();

				while(cellIterator.hasNext()) {

					final Cell cell = cellIterator.next();
					if(cell.getRowIndex() != 0) {

						switch(cell.getColumnIndex()) {
							case 0:
								final String nome = cell.getStringCellValue();
								if(StringUtils.isNotEmpty(nome))
									proponente.setNome(nome);
								else
									erros.append("Coluna Nome; ");
								break;
							case 1:
								final String email = cell.getStringCellValue();
								if(StringUtils.isNotEmpty(email))
									proponente.setEmail(email);
								else
									erros.append("Coluna Email; ");
								break;
							case 2:
								String cpf = cell.getCellType() == Cell.CELL_TYPE_NUMERIC ? String.format("%.0f", cell.getNumericCellValue())
									: cell.getStringCellValue();
								cpf = format("###.###.###-##", cpf);

								if(isCpfValido(cpf))
									proponente.setCpf(cpf);
								else
									erros.append("Coluna CPF; ");
								break;
							case 3:
								if(StringUtils.isNotEmpty(cell.getStringCellValue()))
									proponente.setSexo(cell.getStringCellValue().equals("F") ? Sexo.FEMININO : Sexo.MASCULINO);
								else
									erros.append("Coluna Sexo; ");
								break;
							case 4:
								final String abreviatura = cell.getStringCellValue();
								final Escolaridade escolaridade = retrieveEscolaridade(abreviatura, escolaridades);
								if(escolaridade != null)
									proponente.setEscolaridade(escolaridade);
								else
									erros.append("Coluna Escolaridade; ");
								break;
							case 5:
								final Idade idade = retrieveFaixaIdade(faixasIdade, cell.getDateCellValue());
								if(idade != null)
									proponente.setIdade(idade);
								else
									erros.append("Coluna Idade; ");
								break;
							case 6:
								final EstadoCivil estadoCivil = retrieveEstadoCivil(cell.getStringCellValue());
								if(estadoCivil != null)
									proponente.setEstadoCivil(estadoCivil);
								else
									erros.append("Coluna Estado Civil; ");
								break;
							case 7:
								final String logradouro = cell.getStringCellValue();
								if(StringUtils.isNotEmpty(logradouro))
									proponente.setLogradouro(logradouro);
								else
									erros.append("Coluna Logradouro; ");
								break;
							case 8:
								estadoResidencia = retrieveEstado(cell.getStringCellValue());
								break;
							case 9:
								if(estadoResidencia != null)
									cidadeResidencia = retrieveCidade(cell.getStringCellValue(), estadoResidencia);
								else
									erros.append("Coluna Estado;");
								break;
							case 10:
								if(cidadeResidencia != null) {
									final Bairro bairro = retrieveBairro(cell.getStringCellValue(), cidadeResidencia);
									if(bairro != null)
										proponente.setBairro(bairro);
									else
										erros.append("Coluna Bairro; ");
								} else
									erros.append("Coluna Cidade; ");
								break;
							case 11:
								String cep = cell.getCellType() == Cell.CELL_TYPE_NUMERIC ? String.format("%.0f", cell.getNumericCellValue())
									: cell.getStringCellValue();

								if(StringUtils.isNotEmpty(cep)) {
									cep = format("##.###-###", cep);
									proponente.setCep(cep);
								} else
									erros.append("Coluna CEP; ");
								break;
							case 12:
								final TempoDomicilio tempo = retrieveTempoDomicilio(faixasTempoDomicilio, (int) cell.getNumericCellValue());
								if(tempo != null)
									proponente.setTempoDomicilio(tempo);
								else
									erros.append("Coluna tempo domicilio; ");
								break;
							case 13:
								final TipoImovel tipoImovel = retrieveTipoImovel(cell.getStringCellValue());
								if(tipoImovel != null)
									proponente.setTipoImovel(tipoImovel);
								else
									erros.append("Coluna tipo imóvel; ");
								break;
							case 14:
								estadoNaturalidade = retrieveEstado(cell.getStringCellValue());
								break;
							case 15:
								if(estadoNaturalidade != null) {
									final Cidade cidadeNatal = retrieveCidade(cell.getStringCellValue(), estadoNaturalidade);
									if(cidadeNatal != null)
										proponente.setCidadeNatal(cidadeNatal);
									else
										erros.append("Coluna cidade naturalidade; ");
								} else
									erros.append("Coluna estado naturalidade; ");
								break;
							case 16:
								final Renda faixaRenda = retrieveRenda(faixasRenda, cell.getNumericCellValue());
								if(faixaRenda != null)
									proponente.setRenda(faixaRenda);
								else
									erros.append("Coluna Renda; ");
								break;
							case 17:
								proponente.setNumFilhos((int) cell.getNumericCellValue());
								break;
							case 18:
								proponente.setNumDependentesDiretos((int) cell.getNumericCellValue());
								break;
							case 19:
								estadoDomicilioEleitoral = retrieveEstado(cell.getStringCellValue());
								break;
							case 20:
								if(estadoDomicilioEleitoral != null)
									cidadeDomicilioEleitoral = retrieveCidade(cell.getStringCellValue(), estadoDomicilioEleitoral);
								else
									erros.append("Coluna estado domicílio eleitoral; ");
								break;
							case 21:
								final String zona = cell.getCellType() == Cell.CELL_TYPE_NUMERIC ? String.format("%.0f", cell.getNumericCellValue())
									: cell.getStringCellValue();

								if(cidadeDomicilioEleitoral != null) {
									final DomicilioEleitoral domicilioEleitoral = retrieveDomicilioEleitoral(zona, cidadeDomicilioEleitoral);
									if(domicilioEleitoral != null)
										proponente.setDomicilioEleitoral(domicilioEleitoral);
									else
										erros.append("Coluna Zona;  ");
								} else
									erros.append("Coluna cidade domicílio eleitoral;  ");
								break;
							case 22:
								final String isServidor = cell.getStringCellValue();
								if(StringUtils.isNotEmpty(isServidor))
									proponente.setServidorPublico(isServidor.toLowerCase().equals("sim"));
								else
									erros.append("Coluna é servidor; ");
								break;
						}
					}
				}

				if(erros.length() > 0) {
					final Cell cellErros = row.createCell(23);
					cellErros.setCellValue(erros.toString());
					invalidRows.add(row);
				} else if(row.getRowNum() != 0) {
					proponente.setSenha(getSenhaAleatoria());
					proponente.setSenha2(proponente.getSenha());
					mailerService.send("Nova senha",
						"Seu usuário foi cadastrado com sucesso! Sua nova senha é: " + proponente.getSenha(), proponente.getEmail());
					save(proponente);
				}

			}
		} catch(final Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			sheet.getWorkbook().close();
		}

		if(invalidRows.size() > 0) {
			final DocumentoPendente documentoPendente = mountFileUsuariosInvalidos(invalidRows);
			throw new ImportacaoExcelException("documento-pendente/download/" + documentoPendente.getId());
		}
	}

	private void retrieveTitulosColunasExcel(final Row row) {
		final String[] titulos = { "Nome", "Email", "CPF", "Sexo", "Sigla Escolaridade", "Data Nascimento", "Estado Civil", "Rua", "Sigla Estado", "Cidade",
			"Bairro", "CEP", "Tempo domícilio (em anos)", "Tipo imóvel", "Sigla Estado Naturalidade", "Cidade Naturalidade", "Renda", "Nº filhos",
			"Nº dependentes diretos", "Sigla Estado Domicílio Eletiroal", "Cidade Domicílio Eleitoral", "Zona", "É servidor público?", "Erros" };

		for(int i = 0; i < titulos.length; i++) {
			final Cell cell = row.createCell(i);
			cell.setCellValue(titulos[i]);
		}
	}

	private void fillEmptyCells(final Row row) {
		final ArrayList<Cell> cells = Lists.newArrayList(row.cellIterator());
		if(cells.size() < 23) {
			for(int i = 0; i < 23; i++) {
				if(row.getCell(i) == null)
					row.createCell(i);
			}
		}
	}

	private Bairro retrieveBairro(final String nomeBairro, final Cidade cidade) {
		return bairroService.findByNome(nomeBairro, cidade.getId());
	}

	private Cidade retrieveCidade(final String nomeCidade, final Estado estado) {
		return cidadeService.findByNameAndState(nomeCidade, estado.getId());
	}

	private Estado retrieveEstado(final String siglaEstado) {
		return estadoService.findBySigla(siglaEstado);
	}

	private Escolaridade retrieveEscolaridade(final String abreviatura, final Iterable<Escolaridade> escolaridades) {
		for(final Escolaridade escolaridade : escolaridades) {
			if(escolaridade.getAbreviatura().toUpperCase().equals(abreviatura.trim().toUpperCase()))
				return escolaridade;
		}
		return null;
	}

	private Idade retrieveFaixaIdade(final Iterable<Idade> faixasIdade, final Date dataNascimento) {
		if(dataNascimento != null) {
			final int valor = getAge(dataNascimento);

			for(final Idade idade : faixasIdade) {
				if((idade.getMaxIdade() == 0 || valor >= idade.getMaxIdade())
					&& (idade.getMinIdade() == 0 || valor <= idade.getMinIdade()))
					return idade;
			}
		}
		return null;
	}

	private int getAge(final Date birthDate) {
		final long ageInMillis = new Date().getTime() - birthDate.getTime();
		final Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(ageInMillis);
		return cal.get(Calendar.YEAR);
	}

	private EstadoCivil retrieveEstadoCivil(final String nomeEstadoCivil) {
		return estadoCivilService.findByNome(nomeEstadoCivil);
	}

	private Renda retrieveRenda(final Iterable<Renda> faixasRenda, final Double valor) {
		for(final Renda renda : faixasRenda) {
			if((renda.getMaxRenda() == null || valor >= renda.getMaxRenda())
				&& (renda.getMinRenda() == null || valor <= renda.getMinRenda()))
				return renda;
		}
		return null;
	}

	private TempoDomicilio retrieveTempoDomicilio(final Iterable<TempoDomicilio> faixasTempoDomicilio, final int valor) {

		for(final TempoDomicilio tempoDomicilio : faixasTempoDomicilio) {
			if((tempoDomicilio.getMaxTempoDomicilio() == 0 || valor >= tempoDomicilio.getMaxTempoDomicilio())
				&& (tempoDomicilio.getMinTempoDomicilio() == 0 || valor <= tempoDomicilio.getMinTempoDomicilio()))
				return tempoDomicilio;
		}
		return null;
	}

	private TipoImovel retrieveTipoImovel(final String tipoImovel) {
		return tipoImovelService.findByNome(tipoImovel);
	}

	private DomicilioEleitoral retrieveDomicilioEleitoral(final String zona, final Cidade cidadeDomicilioEleitoral) {
		return domicilioEleitoralService.findByZona(zona, cidadeDomicilioEleitoral.getId());
	}

	private String getSenhaAleatoria() {
		final UUID uuid = UUID.randomUUID();
		final String myRandom = uuid.toString();
		return myRandom.substring(0, 6);
	}

	private DocumentoPendente mountFileUsuariosInvalidos(final List<Row> invalidRows) throws Exception {
		final XSSFWorkbook workbook = new XSSFWorkbook();
		final XSSFSheet sheet = workbook.createSheet("Proposições inválidas");
		int rownum = 1;

		final Row titulos = sheet.createRow(0);
		retrieveTitulosColunasExcel(titulos);

		for(final Row invalidRow : invalidRows) {

			final Iterator<Cell> cellIterator = invalidRow.cellIterator();

			while(cellIterator.hasNext()) {

				final Cell oldCell = cellIterator.next();
				Row row = sheet.getRow(rownum);
				if(row == null)
					row = sheet.createRow(rownum);

				final Cell cell = row.createCell(oldCell.getColumnIndex());

				switch(oldCell.getCellType()) {
					case Cell.CELL_TYPE_BLANK:
						cell.setCellValue(oldCell.getStringCellValue());
						break;
					case Cell.CELL_TYPE_BOOLEAN:
						cell.setCellValue(oldCell.getBooleanCellValue());
						break;
					case Cell.CELL_TYPE_NUMERIC:
						cell.setCellValue(oldCell.getNumericCellValue());
						break;
					case Cell.CELL_TYPE_STRING:
						cell.setCellValue(oldCell.getRichStringCellValue());
						break;
				}
			}
			rownum++;
		}

		final ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			workbook.write(bos);
		} finally {
			bos.close();
			workbook.close();
		}
		final DocumentoPendente documentoPendente = new DocumentoPendente();
		documentoPendente.setNome("proponentes_invalidos" + new Date().getTime() + ".xlsx");
		documentoPendente.setConteudo(bos.toByteArray());
		return documentoPendenteService.save(documentoPendente);
	}

}
