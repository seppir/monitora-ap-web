package br.ufsc.monitoraap.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufsc.monitoraap.model.Cidade;
import br.ufsc.monitoraap.repository.CidadeRepository;
import br.ufsc.monitoraap.service.CidadeService;

@Service
@Transactional
public class CidadeServiceImpl extends AbstractServiceImpl<Cidade, Integer> implements CidadeService {

	@Autowired
	public CidadeServiceImpl(final CidadeRepository repository) {
		super(repository);
	}

	@Override
	public Iterable<Cidade> findByState(final Integer idEstado) {
		return ((CidadeRepository) repository).findByState(idEstado);
	}

	@Override
	public Cidade findByNameAndState(final String nomeCidade, final Integer idEstado) {
		return ((CidadeRepository) repository).findByNameAndState(nomeCidade, idEstado);
	}

	@Override
	public Cidade findByNome(final String nome) {
		return ((CidadeRepository) repository).findByNome(nome);
	}

}
