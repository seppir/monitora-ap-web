package br.ufsc.monitoraap.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufsc.monitoraap.model.Administrador;
import br.ufsc.monitoraap.repository.AdministradorRepository;
import br.ufsc.monitoraap.service.AdministradorService;
import br.ufsc.monitoraap.service.PermissaoService;
import br.ufsc.monitoraap.service.PermissaoService.Permissao;

@Service
@Transactional
public class AdministradorServiceImpl extends AbstractServiceImpl<Administrador, Integer> implements AdministradorService {

	@Autowired
	private PermissaoService permissaoService;

	@Autowired
	public AdministradorServiceImpl(final AdministradorRepository repository) {
		super(repository);
	}

	@Override
	public Administrador findOne(final Integer id) {
		return repository.findOne(id);
	}

	@Override
	public Administrador save(final Administrador entity) {
		if(entity.getId() == null)
			entity.setPermissoes(permissaoService.getIds(Permissao.ADMINISTRADOR));
		return super.save(entity);
	}

	@Override
	public void delete(final Integer id) {
		repository.delete(id);
	}

}
