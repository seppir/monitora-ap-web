package br.ufsc.monitoraap.service.impl;

import org.apache.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

import br.ufsc.monitoraap.service.SolrService;

@Service
public class SolrServiceImpl implements SolrService {

	protected final Logger log = Logger.getLogger(getClass());

	private static final String HOST_ADDRESS = "http://localhost:8081";
	private static final String APSEARCH_ADDRESS = "/apsearch/APService";
	private static final String URL = HOST_ADDRESS + APSEARCH_ADDRESS;

	private static final String OPTION_INDEXING = "indexing";

	private final RestOperations rest = new RestTemplate();

	private String addParams(final String... params) {
		final StringBuffer buffer = new StringBuffer(URL).append("?");
		for(final String param : params) {
			buffer
				.append(param)
				.append("={")
				.append(param)
				.append("}&");
		}
		return buffer
			.deleteCharAt(buffer.length() - 1)
			.toString();
	}

	@Override
	public void executeIndexing(final Integer id) {
		log.debug("SOLR: Indexando proposição [" + id + "]");
		final ResponseEntity<String> response = rest.getForEntity(addParams("option", "id"), String.class, OPTION_INDEXING, id);
		log.debug("SOLR: Resultado indexação proposição [" + id + "]: " + response.getBody());
	}

}
