package br.ufsc.monitoraap.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufsc.monitoraap.model.TempoDomicilio;
import br.ufsc.monitoraap.repository.TempoDomicilioRepository;
import br.ufsc.monitoraap.service.TempoDomicilioService;

@Service
@Transactional
public class TempoDomicilioServiceImpl extends AbstractServiceImpl<TempoDomicilio, Integer> implements TempoDomicilioService {

	@Autowired
	public TempoDomicilioServiceImpl(final TempoDomicilioRepository repository) {
		super(repository);
	}

}
