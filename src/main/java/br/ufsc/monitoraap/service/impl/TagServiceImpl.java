package br.ufsc.monitoraap.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufsc.monitoraap.model.Tag;
import br.ufsc.monitoraap.repository.TagRepository;
import br.ufsc.monitoraap.service.TagService;

@Service
@Transactional
public class TagServiceImpl extends AbstractServiceImpl<Tag, Integer> implements TagService {

	@Autowired
	public TagServiceImpl(final TagRepository repository) {
		super(repository);
	}

	@Override
	public Tag findByValor(final String valor) {
		return ((TagRepository) repository).findByValor(valor);
	}

}
