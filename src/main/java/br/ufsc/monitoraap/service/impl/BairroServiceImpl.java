package br.ufsc.monitoraap.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufsc.monitoraap.model.Bairro;
import br.ufsc.monitoraap.repository.BairroRepository;
import br.ufsc.monitoraap.service.BairroService;

@Service
@Transactional
public class BairroServiceImpl extends AbstractServiceImpl<Bairro, Integer> implements BairroService {

	@Autowired
	public BairroServiceImpl(final BairroRepository repository) {
		super(repository);
	}

	@Override
	public Iterable<Bairro> findByCity(final Integer cityId) {
		return ((BairroRepository) repository).findByCity(cityId);
	}

	@Override
	public Bairro findByNome(final String nomeBairro, final Integer idCidade) {
		return ((BairroRepository) repository).findByNome(nomeBairro, idCidade);
	}

	@Override
	public Bairro findOneWithAllData(final Integer id) {
		return ((BairroRepository) repository).findOneWithAllData(id);
	}

	@Override
	public Iterable<Bairro> findByCityName(final String cityName) {
		return ((BairroRepository) repository).findByCityName(cityName);
	}
}
