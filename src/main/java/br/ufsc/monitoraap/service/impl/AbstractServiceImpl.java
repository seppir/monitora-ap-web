package br.ufsc.monitoraap.service.impl;

import java.io.Serializable;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import br.ufsc.monitoraap.model.Entidade;
import br.ufsc.monitoraap.service.AbstractService;

@Service
@Transactional
public abstract class AbstractServiceImpl<T extends Entidade, ID extends Serializable> implements AbstractService<T, ID> {

	protected final Logger log = Logger.getLogger(getClass());

	protected JpaRepository<T, ID> repository;

	public AbstractServiceImpl(final JpaRepository<T, ID> repository) {
		this.repository = repository;
	}

	@Override
	public T findOne(final ID id) {
		return repository.findOne(id);
	}

	@Override
	public Page<T> getAllPaged(final Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Iterable<T> getAll() {
		return repository.findAll();
	}

	@Override
	public T save(final T entity) {
		return repository.save(entity);
	}

	@Override
	public void delete(final ID id) {
		repository.delete(id);
	}

}
