package br.ufsc.monitoraap.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufsc.monitoraap.model.TipoImovel;
import br.ufsc.monitoraap.repository.TipoImovelRepository;
import br.ufsc.monitoraap.service.TipoImovelService;

@Service
@Transactional
public class TipoImovelServiceImpl extends AbstractServiceImpl<TipoImovel, Integer> implements TipoImovelService {

	@Autowired
	public TipoImovelServiceImpl(final TipoImovelRepository repository) {
		super(repository);
	}
	
	@Override
	public TipoImovel findByNome(String nomeTipoImovel){
		return ((TipoImovelRepository) repository).findByNome(nomeTipoImovel);
	}

}
