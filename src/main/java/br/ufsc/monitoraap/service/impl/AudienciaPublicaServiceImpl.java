package br.ufsc.monitoraap.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.ufsc.monitoraap.model.AudienciaPublica;
import br.ufsc.monitoraap.model.Documento;
import br.ufsc.monitoraap.model.Tag;
import br.ufsc.monitoraap.model.Usuario;
import br.ufsc.monitoraap.repository.AudienciaPublicaRepository;
import br.ufsc.monitoraap.service.AudienciaPublicaService;
import br.ufsc.monitoraap.service.DocumentoService;
import br.ufsc.monitoraap.service.TagService;

@Service
@Transactional
public class AudienciaPublicaServiceImpl extends AbstractServiceImpl<AudienciaPublica, Integer> implements AudienciaPublicaService {

	@Autowired
	private TagService tagService;

	@Autowired
	private DocumentoService documentoService;

	@Autowired
	public AudienciaPublicaServiceImpl(final AudienciaPublicaRepository repository) {
		super(repository);
	}

	@Override
	public AudienciaPublica save(final AudienciaPublica entity) {
		if(entity.getId() == null)
			entity.setDataCriacao(Calendar.getInstance().getTime());

		final int qtdTags = CollectionUtils.size(entity.getTags());
		if(qtdTags > 0) {
			final List<Tag> doppelgangers = new ArrayList<>(qtdTags);
			for(final Iterator<Tag> iterator = entity.getTags().iterator(); iterator.hasNext();) {
				final Tag tag = iterator.next();
				if(tag.getId() == null) {
					final Tag doppelganger = tagService.findByValor(tag.getValor());
					if(doppelganger != null) {
						iterator.remove();
						doppelgangers.add(doppelganger);
						continue;
					} else {
						tagService.save(tag);
					}
				}
			}
			if(CollectionUtils.isNotEmpty(doppelgangers))
				entity.getTags().addAll(doppelgangers);
		}

		super.save(entity);

		if(CollectionUtils.isNotEmpty(entity.getDocumentos()))
			for(final Documento documento : entity.getDocumentos()) {
				if(documento.getId() == null) {
					documento.setIdAp(entity.getId());
					documentoService.save(documento);
				}
			}

		return entity;
	}

	@Override
	public AudienciaPublica save(final Usuario usuarioCadastrador, final AudienciaPublica audienciaPublica) {
		if(audienciaPublica.getId() == null || audienciaPublica.getCriador() == null)
			audienciaPublica.setCriador(usuarioCadastrador);
		return save(audienciaPublica);
	}

	@Override
	public Iterable<AudienciaPublica> findAllWithCreator() {
		return ((AudienciaPublicaRepository) repository).findAllWithcreator();
	}

	@Override
	public AudienciaPublica findOneWithAllData(final Integer id) {
		return ((AudienciaPublicaRepository) repository).findOneWithCreator(id);
	}

	@Override
	public AudienciaPublica findOneWithCreator(final Integer id) {
		return ((AudienciaPublicaRepository) repository).findOneWithCreator(id);
	}

	@Override
	public AudienciaPublica findByCodAP(final String codAp) {
		return ((AudienciaPublicaRepository) repository).findByCodAP(codAp);
	}

	@Override
	public Iterable<AudienciaPublica> findFromCreator(final Integer idCriador) {
		return ((AudienciaPublicaRepository) repository).findFromCreator(idCriador);
	}

}
