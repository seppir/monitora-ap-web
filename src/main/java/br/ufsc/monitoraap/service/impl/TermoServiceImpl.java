package br.ufsc.monitoraap.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.ufsc.monitoraap.exception.impl.DescricaoTermoInvalidaException;
import br.ufsc.monitoraap.exception.impl.EntidadeNaoEncontradaException;
import br.ufsc.monitoraap.model.Termo;
import br.ufsc.monitoraap.model.TermoAudienciaPublica;
import br.ufsc.monitoraap.model.TermoAudienciaPublica.TermoAudienciaPublicaPK;
import br.ufsc.monitoraap.repository.TermoAudienciaPublicaRepository;
import br.ufsc.monitoraap.repository.TermoRepository;
import br.ufsc.monitoraap.service.TermoService;

@Service
@Transactional
public class TermoServiceImpl extends AbstractServiceImpl<Termo, Integer> implements TermoService {

	private final TermoAudienciaPublicaRepository termoAudienciaPublicaRepository;

	@Autowired
	public TermoServiceImpl(final TermoRepository repository, final TermoAudienciaPublicaRepository termoAudienciaPublicaRepository) {
		super(repository);
		this.termoAudienciaPublicaRepository = termoAudienciaPublicaRepository;
	}

	@Override
	public void setValidade(final Integer idTermo, final boolean valido) {
		final Termo termo = findOne(idTermo);
		if(termo == null)
			throw new EntidadeNaoEncontradaException(Termo.class, idTermo);
		final List<TermoAudienciaPublica> termosAp = termoAudienciaPublicaRepository.findByTermo(termo.getId());
		boolean updated = false;
		for(final TermoAudienciaPublica termoAudienciaPublica : termosAp)
			if(termoAudienciaPublica.getValidoParaCp() == null) {
				termoAudienciaPublica.setValidoParaCp(valido);
				updated = true;
			}
		if(updated)
			termoAudienciaPublicaRepository.save(termosAp);
		termo.setValido(valido);
		save(termo);
	}

	@Override
	public void setDescricao(final Integer idTermo, final String descricao) {
		final Termo termo = findOne(idTermo);
		if(termo == null)
			throw new EntidadeNaoEncontradaException(Termo.class, idTermo);
		if(StringUtils.isBlank(descricao))
			throw new DescricaoTermoInvalidaException();
		termo.setDescricao(StringUtils.trimToEmpty(descricao));
		save(termo);
	}

	@Override
	public void setValidadeAudienciaPublica(final Integer idAp, final Integer idTermoAudienciaPublica, final boolean valido) {
		final TermoAudienciaPublicaPK id = new TermoAudienciaPublicaPK(idAp, findOne(idTermoAudienciaPublica));
		final TermoAudienciaPublica termo = termoAudienciaPublicaRepository.findOne(id);
		if(termo == null)
			throw new EntidadeNaoEncontradaException(TermoAudienciaPublica.class, id);
		if(termo.getTermoAudienciaPublicaPk().getTermo().getValido() == null)
			termo.getTermoAudienciaPublicaPk().getTermo().setValido(valido);
		termo.setValidoParaCp(valido);
		termoAudienciaPublicaRepository.save(termo);
	}

	@Override
	public void setDescricaoAudienciaPublica(final Integer idAp, final Integer idTermoAudienciaPublica, final String descricao) {
		final TermoAudienciaPublicaPK id = new TermoAudienciaPublicaPK(idAp, findOne(idTermoAudienciaPublica));
		final TermoAudienciaPublica termo = termoAudienciaPublicaRepository.findOne(id);
		if(termo == null)
			throw new EntidadeNaoEncontradaException(TermoAudienciaPublica.class, id);
		if(StringUtils.isBlank(descricao))
			throw new DescricaoTermoInvalidaException();
		termo.getTermoAudienciaPublicaPk().getTermo().setDescricao(StringUtils.trimToEmpty(descricao));
		termoAudienciaPublicaRepository.save(termo);
	}

	@Override
	public Iterable<TermoAudienciaPublica> findByAudienciaPublica(final Integer idAp) {
		return termoAudienciaPublicaRepository.findByAudienciaPublica(idAp);
	}

}
