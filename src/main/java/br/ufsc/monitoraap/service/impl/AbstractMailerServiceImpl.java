package br.ufsc.monitoraap.service.impl;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;

import com.sun.mail.util.MailSSLSocketFactory;

import br.ufsc.monitoraap.service.MailerService;
import br.ufsc.monitoraap.util.mail.MailProperties;

public abstract class AbstractMailerServiceImpl implements MailerService {

	private final MailProperties mailProperties;

	protected AbstractMailerServiceImpl(final MailProperties mailProperties) {
		this.mailProperties = mailProperties;
	}

	protected void sendEmail(final String subject, final String message, final String to) throws Exception {
		if(to != null && StringUtils.isNotEmpty(to)) {
			final MailSSLSocketFactory sf = new MailSSLSocketFactory();
			sf.setTrustAllHosts(true);

			final Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", mailProperties.getHostName());
			props.put("mail.smtp.port", mailProperties.getSmtpPort());
			props.put("mail.smtp.ssl.socketFactory", sf);

			final Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(mailProperties.getFrom(), mailProperties.getPassword());
					}
				});

			final Message msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(mailProperties.getFrom()));
			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
			msg.setSubject(subject);
			msg.setContent(message, MediaType.TEXT_HTML_VALUE);

			Transport.send(msg);
		}
	}

}
