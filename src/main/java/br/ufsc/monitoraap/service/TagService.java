package br.ufsc.monitoraap.service;

import br.ufsc.monitoraap.model.Tag;

public interface TagService extends AbstractService<Tag, Integer> {

	Tag findByValor(String valor);

}
