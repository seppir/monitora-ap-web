package br.ufsc.monitoraap.service;

import br.ufsc.monitoraap.model.Documento;
import br.ufsc.monitoraap.model.Proponente;

public interface ProponenteService extends AbstractService<Proponente, Integer> {

	void importaProponentesExcel(final Documento documento) throws Exception;

}
