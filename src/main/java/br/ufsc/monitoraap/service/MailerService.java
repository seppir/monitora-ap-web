package br.ufsc.monitoraap.service;

public interface MailerService {

	void send(String subject, String message, String to) throws Exception;

}
