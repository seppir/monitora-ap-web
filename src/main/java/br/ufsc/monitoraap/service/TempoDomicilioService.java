package br.ufsc.monitoraap.service;

import br.ufsc.monitoraap.model.TempoDomicilio;

public interface TempoDomicilioService extends AbstractService<TempoDomicilio, Integer> {}
