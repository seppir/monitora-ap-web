package br.ufsc.monitoraap.service;

import br.ufsc.monitoraap.model.Administrador;

public interface AdministradorService extends AbstractService<Administrador, Integer> {}
