package br.ufsc.monitoraap.service;

import br.ufsc.monitoraap.model.EstadoCivil;

public interface EstadoCivilService extends AbstractService<EstadoCivil, Integer> {
	
	EstadoCivil findByNome(String nomeEstadoCivil);
}
