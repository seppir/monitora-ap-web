package br.ufsc.monitoraap.service;

import br.ufsc.monitoraap.model.Politico;

public interface PoliticoService extends AbstractService<Politico, Integer> {}
