package br.ufsc.monitoraap.service;

import br.ufsc.monitoraap.model.Termo;
import br.ufsc.monitoraap.model.TermoAudienciaPublica;

public interface TermoService extends AbstractService<Termo, Integer> {

	void setValidade(Integer idTermo, boolean valido);

	void setDescricao(Integer idTermo, String descricao);

	void setValidadeAudienciaPublica(Integer idAp, Integer idTermoAudienciaPublica, boolean valido);

	void setDescricaoAudienciaPublica(Integer idAp, Integer idTermoAudienciaPublica, String descricao);

	Iterable<TermoAudienciaPublica> findByAudienciaPublica(Integer idAp);

}
