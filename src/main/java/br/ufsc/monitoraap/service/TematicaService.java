package br.ufsc.monitoraap.service;

import br.ufsc.monitoraap.model.Tematica;

public interface TematicaService extends AbstractService<Tematica, Integer> {

	Tematica findByNome(String nome);

}
