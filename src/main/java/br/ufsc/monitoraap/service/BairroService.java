package br.ufsc.monitoraap.service;

import br.ufsc.monitoraap.model.Bairro;

public interface BairroService extends AbstractService<Bairro, Integer> {

	Iterable<Bairro> findByCity(Integer cityId);

	Bairro findByNome(final String nomeBairro, Integer idCidade);

	Bairro findOneWithAllData(Integer id);

	Iterable<Bairro> findByCityName(String cityName);

}
