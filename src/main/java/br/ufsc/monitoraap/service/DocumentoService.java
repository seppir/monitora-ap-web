package br.ufsc.monitoraap.service;

import br.ufsc.monitoraap.model.Documento;

public interface DocumentoService extends AbstractService<Documento, Integer> {}
