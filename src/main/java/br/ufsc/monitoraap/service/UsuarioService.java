package br.ufsc.monitoraap.service;

import br.ufsc.monitoraap.model.Usuario;

public interface UsuarioService extends AbstractService<Usuario, Integer> {

	Usuario findByEmail(String email);

	Usuario authenticate(String email);

	Usuario findByFacebookId(String facebookId);

	void bindFacebookId(Integer id, String facebookId);

}
