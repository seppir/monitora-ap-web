package br.ufsc.monitoraap.service;

import java.io.Serializable;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.ufsc.monitoraap.model.Entidade;

public interface AbstractService<T extends Entidade, ID extends Serializable> {

	T findOne(ID id);

	Page<T> getAllPaged(Pageable pageable);

	Iterable<T> getAll();

	T save(T entity);

	void delete(ID id);

}
