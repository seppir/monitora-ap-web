package br.ufsc.monitoraap.service;

import java.util.Date;

import br.ufsc.monitoraap.dto.ExportacaoAudienciaPublicaDTO;
import br.ufsc.monitoraap.model.Documento;
import br.ufsc.monitoraap.model.DocumentoPendente;
import br.ufsc.monitoraap.model.Proponente;
import br.ufsc.monitoraap.model.Proposicao;

public interface ProposicaoService extends AbstractService<Proposicao, Integer> {

	Iterable<Proposicao> findByAp(Integer idAp);

	Iterable<Proposicao> findByUserAndAp(Integer idProponente, Integer idAp);

	Proposicao save(Proponente proponente, Proposicao proposicao);

	void importaProposicoesExcel(Integer idProponente, Date dataCadastro, Documento documento) throws Exception;

	DocumentoPendente exportarExcelParaImportacao(Iterable<ExportacaoAudienciaPublicaDTO> proposicoes) throws Exception;

}
