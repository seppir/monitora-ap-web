package br.ufsc.monitoraap.service;

import br.ufsc.monitoraap.model.TipoImovel;

public interface TipoImovelService extends AbstractService<TipoImovel, Integer> {
	
	TipoImovel findByNome(String nomeTipoImovel);
}
