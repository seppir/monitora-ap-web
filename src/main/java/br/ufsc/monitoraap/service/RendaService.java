package br.ufsc.monitoraap.service;

import br.ufsc.monitoraap.model.Renda;

public interface RendaService extends AbstractService<Renda, Integer> {}
