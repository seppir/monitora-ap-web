package br.ufsc.monitoraap.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@Table(name = "entrada_csv")
public class EntradaCsv extends Entidade {

	@Column(name = "resposta_1", length = 5000)
	@NotEmpty
	private String resposta1;

	@Column(name = "resposta_2", length = 5000)
	@NotEmpty
	private String resposta2;

	@Column(name = "email")
	@NotEmpty
	@Email
	private String email;

	@Column(name = "nome")
	@NotEmpty
	private String nome;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_idade")
	@JsonInclude(Include.NON_NULL)
	private Idade idade;

	@Column(name = "whatsapp")
	private String whatsapp;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_bairro")
	@JsonInclude(Include.NON_NULL)
	@NotNull
	private Bairro bairro;

	public String getResposta1() {
		return resposta1;
	}

	public void setResposta1(final String resposta1) {
		this.resposta1 = resposta1;
	}

	public String getResposta2() {
		return resposta2;
	}

	public void setResposta2(final String resposta2) {
		this.resposta2 = resposta2;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(final String nome) {
		this.nome = nome;
	}

	public Idade getIdade() {
		return idade;
	}

	public void setIdade(final Idade idade) {
		this.idade = idade;
	}

	public String getWhatsapp() {
		return whatsapp;
	}

	public void setWhatsapp(final String whatsapp) {
		this.whatsapp = whatsapp;
	}

	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(final Bairro bairro) {
		this.bairro = bairro;
	}

}
