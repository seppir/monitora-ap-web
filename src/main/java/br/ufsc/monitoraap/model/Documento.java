package br.ufsc.monitoraap.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "documento")
public class Documento extends Arquivo {

	@Column(name = "id_ap", updatable = false)
	@NotNull
	@JsonIgnore
	private Integer idAp;

	public Integer getIdAp() {
		return idAp;
	}

	public void setIdAp(final Integer idAp) {
		this.idAp = idAp;
	}

}
