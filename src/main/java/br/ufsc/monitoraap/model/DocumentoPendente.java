package br.ufsc.monitoraap.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "documento_pendente")
public class DocumentoPendente  extends Arquivo {}
