package br.ufsc.monitoraap.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "escolaridade")
public class Escolaridade extends Entidade {

	@Column(name = "descricao")
	@NotEmpty
	private String descricao;
	
	@Column(name = "abreviatura")
	@NotEmpty
	private String abreviatura;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(final String descricao) {
		this.descricao = descricao;
	}

	public String getAbreviatura() {
		return abreviatura;
	}

	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}
}
