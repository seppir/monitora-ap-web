package br.ufsc.monitoraap.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;

@Entity
@Table(name = "domicilio_eleitoral")
public class DomicilioEleitoral extends Entidade {

	@Column(name = "zona")
	private String zona;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_cidade")
	@JsonIgnore
	@NotNull
	private Cidade cidade;

	public String getZona() {
		return zona;
	}

	public void setZona(final String zona) {
		this.zona = zona;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(final Cidade cidade) {
		this.cidade = cidade;
	}

	@JsonGetter
	public Integer getIdCidade() {
		return cidade.getId();
	}

	@JsonSetter
	public void setIdCidade(final Integer idCidade) {
		cidade = new Cidade();
		cidade.setId(idCidade);
	}

}
