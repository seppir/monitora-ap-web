package br.ufsc.monitoraap.model;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.br.CPF;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonSetter;

import br.ufsc.monitoraap.enumeration.Sexo;

@Entity
@Table(name = "proponente")
public class Proponente extends Usuario {

	@Column(name = "cpf", unique = true, updatable = false)
	@CPF
	private String cpf;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_bairro")
	@JsonInclude(Include.NON_NULL)
	private Bairro bairro;

	@Column(name = "cep")
	private String cep;

	@Column(name = "logradouro")
	private String logradouro;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_tempo_domicilio")
	@JsonInclude(Include.NON_NULL)
	private TempoDomicilio tempoDomicilio;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_tipo_imovel")
	@JsonInclude(Include.NON_NULL)
	private TipoImovel tipoImovel;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_cidade_natal")
	@JsonInclude(Include.NON_NULL)
	private Cidade cidadeNatal;

	@Column(name = "sexo")
	@Enumerated(EnumType.STRING)
	private Sexo sexo;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_escolaridade")
	@JsonInclude(Include.NON_NULL)
	private Escolaridade escolaridade;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_idade")
	@JsonInclude(Include.NON_NULL)
	private Idade idade;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_estado_civil")
	@JsonInclude(Include.NON_NULL)
	private EstadoCivil estadoCivil;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_renda")
	@JsonInclude(Include.NON_NULL)
	private Renda renda;

	@Column(name = "num_filhos")
	private Integer numFilhos;

	@Column(name = "num_dependentes_diretos")
	private Integer numDependentesDiretos;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_domicilio_eleitoral")
	private DomicilioEleitoral domicilioEleitoral;

	@Column(name = "servidor_publico")
	private Boolean servidorPublico;

	public String getCpf() {
		return cpf;
	}

	public void setCpf(final String cpf) {
		this.cpf = cpf;
	}

	@JsonGetter
	public boolean isPossuiCPF() {
		return cpf != null && !cpf.isEmpty();
	}

	@JsonSetter
	public void setPossuiCPF() {}

	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(final Bairro bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(final String cep) {
		this.cep = cep;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(final String logradouro) {
		this.logradouro = logradouro;
	}

	public TempoDomicilio getTempoDomicilio() {
		return tempoDomicilio;
	}

	public void setTempoDomicilio(final TempoDomicilio tempoDomicilio) {
		this.tempoDomicilio = tempoDomicilio;
	}

	public TipoImovel getTipoImovel() {
		return tipoImovel;
	}

	public void setTipoImovel(final TipoImovel tipoImovel) {
		this.tipoImovel = tipoImovel;
	}

	public Cidade getCidadeNatal() {
		return cidadeNatal;
	}

	public void setCidadeNatal(final Cidade cidadeNatal) {
		this.cidadeNatal = cidadeNatal;
	}

	public Sexo getSexo() {
		return sexo;
	}

	public void setSexo(final Sexo sexo) {
		this.sexo = sexo;
	}

	public Escolaridade getEscolaridade() {
		return escolaridade;
	}

	public void setEscolaridade(final Escolaridade escolaridade) {
		this.escolaridade = escolaridade;
	}

	public Idade getIdade() {
		return idade;
	}

	public void setIdade(final Idade idade) {
		this.idade = idade;
	}

	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(final EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public Renda getRenda() {
		return renda;
	}

	public void setRenda(final Renda renda) {
		this.renda = renda;
	}

	public Integer getNumFilhos() {
		return numFilhos;
	}

	public void setNumFilhos(final Integer numFilhos) {
		this.numFilhos = numFilhos;
	}

	public Integer getNumDependentesDiretos() {
		return numDependentesDiretos;
	}

	public void setNumDependentesDiretos(final Integer numDependentesDiretos) {
		this.numDependentesDiretos = numDependentesDiretos;
	}

	public DomicilioEleitoral getDomicilioEleitoral() {
		return domicilioEleitoral;
	}

	public void setDomicilioEleitoral(final DomicilioEleitoral domicilioEleitoral) {
		this.domicilioEleitoral = domicilioEleitoral;
	}

	public Boolean getServidorPublico() {
		return servidorPublico;
	}

	public void setServidorPublico(final Boolean servidorPublico) {
		this.servidorPublico = servidorPublico;
	}

	@JsonGetter
	public boolean isNecessarioAtualizarCadastro() {
		final Date d = getDataUltimaAtualizacao();
		if(d != null) {
			final Calendar c = Calendar.getInstance();
			c.add(Calendar.MONTH, -6);
			return d.before(c.getTime());
		}
		return false;
	}

}
