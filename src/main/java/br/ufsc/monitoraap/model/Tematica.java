package br.ufsc.monitoraap.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "tematica")
public class Tematica extends Entidade {

	@Column(name = "nome", unique = true)
	@NotEmpty
	private String nome;

	public Tematica() {
		super();
	}

	public Tematica(final Integer id) {
		setId(id);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(final String nome) {
		this.nome = nome;
	}

}
