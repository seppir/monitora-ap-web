package br.ufsc.monitoraap.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "proposicao")
public class Proposicao extends Entidade {

	@Column(name = "id_ap")
	@NotNull
	private Integer idAudienciaPublica;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_proponente")
	@NotNull
	private Proponente proponente;

	@Column(name = "opiniao_conteudos", length = 5000)
	private String opiniaoConteudoAp;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinTable(name = "assoc_proposicao_tematica",
		joinColumns = {
			@JoinColumn(name = "id_proposicao", updatable = false, nullable = false)
		}, inverseJoinColumns = {
			@JoinColumn(name = "id_tematica", updatable = false, nullable = false)
		})
	private List<Tematica> sugestaoTematicas;

	@Column(name = "texto", length = 5000)
	@NotEmpty
	private String texto;

	@Column(name = "data_cadastro", updatable = false)
	@NotNull
	private Date dataCadastro;

	public Integer getIdAudienciaPublica() {
		return idAudienciaPublica;
	}

	public void setIdAudienciaPublica(final Integer idAudienciaPublica) {
		this.idAudienciaPublica = idAudienciaPublica;
	}

	public Proponente getProponente() {
		return proponente;
	}

	public void setProponente(final Proponente proponente) {
		this.proponente = proponente;
	}

	public String getOpiniaoConteudoAp() {
		return opiniaoConteudoAp;
	}

	public void setOpiniaoConteudoAp(final String opiniaoConteudoAp) {
		this.opiniaoConteudoAp = opiniaoConteudoAp;
	}

	public List<Tematica> getSugestaoTematicas() {
		return sugestaoTematicas;
	}

	public void setSugestaoTematicas(final List<Tematica> sugestaoTematicas) {
		this.sugestaoTematicas = sugestaoTematicas;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(final String texto) {
		this.texto = texto;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(final Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

}
