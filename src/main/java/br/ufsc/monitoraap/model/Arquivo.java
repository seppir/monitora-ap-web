package br.ufsc.monitoraap.model;

import java.io.File;
import java.io.IOException;

import javax.persistence.Column;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import br.ufsc.monitoraap.util.persistence.fileresource.FileResource;
import br.ufsc.monitoraap.util.persistence.fileresource.FileResourceManager;

@MappedSuperclass
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
@JsonSubTypes({
	@JsonSubTypes.Type(Documento.class)
})
public class Arquivo extends Entidade implements FileResource {

	@Column(name = "nome", updatable = false)
	private String nome;

	@Column(name = "conteudo", updatable = false)
	@Lob
	private byte[] conteudo;

	public String getNome() {
		return nome;
	}

	public void setNome(final String nome) {
		this.nome = nome;
	}

	@JsonIgnore
	public byte[] getConteudo() {
		return conteudo;
	}

	@JsonSetter
	public void setConteudo(final byte[] conteudo) {
		this.conteudo = conteudo;
	}

	@JsonGetter
	public Integer getTamanho() {
		return conteudo.length;
	}

	@Override
	public File toFile() throws IOException {
		return FileResourceManager.createFile(nome, conteudo);
	}

}
