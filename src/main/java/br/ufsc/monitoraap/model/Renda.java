package br.ufsc.monitoraap.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "renda")
public class Renda extends Entidade {

	@Column(name = "descricao")
	@NotEmpty
	private String descricao;
	
	@Column(name = "min_renda")
	private Double minRenda;
	
	@Column(name = "max_renda")
	private Double maxRenda;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(final String descricao) {
		this.descricao = descricao;
	}

	public Double getMinRenda() {
		return minRenda;
	}

	public void setMinRenda(Double minRenda) {
		this.minRenda = minRenda;
	}

	public Double getMaxRenda() {
		return maxRenda;
	}

	public void setMaxRenda(Double maxRenda) {
		this.maxRenda = maxRenda;
	}
}
