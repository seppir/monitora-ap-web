package br.ufsc.monitoraap.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "assoc_proposicao_tematica")
public class AssocProposicaoTematica extends Entidade {

	@Column(name = "id_proposicao")
	@NotNull
	private Integer idProposicao;

	@JoinColumn(name = "id_tematica")
	@ManyToOne
	private Tematica tematica;

	public AssocProposicaoTematica() {}

	public AssocProposicaoTematica(final Integer idProposicao, final Tematica tematica) {
		this.idProposicao = idProposicao;
		this.tematica = tematica;
	}

	public Integer getIdProposicao() {
		return idProposicao;
	}

	public void setIdProposicao(final Integer idProposicao) {
		this.idProposicao = idProposicao;
	}

	public Tematica getTematica() {
		return tematica;
	}

	public void setTematica(final Tematica tematica) {
		this.tematica = tematica;
	}

}
