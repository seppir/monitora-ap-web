package br.ufsc.monitoraap.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonGetter;

@Entity
@Table(name = "bairro")
public class Bairro extends Entidade {

	@Column(name = "nome")
	@NotEmpty
	private String nome;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_cidade")
	@NotNull
	private Cidade cidade;

	public String getNome() {
		return nome;
	}

	public void setNome(final String nome) {
		this.nome = nome;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public void setCidade(final Cidade cidade) {
		this.cidade = cidade;
	}

	@JsonGetter
	public String getNomeApresentacao() {
		return nome.concat(", ").concat(cidade.getNome());
	}

}
