package br.ufsc.monitoraap.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "idade")
public class Idade extends Entidade {

	@Column(name = "descricao")
	@NotEmpty
	private String descricao;

	@Column(name = "min_idade")
	private Integer minIdade;

	@Column(name = "max_idade")
	private Integer maxIdade;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(final String descricao) {
		this.descricao = descricao;
	}

	public Integer getMinIdade() {
		return minIdade;
	}

	public void setMinIdade(final Integer minIdade) {
		this.minIdade = minIdade;
	}

	public Integer getMaxIdade() {
		return maxIdade;
	}

	public void setMaxIdade(final Integer maxIdade) {
		this.maxIdade = maxIdade;
	}

}
