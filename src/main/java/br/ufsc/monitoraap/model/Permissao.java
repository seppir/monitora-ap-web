package br.ufsc.monitoraap.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

@Entity
@Table(name = "permissao")
public class Permissao extends Entidade {

	@Column(name = "nome", unique = true)
	@NotEmpty
	private String nome;

	@Column(name = "descricao")
	@NotEmpty
	private String descricao;

	public Permissao() {}

	public Permissao(final Integer id) {
		setId(id);
	}

	public GrantedAuthority toGrantedAuthority() {
		return new SimpleGrantedAuthority("ROLE_".concat(nome));
	}

	public String getNome() {
		return nome;
	}

	public void setNome(final String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(final String descricao) {
		this.descricao = descricao;
	}

}
