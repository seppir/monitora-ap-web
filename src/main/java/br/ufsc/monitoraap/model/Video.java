package br.ufsc.monitoraap.model;

import java.io.File;
import java.io.IOException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.ufsc.monitoraap.util.persistence.fileresource.FileResource;
import br.ufsc.monitoraap.util.persistence.fileresource.FileResourceManager;

@Entity
@Table(name = "video")
public class Video extends Entidade implements FileResource {

	@Column(name = "nome", updatable = false)
	private String nome;

	@Column(name = "id_ap", updatable = false)
	@NotNull
	@JsonIgnore
	private Integer idAp;

	@Column(name = "caminho_arquivo", updatable = false)
	@JsonIgnore
	private String caminhoArquivo;

	public String getNome() {
		return nome;
	}

	public void setNome(final String nome) {
		this.nome = nome;
	}

	public Integer getIdAp() {
		return idAp;
	}

	public void setIdAp(final Integer idAp) {
		this.idAp = idAp;
	}

	public String getCaminhoArquivo() {
		return caminhoArquivo;
	}

	public void setCaminhoArquivo(final String caminhoArquivo) {
		this.caminhoArquivo = caminhoArquivo;
	}

	@Override
	public File toFile() throws IOException {
		return FileResourceManager.retrieveFile(caminhoArquivo);
	}

}
