package br.ufsc.monitoraap.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@Entity
@Table(name = "usuario")
@Inheritance(strategy = InheritanceType.JOINED)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
@JsonSubTypes({
	@JsonSubTypes.Type(Administrador.class),
	@JsonSubTypes.Type(Politico.class),
	@JsonSubTypes.Type(Proponente.class)
})
public class Usuario extends Entidade {

	@Column(name = "nome")
	@NotEmpty
	private String nome;

	@Column(name = "email", unique = true)
	@NotEmpty
	@Email
	private String email;

	@Column(name = "senha")
	@NotEmpty
	@JsonIgnore
	private String senha;

	@JsonIgnore
	@Transient
	private String senha2;

	@JsonIgnore
	@Transient
	private String senhaAtualTroca;

	@JsonIgnore
	@Transient
	private String senhaTroca1;

	@JsonIgnore
	@Transient
	private String senhaTroca2;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinTable(name = "assoc_usuario_permissao",
		joinColumns = {
			@JoinColumn(name = "id_usuario", updatable = false, nullable = false)
		}, inverseJoinColumns = {
			@JoinColumn(name = "id_permissao", updatable = false, nullable = false)
		})
	@JsonIgnore
	private List<Permissao> permissoes;

	@Column(name = "habilitado")
	private boolean habilitado;

	@Column(name = "data_ultima_atualizacao")
	private Date dataUltimaAtualizacao;

	@Column(name = "facebook_id", updatable = false, unique = true)
	@JsonIgnore
	private String facebookId;

	public String getNome() {
		return nome;
	}

	public void setNome(final String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(final String senha) {
		this.senha = senha;
	}

	@JsonSetter
	public void setAutenticacao(final String senhaBase64) {
		setSenha(new String(Base64.decodeBase64(senhaBase64)));
	}

	public String getSenha2() {
		return senha2;
	}

	public void setSenha2(final String senha2) {
		this.senha2 = senha2;
	}

	@JsonSetter
	public void setAutenticacao2(final String senha2Base64) {
		setSenha2(new String(Base64.decodeBase64(senha2Base64)));
	}

	public String getSenhaAtualTroca() {
		return senhaAtualTroca;
	}

	public void setSenhaAtualTroca(final String senhaAtualTroca) {
		this.senhaAtualTroca = senhaAtualTroca;
	}

	@JsonSetter
	public void setAutenticacaoAtual(final String senhaAtualTrocaBase64) {
		setSenhaAtualTroca(new String(Base64.decodeBase64(senhaAtualTrocaBase64)));
	}

	public String getSenhaTroca1() {
		return senhaTroca1;
	}

	public void setSenhaTroca1(final String senhaTroca1) {
		this.senhaTroca1 = senhaTroca1;
	}

	@JsonSetter
	public void setAutenticacaoTroca1(final String senhaTroca1Base64) {
		setSenhaTroca1(new String(Base64.decodeBase64(senhaTroca1Base64)));
	}

	public String getSenhaTroca2() {
		return senhaTroca2;
	}

	public void setSenhaTroca2(final String senha2Troca2) {
		senhaTroca2 = senha2Troca2;
	}

	@JsonSetter
	public void setAutenticacaoTroca2(final String senhaTroca2Base64) {
		setSenhaTroca2(new String(Base64.decodeBase64(senhaTroca2Base64)));
	}

	public List<Permissao> getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(final List<Permissao> permissoes) {
		this.permissoes = permissoes;
	}

	public List<Integer> getIdPermissoes() {
		final List<Integer> idPermissoes = new ArrayList<>();
		for(final Permissao permissao : permissoes)
			idPermissoes.add(permissao.getId());
		return idPermissoes;
	}

	public boolean isHabilitado() {
		return habilitado;
	}

	public void setHabilitado(final boolean habilitado) {
		this.habilitado = habilitado;
	}

	public Date getDataUltimaAtualizacao() {
		return dataUltimaAtualizacao;
	}

	public void setDataUltimaAtualizacao(final Date dataUltimaAtualizacao) {
		this.dataUltimaAtualizacao = dataUltimaAtualizacao;
	}

	public String getFacebookId() {
		return facebookId;
	}

	@JsonSetter
	public void setFacebookId(final String facebookId) {
		this.facebookId = facebookId;
	}

	@JsonGetter
	public boolean isAutenticadoFacebook() {
		return StringUtils.isNotBlank(facebookId);
	}

}
