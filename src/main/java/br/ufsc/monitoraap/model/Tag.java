package br.ufsc.monitoraap.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "tag")
public class Tag extends Entidade {

	@Column(name = "valor", unique = true)
	@NotEmpty
	private String valor;

	public Tag() {}

	public Tag(final Integer id) {
		setId(id);
	}

	public String getValor() {
		return valor;
	}

	public void setValor(final String valor) {
		this.valor = valor;
	}

}
