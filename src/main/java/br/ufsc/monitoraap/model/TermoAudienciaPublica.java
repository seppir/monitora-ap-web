package br.ufsc.monitoraap.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@Table(name = "termo_cp")
public class TermoAudienciaPublica {

	@Embeddable
	public static class TermoAudienciaPublicaPK implements Serializable {

		private static final long serialVersionUID = 1L;

		@Column(name = "id_cp", updatable = false)
		@NotNull
		private Integer idAudienciaPublica;

		@JoinColumn(name = "id", updatable = false)
		@ManyToOne(fetch = FetchType.EAGER)
		@NotNull
		private Termo termo;

		public TermoAudienciaPublicaPK() {}

		public TermoAudienciaPublicaPK(final Integer idAudienciaPublica, final Termo termo) {
			this.idAudienciaPublica = idAudienciaPublica;
			this.termo = termo;
		}

		public Integer getIdAudienciaPublica() {
			return idAudienciaPublica;
		}

		public void setIdAudienciaPublica(final Integer idAudienciaPublica) {
			this.idAudienciaPublica = idAudienciaPublica;
		}

		public Termo getTermo() {
			return termo;
		}

		public void setTermo(final Termo termo) {
			this.termo = termo;
		}

		@Override
		public String toString() {
			return ToStringBuilder.reflectionToString(this);
		}

		@Override
		public boolean equals(final Object obj) {
			if(obj == null || !(obj instanceof TermoAudienciaPublicaPK))
				return false;
			final TermoAudienciaPublicaPK other = (TermoAudienciaPublicaPK) obj;
			return idAudienciaPublica.equals(other.getIdAudienciaPublica()) && termo.equals(other.getTermo());
		}

	}

	@EmbeddedId
	private TermoAudienciaPublicaPK termoAudienciaPublicaPk;

	@Column(name = "valido")
	private Boolean validoParaCp;

	public TermoAudienciaPublicaPK getTermoAudienciaPublicaPk() {
		return termoAudienciaPublicaPk;
	}

	public void setTermoAudienciaPublicaPk(final TermoAudienciaPublicaPK termoAudienciaPublicaPk) {
		this.termoAudienciaPublicaPk = termoAudienciaPublicaPk;
	}

	public Boolean getValidoParaCp() {
		return validoParaCp;
	}

	public void setValidoParaCp(final Boolean validoParaCp) {
		this.validoParaCp = validoParaCp;
	}

}
