package br.ufsc.monitoraap.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "estado")
public class Estado extends Entidade {

	@Column(name = "sigla", unique = true)
	@NotEmpty
	private String sigla;

	@Column(name = "nome")
	@NotEmpty
	private String nome;

	public String getSigla() {
		return sigla;
	}

	public void setSigla(final String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(final String nome) {
		this.nome = nome;
	}

}
