package br.ufsc.monitoraap.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "tempo_domicilio")
public class TempoDomicilio extends Entidade {

	@Column(name = "descricao")
	@NotEmpty
	private String descricao;

	@Column(name = "min_tempo_domicilio")
	@JsonIgnore
	private Integer minTempoDomicilio;

	@Column(name = "max_tempo_domicilio")
	@JsonIgnore
	private Integer maxTempoDomicilio;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(final String descricao) {
		this.descricao = descricao;
	}

	public Integer getMinTempoDomicilio() {
		return minTempoDomicilio;
	}

	public void setMinTempoDomicilio(final Integer minTempoDomicilio) {
		this.minTempoDomicilio = minTempoDomicilio;
	}

	public Integer getMaxTempoDomicilio() {
		return maxTempoDomicilio;
	}

	public void setMaxTempoDomicilio(final Integer maxTempoDomicilio) {
		this.maxTempoDomicilio = maxTempoDomicilio;
	}

}
