package br.ufsc.monitoraap.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "termo")
public class Termo extends Entidade {

	@Column(name = "descricao", length = 100)
	private String descricao;

	@Column(name = "valido")
	private Boolean valido;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(
		name = "termo_referencia",
		joinColumns = {
			@JoinColumn(name = "id_pai", updatable = false)
		}, inverseJoinColumns = {
			@JoinColumn(name = "id", updatable = false)
		})
	private List<Termo> termosFilho;

	@Override
	public boolean equals(final Object obj) {
		if(!(obj instanceof Termo))
			return super.equals(obj);
		return getId().equals(((Termo) obj).getId()) || super.equals(obj);
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(final String descricao) {
		this.descricao = descricao;
	}

	public Boolean getValido() {
		return valido;
	}

	public void setValido(final Boolean valido) {
		this.valido = valido;
	}

	public List<Termo> getTermosFilho() {
		return termosFilho;
	}

	public void setTermosFilho(final List<Termo> termosFilho) {
		this.termosFilho = termosFilho;
	}

}
