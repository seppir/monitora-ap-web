package br.ufsc.monitoraap.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Formula;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import br.ufsc.monitoraap.enumeration.Status;

@Entity
@Table(name = "audiencia_publica")
@SQLDelete(sql = "UPDATE audiencia_publica SET deletada = 1 WHERE id = ?")
@Where(clause = "deletada = 0")
public class AudienciaPublica extends Entidade {

	@Column(name = "codigo", unique = true)
	@NotEmpty
	private String codigo;

	@Column(name = "nome")
	@NotEmpty
	private String nome;

	@Column(name = "descricao", length = 5000)
	@NotEmpty
	private String descricao;

	@Column(name = "data_criacao", updatable = false)
	@NotNull
	private Date dataCriacao;

	@Column(name = "data_inicio")
	@NotNull
	private Date dataInicio;

	@Column(name = "data_encerramento")
	@NotNull
	private Date dataEncerramento;

	@Column(name = "cancelada")
	private boolean cancelada;

	@JoinColumn(name = "id_criador", updatable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	@NotNull
	private Usuario criador;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinTable(name = "assoc_ap_tag",
		joinColumns = {
			@JoinColumn(name = "id_ap", updatable = false, nullable = false)
		}, inverseJoinColumns = {
			@JoinColumn(name = "id_tag", updatable = false, nullable = false)
		})
	private Set<Tag> tags;

	@Column(name = "usuarios_identificados")
	private boolean usuariosIdentificados;

	@Formula("(SELECT COUNT(p.id) FROM proposicao p WHERE p.id_ap = id)")
	private Integer numProposicoes;

	@Column(name = "deletada")
	private boolean deletada;

	@OneToMany(mappedBy = "idAp", fetch = FetchType.EAGER, cascade = { CascadeType.MERGE, CascadeType.REMOVE })
	private Set<Documento> documentos;

	@OneToMany(mappedBy = "idAp", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JsonIgnore
	private Set<Video> videos;

	@Column(name = "max_props_usuario")
	@Range(min = 1)
	private Integer maxProposicoesUsuario;

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(
		name = "videos_youtube",
		joinColumns = {
			@JoinColumn(name = "id_ap")
		})
	@Column(name = "id_video")
	private Set<String> videosYoutube;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(final String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(final String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(final String descricao) {
		this.descricao = descricao;
	}

	@JsonGetter
	public String getTexto() {
		return getNome().concat(" ").concat(getDescricao());
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(final Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(final Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataEncerramento() {
		return dataEncerramento;
	}

	public void setDataEncerramento(final Date dataEncerramento) {
		this.dataEncerramento = dataEncerramento;
	}

	@JsonGetter
	public Status getStatus() {
		final Date dataAtual = new Date();

		if(isCancelada())
			return Status.CANCELADA;
		else if(getDataInicio().before(dataAtual)) {
			if(getDataEncerramento().after(dataAtual))
				return Status.EM_ANDAMENTO;
			else
				return Status.ENCERRADA;
		} else
			return Status.AGENDADA;
	}

	public boolean isCancelada() {
		return cancelada;
	}

	public void setCancelada(final boolean cancelada) {
		this.cancelada = cancelada;
	}

	public Usuario getCriador() {
		return criador;
	}

	public void setCriador(final Usuario criador) {
		this.criador = criador;
	}

	public Set<Tag> getTags() {
		return tags;
	}

	public void setTags(final Set<Tag> tags) {
		this.tags = tags;
	}

	public boolean isUsuariosIdentificados() {
		return usuariosIdentificados;
	}

	public void setUsuariosIdentificados(final boolean usuariosIdentificados) {
		this.usuariosIdentificados = usuariosIdentificados;
	}

	public Integer getNumProposicoes() {
		return numProposicoes;
	}

	public void setNumProposicoes(final Integer numProposicoes) {
		this.numProposicoes = numProposicoes;
	}

	public boolean isDeletada() {
		return deletada;
	}

	public Set<Documento> getDocumentos() {
		return documentos;
	}

	public void setDocumentos(final Set<Documento> documentos) {
		this.documentos = documentos;
	}

	@JsonProperty
	public Set<Video> getVideos() {
		return videos;
	}

	@JsonIgnore
	public void setVideos(final Set<Video> videos) {
		this.videos = videos;
	}

	public Integer getMaxProposicoesUsuario() {
		return maxProposicoesUsuario;
	}

	public void setMaxProposicoesUsuario(final Integer maxProposicoesUsuario) {
		this.maxProposicoesUsuario = maxProposicoesUsuario;
	}

	public Set<String> getVideosYoutube() {
		return videosYoutube;
	}

	public void setVideosYoutube(final Set<String> videosYoutube) {
		this.videosYoutube = videosYoutube;
	}

}
