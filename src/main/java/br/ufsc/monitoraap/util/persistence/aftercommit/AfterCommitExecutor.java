package br.ufsc.monitoraap.util.persistence.aftercommit;

import java.util.concurrent.Executor;

public interface AfterCommitExecutor extends Executor {}
