package br.ufsc.monitoraap.util.persistence.aftercommit.annotation;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.ufsc.monitoraap.util.persistence.aftercommit.AfterCommitExecutor;

@Aspect
@Component
public class AfterCommitAnnotationAspect {

	private static final Logger log = Logger.getLogger(AfterCommitAnnotationAspect.class);

	@Autowired
	private AfterCommitExecutor afterCommitExecutor;

	@Around(value = "@annotation(br.ufsc.monitoraap.util.persistence.aftercommit.annotation.AfterCommit)", argNames = "pjp")
	public Object aroundAdvice(final ProceedingJoinPoint pjp) {
		afterCommitExecutor.execute(new PjpAfterCommitRunnable(pjp));
		return null;
	}

	private static final class PjpAfterCommitRunnable implements Runnable {

		private final ProceedingJoinPoint pjp;

		public PjpAfterCommitRunnable(final ProceedingJoinPoint pjp) {
			this.pjp = pjp;
		}

		@Override
		public void run() {
			try {
				pjp.proceed();
			} catch(final Throwable e) {
				log.error("Exception while invoking pjp.proceed()", e);
				throw new RuntimeException(e);
			}
		}

		@Override
		public String toString() {
			final String typeName = pjp.getTarget().getClass().getSimpleName();
			final String methodName = pjp.getSignature().getName();
			return "PjpAfterCommitRunnable[type=" + typeName + ", method=" + methodName + "]";
		}

	}

}
