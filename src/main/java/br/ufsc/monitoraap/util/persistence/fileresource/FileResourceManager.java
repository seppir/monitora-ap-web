package br.ufsc.monitoraap.util.persistence.fileresource;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import br.ufsc.monitoraap.service.SistemaService;

@Component
public class FileResourceManager {

	public static final String FILE_RESOURCE_PATH_PATTERN = "%s/FileResources/%s/";

	public static File createFile(final String name, final byte[] content) throws IOException {
		final String[] partesNome = StringUtils.split(name, ".");
		final String[] nome = new String[partesNome.length - 1];

		for(int i = 0; i < nome.length; i++)
			nome[i] = partesNome[i];

		final String nomeArquivo = StringUtils.join(nome, ".");
		final String extensao = ".".concat(partesNome[partesNome.length - 1]);

		final File file = File.createTempFile(nomeArquivo, extensao);
		FileUtils.writeByteArrayToFile(file, content);

		return file;
	}

	public static File retrieveFile(final String fileName) {
		return new File(fileName);
	}

	public static String getExtension(final String fileName) {
		final String[] partesNome = StringUtils.split(fileName, ".");
		return partesNome[partesNome.length - 1];
	}

	@Autowired
	private SistemaService sistemaService;

	public File save(final FileResource fileResource, final MultipartFile file, final String fileName) throws IOException {
		final File saveDirectory = getSaveDirectory(fileResource);
		final File destFile = new File(saveDirectory, fileName);
		try(InputStream is = file.getInputStream(); OutputStream os = new FileOutputStream(destFile);) {
			int read = 0;
			final byte[] bytes = new byte[1024];
			while((read = is.read(bytes)) != -1) {
				os.write(bytes, 0, read);
			}
		}
		return destFile;
	}

	private File getSaveDirectory(final FileResource fileResource) {
		final String userDirectory = String.format(FILE_RESOURCE_PATH_PATTERN, sistemaService.getCaminhoArquivos(), fileResource.getClass().getSimpleName());
		final File userDirectoryFile = new File(userDirectory);
		if(!userDirectoryFile.exists())
			userDirectoryFile.mkdirs();
		return userDirectoryFile;
	}

}
