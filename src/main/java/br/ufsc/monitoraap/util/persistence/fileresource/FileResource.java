package br.ufsc.monitoraap.util.persistence.fileresource;

import java.io.File;
import java.io.IOException;

public interface FileResource {

	File toFile() throws IOException;

}
