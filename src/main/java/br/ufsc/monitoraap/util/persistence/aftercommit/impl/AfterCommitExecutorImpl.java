package br.ufsc.monitoraap.util.persistence.aftercommit.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import br.ufsc.monitoraap.util.persistence.aftercommit.AfterCommitExecutor;

@Component
public class AfterCommitExecutorImpl extends TransactionSynchronizationAdapter implements AfterCommitExecutor {

	private static final Logger log = Logger.getLogger(AfterCommitExecutorImpl.class);
	private static final ThreadLocal<List<Runnable>> queue = new ThreadLocal<List<Runnable>>();

	@Override
	public void execute(final Runnable runnable) {
		log.debug(String.format("Submitting new runnable %s to run after commit", runnable));
		if(!TransactionSynchronizationManager.isSynchronizationActive()) {
			log.debug("Transaction synchronization is NOT ACTIVE. Executing right now runnable " + runnable);
			runnable.run();
			return;
		}
		List<Runnable> threadRunnables = queue.get();
		if(threadRunnables == null) {
			threadRunnables = new ArrayList<Runnable>();
			queue.set(threadRunnables);
			TransactionSynchronizationManager.registerSynchronization(this);
		}
		threadRunnables.add(runnable);
	}

	@Override
	public void afterCommit() {
		final List<Runnable> threadRunnables = queue.get();
		log.debug(String.format("Transaction successfully committed, executing %d runnables", threadRunnables.size()));
		for(int i = 0; i < threadRunnables.size(); i++) {
			final Runnable runnable = threadRunnables.get(i);
			log.debug("Executing runnable " + runnable);
			try {
				runnable.run();
			} catch(final RuntimeException e) {
				log.error("Failed to execute runnable " + runnable, e);
			}
		}
	}

	@Override
	public void afterCompletion(final int status) {
		log.debug("Transaction completed with status " + (status == STATUS_COMMITTED ? "COMMITTED" : "ROLLED_BACK"));
		queue.remove();
	}

}
