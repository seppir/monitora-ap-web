package br.ufsc.monitoraap.util.json;

import java.io.IOException;

import br.ufsc.monitoraap.enumeration.Status;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class StatusSerializer extends JsonSerializer<Status> {

	  @Override
	  public void serialize(Status value, JsonGenerator generator,
	            SerializerProvider provider) throws IOException,
	            JsonProcessingException {

	    generator.writeStartObject();
	    generator.writeFieldName("id");
	    generator.writeNumber(value.getId());
	    generator.writeFieldName("nome");
	    generator.writeString(value.getNome());
	    generator.writeEndObject();
	  }
}
