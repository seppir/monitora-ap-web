package br.ufsc.monitoraap.util.xls;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import br.ufsc.monitoraap.model.Arquivo;
import br.ufsc.monitoraap.model.Documento;

public class XLSUtils {

	public static Workbook getWorkbook(final Documento documento) throws Exception {
		return WorkbookFactory.create(documento.toFile());
	}

	public static Arquivo createDocument(final String name, final Map<String, List<String>> data, final boolean insertKeyOnTop) throws Exception {
		try(final ByteArrayOutputStream bos = new ByteArrayOutputStream(); final Workbook workbook = new XSSFWorkbook();) {
			final Sheet sheet = workbook.createSheet(name);
			final Set<String> keyset = data.keySet();

			int cellnum = 0;
			for(final String key : keyset) {
				int rownum = 0;
				final List<String> objArr = data.get(key);

				if(insertKeyOnTop)
					objArr.add(0, key);

				for(final Object obj : objArr) {
					Row row = sheet.getRow(rownum);
					if(row == null)
						row = sheet.createRow(rownum);

					final Cell cell = row.createCell(cellnum);
					if(obj instanceof Date)
						cell.setCellValue((Date) obj);
					else if(obj instanceof Boolean)
						cell.setCellValue((Boolean) obj);
					else if(obj instanceof String)
						cell.setCellValue((String) obj);
					else if(obj instanceof Double)
						cell.setCellValue((Double) obj);

					rownum++;
				}
				cellnum++;
			}

			workbook.write(bos);

			final Arquivo arquivo = new Arquivo();
			arquivo.setNome(name.concat(".xls"));
			arquivo.setConteudo(bos.toByteArray());
			return arquivo;
		}
	}

}
