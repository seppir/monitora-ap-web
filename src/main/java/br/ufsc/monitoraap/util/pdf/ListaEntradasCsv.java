package br.ufsc.monitoraap.util.pdf;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPTable;

import br.ufsc.monitoraap.model.EntradaCsv;

public class ListaEntradasCsv extends PdfPTable {

	private final Iterable<EntradaCsv> entradas;

	public ListaEntradasCsv(final Iterable<EntradaCsv> entradas) {
		super(new float[] { 6 });
		this.entradas = entradas;

		setWidthPercentage(100);
		getDefaultCell().setPadding(5);
		getDefaultCell().setBorder(Rectangle.BOX);
		getDefaultCell().setBorderWidth(1);
		getDefaultCell().setBorderColor(BaseColor.BLACK);
		getDefaultCell().setHorizontalAlignment(ALIGN_JUSTIFIED);
		getDefaultCell().setVerticalAlignment(ALIGN_TOP);
		getDefaultCell().setExtraParagraphSpace(5);

		addLista();
	}

	private void addLista() {
		for(final EntradaCsv entrada : entradas) {
			final Paragraph nome = new Paragraph();
			final Paragraph bairro = new Paragraph();
			final Paragraph faixaEtaria = new Paragraph();
			final Paragraph email = new Paragraph();
			final Paragraph whatsApp = new Paragraph();
			final Paragraph resposta1 = new Paragraph();
			final Paragraph resposta2 = new Paragraph();

			nome.add(PDFUtils.write("Nome: ", PDFUtils.FONTE_NEGRITO));
			nome.add(PDFUtils.write(entrada.getNome()));
			bairro.add(PDFUtils.write("Bairro: ", PDFUtils.FONTE_NEGRITO));
			bairro.add(PDFUtils.write(entrada.getBairro().getNomeApresentacao()));
			faixaEtaria.add(PDFUtils.write("Faixa Etária: ", PDFUtils.FONTE_NEGRITO));
			faixaEtaria.add(PDFUtils.write(entrada.getIdade().getDescricao()));
			email.add(PDFUtils.write("Email: ", PDFUtils.FONTE_NEGRITO));
			email.add(PDFUtils.write(entrada.getEmail()));
			whatsApp.add(PDFUtils.write("WhatsApp: ", PDFUtils.FONTE_NEGRITO));
			whatsApp.add(PDFUtils.write(entrada.getWhatsapp()));
			resposta1.add(PDFUtils.write("Resposta 1: ", PDFUtils.FONTE_NEGRITO));
			resposta1.add(PDFUtils.write(entrada.getResposta1()));
			resposta2.add(PDFUtils.write("Resposta 2: ", PDFUtils.FONTE_NEGRITO));
			resposta2.add(PDFUtils.write(entrada.getResposta2()));

			final Paragraph paragraph = new Paragraph();
			paragraph.add(nome);
			paragraph.add(bairro);
			paragraph.add(faixaEtaria);
			paragraph.add(email);
			paragraph.add(whatsApp);
			paragraph.add(resposta1);
			paragraph.add(resposta2);

			addCell(paragraph);
		}
	}

}
