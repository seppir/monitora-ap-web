package br.ufsc.monitoraap.util.pdf;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

import br.ufsc.monitoraap.config.WebAppInitializer;

public class PaginaPDF extends PdfPageEventHelper {

	private Image imagem;

	private final Date dataGeracao;

	public PaginaPDF(final Date dataGeracao) {
		super();
		this.dataGeracao = dataGeracao;
	}

	private Image carregaImagemCabecalho() throws BadElementException, MalformedURLException, IOException {
		if(imagem == null) {
			final String url = WebAppInitializer.getUrlBase().concat("img/banner_pdf.png");
			final Image i = Image.getInstance(new URL(url));
			i.scalePercent(40);
			imagem = i;
		}
		return imagem;
	}

	@Override
	public void onStartPage(final PdfWriter writer, final Document document) {
		try {
			document.add(carregaImagemCabecalho());
		} catch(DocumentException | IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onEndPage(final PdfWriter writer, final Document document) {
		final PdfPTable t = new RodapePaginaPDF(dataGeracao, writer.getCurrentPageNumber());
		t.writeSelectedRows(0, -1, 18, 24, writer.getDirectContent());
	}

}
