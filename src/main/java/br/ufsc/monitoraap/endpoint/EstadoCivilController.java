package br.ufsc.monitoraap.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraap.model.EstadoCivil;
import br.ufsc.monitoraap.service.EstadoCivilService;

@RestController
@RequestMapping("/estadocivil")
public class EstadoCivilController extends AbstractRestController<EstadoCivil, Integer> {

	@Autowired
	protected EstadoCivilController(final EstadoCivilService service) {
		super(service);
	}

}
