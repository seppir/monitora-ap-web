package br.ufsc.monitoraap.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraap.model.Idade;
import br.ufsc.monitoraap.service.IdadeService;

@RestController
@RequestMapping("/idade")
public class IdadeController extends AbstractRestController<Idade, Integer> {

	@Autowired
	protected IdadeController(final IdadeService service) {
		super(service);
	}

}
