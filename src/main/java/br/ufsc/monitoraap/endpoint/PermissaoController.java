package br.ufsc.monitoraap.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraap.model.Permissao;
import br.ufsc.monitoraap.service.PermissaoService;

@RestController
@RequestMapping("/permissoes")
public class PermissaoController extends AbstractRestController<Permissao, Integer> {

	@Autowired
	protected PermissaoController(final PermissaoService service) {
		super(service);
	}

}
