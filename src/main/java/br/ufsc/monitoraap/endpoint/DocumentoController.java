package br.ufsc.monitoraap.endpoint;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraap.service.DocumentoService;

@RestController
@RequestMapping("/documento")
public class DocumentoController {

	protected final Logger log = Logger.getLogger(getClass());

	@Autowired
	private DocumentoService service;

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public HttpEntity<?> remove(@PathVariable final Integer id) {
		log.debug("/delete?" + id);
		try {
			service.delete(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch(final Exception e) {
			log.error(e);
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
