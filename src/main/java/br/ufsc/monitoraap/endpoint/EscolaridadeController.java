package br.ufsc.monitoraap.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraap.model.Escolaridade;
import br.ufsc.monitoraap.service.EscolaridadeSerivce;

@RestController
@RequestMapping("/escolaridade")
public class EscolaridadeController extends AbstractRestController<Escolaridade, Integer> {

	@Autowired
	protected EscolaridadeController(final EscolaridadeSerivce service) {
		super(service);
	}

}
