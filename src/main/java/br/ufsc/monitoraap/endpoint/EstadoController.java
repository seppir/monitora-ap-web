package br.ufsc.monitoraap.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraap.model.Estado;
import br.ufsc.monitoraap.service.EstadoService;

@RestController
@RequestMapping("/estado")
public class EstadoController extends AbstractRestController<Estado, Integer> {

	@Autowired
	protected EstadoController(final EstadoService service) {
		super(service);
	}

	@RequestMapping("/porcidade/{idCidade}")
	public HttpEntity<?> getByCity(@PathVariable final Integer idCidade) {
		log.debug("/porcidade?" + idCidade);
		final Estado e = ((EstadoService) service).findByCity(idCidade);
		return new ResponseEntity<>(e, HttpStatus.OK);
	}

}
