package br.ufsc.monitoraap.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraap.model.Renda;
import br.ufsc.monitoraap.service.RendaService;

@RestController
@RequestMapping("/renda")
public class RendaController extends AbstractRestController<Renda, Integer> {

	@Autowired
	protected RendaController(final RendaService service) {
		super(service);
	}

}
