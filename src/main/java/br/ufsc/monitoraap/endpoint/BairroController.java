package br.ufsc.monitoraap.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraap.model.Bairro;
import br.ufsc.monitoraap.service.BairroService;

@RestController
@RequestMapping("/bairro")
public class BairroController extends AbstractRestController<Bairro, Integer> {

	@Autowired
	protected BairroController(final BairroService service) {
		super(service);
	}

	@RequestMapping("/porcidade/{idCidade}")
	public HttpEntity<?> getAllFromState(@PathVariable final Integer idCidade) {
		log.debug("/porcidade?" + idCidade);
		final Iterable<Bairro> list = ((BairroService) service).findByCity(idCidade);
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

	@RequestMapping("/withalldata/{id}")
	public HttpEntity<?> getWithAllData(@PathVariable final Integer id) {
		log.debug("/withalldata?" + id);
		final Bairro b = ((BairroService) service).findOneWithAllData(id);
		return new ResponseEntity<>(b, HttpStatus.OK);
	}

	@RequestMapping("/nomecidade/{cityName}")
	public HttpEntity<?> getAllByCityName(@PathVariable final String cityName) {
		log.debug("/nomecidade?" + cityName);
		final Iterable<Bairro> b = ((BairroService) service).findByCityName(cityName);
		return new ResponseEntity<>(b, HttpStatus.OK);
	}

}
