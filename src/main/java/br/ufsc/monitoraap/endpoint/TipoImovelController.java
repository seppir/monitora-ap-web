package br.ufsc.monitoraap.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraap.model.TipoImovel;
import br.ufsc.monitoraap.service.TipoImovelService;

@RestController
@RequestMapping("/tipoimovel")
public class TipoImovelController extends AbstractRestController<TipoImovel, Integer> {

	@Autowired
	protected TipoImovelController(final TipoImovelService service) {
		super(service);
	}

}
