package br.ufsc.monitoraap.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraap.model.Tag;
import br.ufsc.monitoraap.service.TagService;

@RestController
@RequestMapping("/tag")
public class TagController extends AbstractRestController<Tag, Integer> {

	@Autowired
	protected TagController(final TagService service) {
		super(service);
	}

}
