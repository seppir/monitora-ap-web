package br.ufsc.monitoraap.endpoint;

import java.security.Principal;
import java.util.Date;
import java.util.List;

import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraap.dto.ExportacaoAudienciaPublicaDTO;
import br.ufsc.monitoraap.model.Documento;
import br.ufsc.monitoraap.model.DocumentoPendente;
import br.ufsc.monitoraap.model.Proponente;
import br.ufsc.monitoraap.model.Proposicao;
import br.ufsc.monitoraap.repository.UsuarioRepository;
import br.ufsc.monitoraap.service.ProposicaoService;

@RestController
@RequestMapping("/proposicao")
public class ProposicaoController extends AbstractRestController<Proposicao, Integer> {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	protected ProposicaoController(final ProposicaoService service) {
		super(service);
	}

	@RequestMapping(value = "/ap/{idAp}", method = RequestMethod.GET)
	public HttpEntity<?> findByAp(@PathVariable final Integer idAp) {
		log.debug("/ap/" + idAp);
		final Iterable<Proposicao> proposicoes = ((ProposicaoService) service).findByAp(idAp);
		return new ResponseEntity<>(proposicoes, HttpStatus.OK);
	}

	@RequestMapping(value = "/{idProponente}/{idAp}", method = RequestMethod.GET)
	public HttpEntity<?> findByUserAndAp(@PathVariable final Integer idProponente, @PathVariable final Integer idAp) {
		log.debug("/idProponente/idAp - /" + idProponente + "/" + idAp);
		final Iterable<Proposicao> proposicoes = ((ProposicaoService) service).findByUserAndAp(idProponente, idAp);
		return new ResponseEntity<>(proposicoes, HttpStatus.OK);
	}

	@Override
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public HttpEntity<?> add(final Proposicao entity) {
		log.debug("/add");
		return new ResponseEntity<>(HttpStatus.MOVED_PERMANENTLY);
	}

	@RequestMapping(value = "/addproposicao", method = RequestMethod.POST)
	public HttpEntity<?> add(@RequestBody final Proposicao entity, final Principal principal) {
		log.debug("/addproposicao");
		try {
			((ProposicaoService) service).save((Proponente) usuarioRepository.findByEmail(principal.getName()), entity);
			return new ResponseEntity<>(HttpStatus.CREATED);
		} catch(final Exception e) {
			log.error(e);
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/savefromexcel", method = RequestMethod.POST)
	public HttpEntity<?> saveFromExcel(@RequestBody final Documento documento, @QueryParam("idProponente") final Integer idProponente,
		@QueryParam("dataCadastro") final long dataCadastro) {
		log.debug("/savefromexcel");
		try {
			((ProposicaoService) service).importaProposicoesExcel(idProponente, new Date(dataCadastro), documento);
			return new ResponseEntity<>(HttpStatus.CREATED);
		} catch(final Exception e) {
			log.error(e);
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/exporttoexcel", method = RequestMethod.POST)
	public HttpEntity<?> exportExcelForImport(@RequestBody final List<ExportacaoAudienciaPublicaDTO> proposicoes) {
		log.debug("/exporttoexcel");
		try {
			final DocumentoPendente doc = ((ProposicaoService) service).exportarExcelParaImportacao(proposicoes);
			return new ResponseEntity<>(doc.getId(), HttpStatus.OK);
		} catch(final Exception e) {
			log.error(e);
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
