package br.ufsc.monitoraap.endpoint;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.ufsc.monitoraap.model.Video;
import br.ufsc.monitoraap.service.VideoService;

@RestController
@RequestMapping("/video")
public class VideoController {

	protected final Logger log = Logger.getLogger(getClass());

	@Autowired
	private VideoService service;

	@RequestMapping(value = "/{id}", produces = MediaType.APPLICATION_OCTET_STREAM)
	public Resource load(@PathVariable final Integer id, final HttpServletResponse response) throws IOException {
		final Video video = service.findOne(id);
		response.setHeader("Content-Disposition", "attachment;filename=".concat(video.getNome().replaceAll(" ", "_")));
		return new FileSystemResource(video.toFile());
	}

	@RequestMapping(value = "/upload/{idAp}", method = RequestMethod.POST)
	public HttpEntity<?> uploadVideo(@PathVariable final Integer idAp, @RequestParam final MultipartFile file) throws IOException {
		log.debug("/add");
		try {
			final Video video = new Video();
			video.setIdAp(idAp);
			service.save(video, file);
			return new ResponseEntity<>(HttpStatus.CREATED);
		} catch(final Exception e) {
			log.error(e);
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public HttpEntity<?> remove(@PathVariable final Integer id) {
		log.debug("/delete?" + id);
		try {
			service.delete(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch(final Exception e) {
			log.error(e);
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
