package br.ufsc.monitoraap.endpoint;

import java.io.IOException;
import java.io.Serializable;

import org.apache.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.ufsc.monitoraap.model.Entidade;
import br.ufsc.monitoraap.service.AbstractService;

public abstract class AbstractRestController<T extends Entidade, ID extends Serializable> {

	protected final Logger log = Logger.getLogger(getClass());

	protected final AbstractService<T, ID> service;

	protected AbstractRestController(final AbstractService<T, ID> service) {
		this.service = service;
	}

	@RequestMapping(method = RequestMethod.GET)
	public HttpEntity<?> getAllPaged(@PageableDefault(size = 20, page = 0) final Pageable pageable) {
		log.debug("/getAllPaged");
		final Page<T> pages = service.getAllPaged(pageable);
		final Iterable<T> list = pages.getContent();
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public HttpEntity<?> getAll() {
		log.debug("/all");
		final Iterable<T> lista = service.getAll();
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public HttpEntity<?> get(@PathVariable final ID id) {
		log.debug("/get?" + id);
		final T entity = service.findOne(id);
		return new ResponseEntity<>(entity, HttpStatus.OK);
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public HttpEntity<?> add(@RequestBody final T entity) {
		try {
			log.debug("/add");
			service.save(entity);
			return new ResponseEntity<>(HttpStatus.CREATED);
		} catch(final Exception e) {
			log.error(e);
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public HttpEntity<?> update(@PathVariable final ID id, @RequestBody final String json) throws JsonProcessingException, IOException {
		log.debug("/update?" + id);
		try {
			final ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			final T entity = objectMapper.readerForUpdating(service.findOne(id)).readValue(json);
			service.save(entity);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch(final Exception e) {
			log.error(e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public HttpEntity<?> remove(@PathVariable final ID id) {
		log.debug("/delete?" + id);
		service.delete(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
