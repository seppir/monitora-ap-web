package br.ufsc.monitoraap.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraap.model.Documento;
import br.ufsc.monitoraap.model.Proponente;
import br.ufsc.monitoraap.service.ProponenteService;

@RestController
@RequestMapping("/proponentes")
public class ProponenteController extends AbstractRestController<Proponente, Integer> {

	@Autowired
	protected ProponenteController(final ProponenteService service) {
		super(service);
	}

	@RequestMapping(value = "/savefromexcel", method = RequestMethod.POST)
	public HttpEntity<?> saveFromExcel(@RequestBody final Documento documento) {
		log.debug("/savefromexcel");
		try {
			((ProponenteService) service).importaProponentesExcel(documento);
			return new ResponseEntity<>(HttpStatus.CREATED);
		} catch(final Exception e) {
			log.error(e);
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
