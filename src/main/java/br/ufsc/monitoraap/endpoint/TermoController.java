package br.ufsc.monitoraap.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraap.model.Termo;
import br.ufsc.monitoraap.model.TermoAudienciaPublica;
import br.ufsc.monitoraap.service.TermoService;

@RestController
@RequestMapping("/termo")
public class TermoController extends AbstractRestController<Termo, Integer> {

	@Autowired
	protected TermoController(final TermoService service) {
		super(service);
	}

	@RequestMapping(value = "/{idTermo}/valido", method = RequestMethod.PUT)
	public HttpEntity<?> setValidade(@PathVariable final Integer idTermo, @RequestBody final String valido) {
		log.debug("/valido?" + idTermo);
		try {
			((TermoService) service).setValidade(idTermo, Boolean.valueOf(valido));
			return new ResponseEntity<>(HttpStatus.OK);
		} catch(final Exception e) {
			log.error(e);
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{idTermo}/atualizar", method = RequestMethod.PUT)
	public HttpEntity<?> setDescricao(@PathVariable final Integer idTermo, @RequestBody final String descricao) {
		log.debug("/atualizar?" + idTermo);
		try {
			((TermoService) service).setDescricao(idTermo, descricao);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch(final Exception e) {
			log.error(e);
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/ap/{idAp}/{idTermo}/valido", method = RequestMethod.PUT)
	public HttpEntity<?> setValidadeAudienciaPublica(@PathVariable final Integer idTermo, @PathVariable final Integer idAp, @RequestBody final String valido) {
		log.debug("/ap/valido?" + idTermo + "&" + idAp);
		try {
			((TermoService) service).setValidadeAudienciaPublica(idAp, idTermo, Boolean.valueOf(valido));
			return new ResponseEntity<>(HttpStatus.OK);
		} catch(final Exception e) {
			log.error(e);
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/ap/{idAp}/{idTermo}/atualizar", method = RequestMethod.PUT)
	public HttpEntity<?> setDescricaoAudienciaPublica(@PathVariable final Integer idTermo, @PathVariable final Integer idAp,
		@RequestBody final String descricao) {
		log.debug("/ap/atualizar?" + idTermo + "&" + idAp);
		try {
			((TermoService) service).setDescricaoAudienciaPublica(idAp, idTermo, descricao);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch(final Exception e) {
			log.error(e);
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping("/ap/{idAp}")
	public HttpEntity<?> getFromAudienciaPublica(@PathVariable final Integer idAp) {
		final Iterable<TermoAudienciaPublica> termos = ((TermoService) service).findByAudienciaPublica(idAp);
		return new ResponseEntity<>(termos, HttpStatus.OK);
	}

}
