package br.ufsc.monitoraap.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraap.model.Cidade;
import br.ufsc.monitoraap.service.CidadeService;

@RestController
@RequestMapping("/cidade")
public class CidadeController extends AbstractRestController<Cidade, Integer> {

	@Autowired
	protected CidadeController(final CidadeService service) {
		super(service);
	}

	@RequestMapping("/porestado/{idEstado}")
	public HttpEntity<?> getAllFromState(@PathVariable final Integer idEstado) {
		log.debug("/porestado?" + idEstado);
		final Iterable<Cidade> list = ((CidadeService) service).findByState(idEstado);
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

	@RequestMapping("/pornome/{nome}")
	public HttpEntity<?> getByNameAndUf(@PathVariable final String nome) {
		log.debug("/pornome?" + nome);
		final Cidade cidade = ((CidadeService) service).findByNome(nome);
		return new ResponseEntity<>(cidade, HttpStatus.OK);
	}

}
