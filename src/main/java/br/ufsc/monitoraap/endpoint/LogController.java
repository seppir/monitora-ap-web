package br.ufsc.monitoraap.endpoint;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/log")
public class LogController {

	protected final Logger log = Logger.getLogger(getClass());

	@RequestMapping(value = "/reloadconfig", method = RequestMethod.PATCH)
	public HttpEntity<?> reloadConfig() throws IOException {
		final Resource propertiesFile = new ClassPathResource("log4j.properties");
		PropertyConfigurator.configure(propertiesFile.getInputStream());
		log.debug("Reloaded log config");
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
