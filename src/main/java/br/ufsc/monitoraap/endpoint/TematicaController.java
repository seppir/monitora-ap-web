package br.ufsc.monitoraap.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraap.model.Tematica;
import br.ufsc.monitoraap.service.TematicaService;

@RestController
@RequestMapping("/tematica")
public class TematicaController extends AbstractRestController<Tematica, Integer> {

	@Autowired
	protected TematicaController(final TematicaService service) {
		super(service);
	}

}
