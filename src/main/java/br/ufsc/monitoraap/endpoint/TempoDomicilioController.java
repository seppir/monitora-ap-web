package br.ufsc.monitoraap.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraap.model.TempoDomicilio;
import br.ufsc.monitoraap.service.TempoDomicilioService;

@RestController
@RequestMapping("/tempodomicilio")
public class TempoDomicilioController extends AbstractRestController<TempoDomicilio, Integer> {

	@Autowired
	protected TempoDomicilioController(final TempoDomicilioService service) {
		super(service);
	}

}
