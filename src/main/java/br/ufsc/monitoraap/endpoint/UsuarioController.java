package br.ufsc.monitoraap.endpoint;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraap.exception.ValidationException;
import br.ufsc.monitoraap.model.Usuario;
import br.ufsc.monitoraap.service.UsuarioService;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController extends AbstractRestController<Usuario, Integer> {

	@Autowired
	protected UsuarioController(final UsuarioService service) {
		super(service);
	}

	@RequestMapping(value = "/authenticate", method = RequestMethod.GET)
	public HttpEntity<Usuario> authenticate(final Principal principal) {
		log.debug("/authenticate");
		final Usuario u = ((UsuarioService) service).authenticate(principal.getName());
		if(u != null)
			return new ResponseEntity<>(u, HttpStatus.OK);
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
	}

	@RequestMapping(value = "/facebook/{facebookId}", method = RequestMethod.GET)
	public HttpEntity<Usuario> getByFacebookId(@PathVariable final String facebookId) {
		log.debug("/facebook");
		final Usuario u = ((UsuarioService) service).findByFacebookId(facebookId);
		if(u != null)
			return new ResponseEntity<>(u, HttpStatus.OK);
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
	}

	@RequestMapping(value = "/facebook", method = RequestMethod.PUT)
	public HttpEntity<?> vincularFacebook(@RequestBody final String facebookId, final Principal principal) {
		log.debug("/vincularFacebook");
		final Usuario u = ((UsuarioService) service).findByEmail(principal.getName());
		if(u != null) {
			try {
				((UsuarioService) service).bindFacebookId(u.getId(), facebookId);
				return new ResponseEntity<>(HttpStatus.OK);
			} catch(final ValidationException e) {
				log.error(e);
				return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
	}

}
