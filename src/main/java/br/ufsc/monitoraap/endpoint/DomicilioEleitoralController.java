package br.ufsc.monitoraap.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraap.model.DomicilioEleitoral;
import br.ufsc.monitoraap.service.DomicilioEleitoralService;

@RestController
@RequestMapping("/domicilioeleitoral")
public class DomicilioEleitoralController extends AbstractRestController<DomicilioEleitoral, Integer> {

	@Autowired
	protected DomicilioEleitoralController(final DomicilioEleitoralService service) {
		super(service);
	}

	@RequestMapping("/porcidade/{idCidade}")
	public HttpEntity<?> getAllFromState(@PathVariable final Integer idCidade) {
		log.debug("/porcidade?" + idCidade);
		final Iterable<DomicilioEleitoral> list = ((DomicilioEleitoralService) service).findByCity(idCidade);
		return new ResponseEntity<>(list, HttpStatus.OK);
	}

}
