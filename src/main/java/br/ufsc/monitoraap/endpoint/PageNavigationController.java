package br.ufsc.monitoraap.endpoint;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import br.ufsc.monitoraap.config.WebAppInitializer;

@Controller
public class PageNavigationController {

	@RequestMapping("/")
	public String index(final HttpServletRequest request) {
		WebAppInitializer.setUrlBase(request.getRequestURL().toString());
		return "index";
	}

	@RequestMapping("/florianopolismelhor")
	public String gerarCsv(final HttpServletRequest request) {
		final StringBuffer baseUrl = request.getRequestURL();
		// Remove a parte após "portalcidadao/"
		baseUrl.delete(baseUrl.indexOf("portalcidadao/") + 14, baseUrl.length());
		WebAppInitializer.setUrlBase(baseUrl.toString());
		return "geracao_csv/participacao";
	}

}
