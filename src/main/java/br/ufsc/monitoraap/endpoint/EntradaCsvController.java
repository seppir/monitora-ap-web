package br.ufsc.monitoraap.endpoint;

import java.io.File;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraap.model.Arquivo;
import br.ufsc.monitoraap.model.EntradaCsv;
import br.ufsc.monitoraap.service.EntradaCsvService;

@RestController
@RequestMapping("/entradacsv")
public class EntradaCsvController extends AbstractRestController<EntradaCsv, Integer> {

	@Autowired
	protected EntradaCsvController(final EntradaCsvService service) {
		super(service);
	}

	@RequestMapping(value = "/xls", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM)
	public Resource exportarXls(final HttpServletResponse response) throws Exception {
		final Arquivo arquivo = ((EntradaCsvService) service).gerarExcel();
		response.setHeader("Content-Disposition", "attachment;filename=".concat(arquivo.getNome()));
		return new FileSystemResource(arquivo.toFile());
	}

	@RequestMapping(value = "/pdf", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM)
	public Resource exportarPdf(final HttpServletResponse response) throws Exception {
		final File arquivo = ((EntradaCsvService) service).gerarPdf();
		response.setHeader("Content-Disposition", "attachment;filename=".concat(arquivo.getName()));
		return new FileSystemResource(arquivo);
	}

}
