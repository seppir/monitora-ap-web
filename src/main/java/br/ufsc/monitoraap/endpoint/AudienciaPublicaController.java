package br.ufsc.monitoraap.endpoint;

import java.io.IOException;
import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.ufsc.monitoraap.model.AudienciaPublica;
import br.ufsc.monitoraap.model.Usuario;
import br.ufsc.monitoraap.repository.UsuarioRepository;
import br.ufsc.monitoraap.service.AudienciaPublicaService;

@RestController
@RequestMapping("/ap")
public class AudienciaPublicaController extends AbstractRestController<AudienciaPublica, Integer> {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	protected AudienciaPublicaController(final AudienciaPublicaService service) {
		super(service);
	}

	@Override
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public HttpEntity<?> add(final AudienciaPublica entity) {
		log.debug("/add");
		return new ResponseEntity<>(HttpStatus.MOVED_PERMANENTLY);
	}

	@RequestMapping(value = "/addap", method = RequestMethod.POST)
	public HttpEntity<?> add(@RequestBody final AudienciaPublica entity, final Principal principal) {
		log.debug("/addap");
		try {
			((AudienciaPublicaService) service).save(usuarioRepository.findByEmail(principal.getName()), entity);
			return new ResponseEntity<>(entity.getId(), HttpStatus.CREATED);
		} catch(final Exception e) {
			log.error(e);
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public HttpEntity<?> update(final Integer id, final String json) throws JsonProcessingException, IOException {
		log.debug("/update?" + id);
		return new ResponseEntity<>(HttpStatus.MOVED_PERMANENTLY);
	}

	@RequestMapping(value = "/updateap/{id}", method = RequestMethod.PUT)
	public HttpEntity<?> update(@PathVariable final Integer id, @RequestBody final String json, final Principal principal)
		throws JsonProcessingException, IOException {
		log.debug("/update?" + id);
		try {
			final ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			objectMapper.configure(MapperFeature.USE_GETTERS_AS_SETTERS, false);
			final AudienciaPublica entity = objectMapper.readerForUpdating(service.findOne(id)).readValue(json);
			((AudienciaPublicaService) service).save(usuarioRepository.findByEmail(principal.getName()), entity);
			return new ResponseEntity<>(id, HttpStatus.OK);
		} catch(final Exception e) {
			log.error(e);
			return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/allwithcreator", method = RequestMethod.GET)
	public HttpEntity<?> getAllWithCreator() {
		log.debug("/allwithcreator");
		return new ResponseEntity<>(((AudienciaPublicaService) service).findAllWithCreator(), HttpStatus.OK);
	}

	@RequestMapping(value = "/creator/{id}", method = RequestMethod.GET)
	public HttpEntity<?> getWithCreator(@PathVariable final Integer id) {
		log.debug("/creator/" + id);
		return new ResponseEntity<>(((AudienciaPublicaService) service).findOneWithCreator(id), HttpStatus.OK);
	}

	@RequestMapping("/mine")
	public HttpEntity<?> getMine(final Principal principal) {
		log.debug("/mine");
		final Usuario criador = usuarioRepository.findByEmail(principal.getName());
		return new ResponseEntity<>(((AudienciaPublicaService) service).findFromCreator(criador.getId()), HttpStatus.OK);
	}

}
