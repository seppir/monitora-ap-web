package br.ufsc.monitoraap.endpoint;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraap.model.DocumentoPendente;
import br.ufsc.monitoraap.repository.DocumentoPendenteRepository;
import br.ufsc.monitoraap.service.DocumentoPendenteService;

@RestController
@RequestMapping("/documento-pendente")
public class DocumentoPendenteController extends AbstractRestController<DocumentoPendente, Integer> {

	@Autowired
	protected DocumentoPendenteController(final DocumentoPendenteService service) {
		super(service);
	}

	@Autowired
	private DocumentoPendenteRepository repository;

	@RequestMapping(value = "/download/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_OCTET_STREAM)
	public Resource downloadDocumentoPendente(@PathVariable final Integer id, final HttpServletResponse response) throws IOException {
		final DocumentoPendente documento = repository.findOne(id);

		response.setHeader("Content-Disposition", "attachment;filename=".concat(documento.getNome().replaceAll(" ", "_")));

		final File file = documento.toFile();
		return new FileSystemResource(file);
	}

}
