package br.ufsc.monitoraap.endpoint;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraap.service.SistemaService;

@RestController
@RequestMapping("/sistema")
public class SistemaController {

	@Autowired
	private SistemaService sistemaService;

	@RequestMapping("/dados")
	public String getVersao() throws IOException {
		return sistemaService.getVersao();
	}

}
