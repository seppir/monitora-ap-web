package br.ufsc.monitoraap.endpoint;

import java.util.Arrays;

import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.ufsc.monitoraap.enumeration.Status;

@RestController
@RequestMapping("/status")
public class StatusController {

	protected final Logger log = Logger.getLogger(getClass());

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public HttpEntity<?> getAll() {
		log.debug("/all");
		final Iterable<Status> lista = Arrays.asList(Status.values());
		return new ResponseEntity<>(lista, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public HttpEntity<?> get(@PathVariable final Long id) {
		log.debug("/get?" + id);
		final Status entity = Status.getStatus(id);
		return new ResponseEntity<>(entity, HttpStatus.OK);
	}

}
