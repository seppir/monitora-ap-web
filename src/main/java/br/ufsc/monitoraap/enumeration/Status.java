package br.ufsc.monitoraap.enumeration;

import br.ufsc.monitoraap.util.json.StatusSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(using = StatusSerializer.class)
public enum Status {

	AGENDADA(1l, "Agendada"),
	EM_ANDAMENTO(2l, "Em andamento"),
	ENCERRADA(3l, "Encerrada"),
	CANCELADA(4l, "Cancelada");

	private Long id;
	private String nome;

	private Status(final Long idStatus, final String nome) {
		id = idStatus;
		this.nome = nome;
	}

	public static Status getStatus(final Long id) {
		for(final Status e : values())
			if(e.id.equals(id))
				return e;
		return null;
	}

	public Long getId() {
		return id;
	}

	public void setId(final Long idStatus) {
		id = idStatus;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(final String nome) {
		this.nome = nome;
	}

}
