package br.ufsc.monitoraap.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ufsc.monitoraap.model.Permissao;

@Repository
public interface PermissaoRepository extends JpaRepository<Permissao, Integer> {}
