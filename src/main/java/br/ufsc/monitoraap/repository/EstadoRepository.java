package br.ufsc.monitoraap.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.ufsc.monitoraap.model.Estado;

@Repository
public interface EstadoRepository extends JpaRepository<Estado, Integer> {

	@Query("select c.estado from Cidade c where c.id = ?")
	Estado findByCity(Integer idCidade);

	@Query("from Estado e where e.sigla = :sigla")
	Estado findBySigla (@Param("sigla") String siglaEstado);
}
