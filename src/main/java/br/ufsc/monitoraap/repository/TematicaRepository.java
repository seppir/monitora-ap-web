package br.ufsc.monitoraap.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.ufsc.monitoraap.model.Tematica;

@Repository
public interface TematicaRepository extends JpaRepository<Tematica, Integer> {

	@Override
	@Query("from Tematica t order by t.nome")
	List<Tematica> findAll();

	@Query("from Tematica t where lower(trim(t.nome)) like lower(trim(?))")
	Tematica findByNome(String nome);

}
