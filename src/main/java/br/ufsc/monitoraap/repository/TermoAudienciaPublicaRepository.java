package br.ufsc.monitoraap.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.ufsc.monitoraap.model.TermoAudienciaPublica;
import br.ufsc.monitoraap.model.TermoAudienciaPublica.TermoAudienciaPublicaPK;

@Repository
public interface TermoAudienciaPublicaRepository extends JpaRepository<TermoAudienciaPublica, TermoAudienciaPublicaPK> {

	@Query("from TermoAudienciaPublica t where t.termoAudienciaPublicaPk.idAudienciaPublica = ?")
	List<TermoAudienciaPublica> findByAudienciaPublica(Integer idAp);

	@Query("from TermoAudienciaPublica t where t.termoAudienciaPublicaPk.termo.id = ?")
	List<TermoAudienciaPublica> findByTermo(Integer idTermo);

}
