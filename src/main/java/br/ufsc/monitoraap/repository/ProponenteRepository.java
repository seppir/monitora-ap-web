package br.ufsc.monitoraap.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ufsc.monitoraap.model.Proponente;

@Repository
public interface ProponenteRepository extends JpaRepository<Proponente, Integer> {}
