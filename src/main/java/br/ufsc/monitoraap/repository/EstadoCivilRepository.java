package br.ufsc.monitoraap.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.ufsc.monitoraap.model.EstadoCivil;

@Repository
public interface EstadoCivilRepository extends JpaRepository<EstadoCivil, Integer> {
	
	@Query("from EstadoCivil ec where ec.descricao = ?")
	EstadoCivil findByNome (String nomeEstadoCivil);
}
