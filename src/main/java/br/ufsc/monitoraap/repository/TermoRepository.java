package br.ufsc.monitoraap.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ufsc.monitoraap.model.Termo;

@Repository
public interface TermoRepository extends JpaRepository<Termo, Integer> {}
