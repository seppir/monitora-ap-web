package br.ufsc.monitoraap.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ufsc.monitoraap.model.Politico;

@Repository
public interface PoliticoRepository extends JpaRepository<Politico, Integer> {}
