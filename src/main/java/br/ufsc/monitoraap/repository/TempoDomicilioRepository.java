package br.ufsc.monitoraap.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ufsc.monitoraap.model.TempoDomicilio;

@Repository
public interface TempoDomicilioRepository extends JpaRepository<TempoDomicilio, Integer> {}
