package br.ufsc.monitoraap.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.ufsc.monitoraap.model.Tag;

@Repository
public interface TagRepository extends JpaRepository<Tag, Integer> {

	@Query("from Tag t where lower(trim(t.valor)) like lower(trim(?))")
	Tag findByValor(String valor);

}
