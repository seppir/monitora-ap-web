package br.ufsc.monitoraap.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ufsc.monitoraap.model.Renda;

@Repository
public interface RendaRepository extends JpaRepository<Renda, Integer> {}
