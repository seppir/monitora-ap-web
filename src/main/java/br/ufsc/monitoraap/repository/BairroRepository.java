package br.ufsc.monitoraap.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.ufsc.monitoraap.model.Bairro;

@Repository
public interface BairroRepository extends JpaRepository<Bairro, Integer> {

	@Query("from Bairro b where b.cidade.id = ? "
		+ "order by b.nome")
	List<Bairro> findByCity(Integer cityId);

	@Query("from Bairro b where b.nome = :nomeBairro and b.cidade.id = :idCidade")
	Bairro findByNome(@Param("nomeBairro") String nomeBairro, @Param("idCidade") Integer idCidade);

	@Query("from Bairro b "
		+ "join fetch b.cidade.estado "
		+ "where b.id = ?")
	Bairro findOneWithAllData(Integer id);

	@Query("from Bairro b where b.cidade.nome = ? "
		+ "order by b.nome")
	List<Bairro> findByCityName(String cityName);

}
