package br.ufsc.monitoraap.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.ufsc.monitoraap.model.AudienciaPublica;

@Repository
public interface AudienciaPublicaRepository extends JpaRepository<AudienciaPublica, Integer> {

	@Query("select distinct a from AudienciaPublica a "
		+ "join fetch a.criador ")
	List<AudienciaPublica> findAllWithcreator();

	@Query("select distinct a from AudienciaPublica a "
		+ "join fetch a.criador "
		+ "where a.criador.id = ? ")
	List<AudienciaPublica> findFromCreator(Integer idCriador);

	@Query("from AudienciaPublica a "
		+ "join fetch a.criador "
		+ "where a.id = ?")
	AudienciaPublica findOneWithCreator(Integer id);

	@Query("from AudienciaPublica a "
		+ "where a.codigo = ?")
	AudienciaPublica findByCodAP(String codAp);

}
