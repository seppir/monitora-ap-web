package br.ufsc.monitoraap.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.ufsc.monitoraap.model.Cidade;

@Repository
public interface CidadeRepository extends JpaRepository<Cidade, Integer> {

	@Query("from Cidade c where c.estado.id = ?")
	List<Cidade> findByState(Integer idEstado);

	@Query("from Cidade c where c.nome = :nomeCidade and c.estado.id = :idEstado")
	Cidade findByNameAndState(@Param("nomeCidade") String nomeCidade, @Param("idEstado") Integer idEstado);

	@Query("from Cidade c "
		+ "join fetch c.estado "
		+ "where c.nome = ?")
	Cidade findByNome(String nome);

}
