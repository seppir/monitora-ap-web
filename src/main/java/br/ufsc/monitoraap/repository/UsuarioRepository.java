package br.ufsc.monitoraap.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.ufsc.monitoraap.model.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

	@Query("from Usuario u where lower(u.email) = lower(?)")
	Usuario findByEmail(String email);

	@Query("from Usuario u where u.facebookId = ?")
	Usuario findByFacebookId(String facebookId);

	@Query("select count(u) > 0 from Usuario u where u.facebookId = ?")
	boolean existsFacebookId(String facebookId);

	@Transactional
	@Modifying
	@Query("update Usuario u "
		+ "set u.facebookId = :facebookId "
		+ "where u.id = :id")
	void bindFacebookId(@Param("id") Integer id, @Param("facebookId") String facebookId);

}
