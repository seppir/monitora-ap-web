package br.ufsc.monitoraap.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ufsc.monitoraap.model.Documento;

@Repository
public interface DocumentoRepository extends JpaRepository<Documento, Integer> {}
