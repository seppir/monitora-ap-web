package br.ufsc.monitoraap.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.ufsc.monitoraap.model.Proposicao;

@Repository
public interface ProposicaoRepository extends JpaRepository<Proposicao, Integer> {

	@Query("from Proposicao p join fetch p.proponente where p.idAudienciaPublica = ?")
	List<Proposicao> findByAp(Integer idAp);

	@Query("from Proposicao p where p.proponente.id = :idProponente and p.idAudienciaPublica = :idAp")
	List<Proposicao> findByUserAndAp(@Param("idProponente") Integer idProponente, @Param("idAp") Integer idAp);

	@Query("select count(distinct p) from Proposicao p where p.proponente.id = :idProponente and p.idAudienciaPublica = :idAp")
	Integer countByUserAndAp(@Param("idProponente") Integer idProponente, @Param("idAp") Integer idAp);

}
