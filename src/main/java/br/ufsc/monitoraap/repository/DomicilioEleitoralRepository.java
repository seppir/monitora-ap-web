package br.ufsc.monitoraap.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.ufsc.monitoraap.model.DomicilioEleitoral;

@Repository
public interface DomicilioEleitoralRepository extends JpaRepository<DomicilioEleitoral, Integer> {

	@Query("from DomicilioEleitoral d where d.cidade.id = ?")
	List<DomicilioEleitoral> findByCity(Integer cityId);
	
	@Query("from DomicilioEleitoral d where d.zona = ? and d.cidade.id = ?")
	DomicilioEleitoral findByZona(String zona, Integer cityId);

}
