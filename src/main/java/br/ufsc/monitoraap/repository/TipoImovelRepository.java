package br.ufsc.monitoraap.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.ufsc.monitoraap.model.TipoImovel;

@Repository
public interface TipoImovelRepository extends JpaRepository<TipoImovel, Integer> {
	
	@Query("from TipoImovel t where t.descricao = ?")
	public TipoImovel findByNome (String nomeTipoImovel);
}
