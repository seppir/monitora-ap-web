package br.ufsc.monitoraap.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.ufsc.monitoraap.model.Video;

@Repository
public interface VideoRepository extends JpaRepository<Video, Integer> {

	@Transactional
	@Modifying
	@Query("update Video v "
		+ "set v.caminhoArquivo = :path "
		+ "where v.id = :id")
	void updateFilePath(@Param("id") Integer id, @Param("path") String path);

}
