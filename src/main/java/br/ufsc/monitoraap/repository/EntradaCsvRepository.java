package br.ufsc.monitoraap.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.ufsc.monitoraap.model.EntradaCsv;

@Repository
public interface EntradaCsvRepository extends JpaRepository<EntradaCsv, Integer> {

	@Query("select count(*) from EntradaCsv e where lower(e.email) = lower(?)")
	Integer countByEmail(String email);

}
