angular
	.module('app')
	.directive('datepopup', DatePopupDirective);

function DatePopupDirective() {
	return {
		restrict: 'E',
		replace: true,
		scope: {
			required: '=ngRequired',
			model: '=ngModel',
			minDate: '=minDate',
			maxDate: '=maxDate'
		},
		templateUrl: './views/partials/templates/datepopup.tpl.html',
		link: function(scope, elm, attrs, ctrl) {
			scope.status = {
				opened: false
			}
		}
	}
}
