angular
	.module('app')
	.directive('searchBox', SearchBoxDirective);

function SearchBoxDirective() {
	return {
		restrict: 'E',
		transclude: true,
		replace: true,
		scope: {
			title: '@',
			placeholder: '@',
			model: '=ngModel'
		},
		templateUrl: './views/partials/templates/searchbox.tpl.html',
		link: function(scope, elm, attrs, ctrl, $transclude) {
			scope.toggleFilters = function($event) {
				elm
					.find('#filters-parent')
					.toggleClass('hidden');
			};

			$transclude(function(clone) {
				scope.hasTransclude = clone.length > 1;
			});
		}
	}
}
