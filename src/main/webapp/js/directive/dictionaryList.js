angular
	.module('app')
	.directive('dictionaryList', ['$filter', '$window', DictionaryListDirective]);

function DictionaryListDirective($filter, $window) {

	var defaults = {
		fn: function() {
			return {
				then: function(fn) {
					fn();
				}
			}
		},
		attrValid: 'valido',
		attrDescription: 'descricao'
	};

	return {
		restrict: 'E',
		replace: true,
		require: 'ngModel',
		scope: {
			ngModel: '=',
			fnValid: '=',
			fnInvalid: '=',
			fnUpdate: '=',
			attrValid: '@',
			attrDescription: '@'
		},
		templateUrl: './views/partials/templates/dictionarylist.tpl.html',
		link: {
			pre: function(scope, elm, attrs, ctrl) {
				if(!attrs.fnValid)
					scope.fnValid = defaults.fn;

				if(!attrs.fnInvalid)
					scope.fnInvalid = defaults.fn;

				if(!attrs.fnUpdate)
					scope.fnUpdate = defaults.fn;

				if(!attrs.attrValid)
					scope.attrValid = defaults.attrValid;

				if(!attrs.attrDescription)
					scope.attrDescription = defaults.attrDescription;
			},
			post: function(scope, elm, attrs, ctrl) {

				scope.isScreenSmall = function() {
					return scope.screenWidth.factor <= scope.$root.ScreenWidth.SMALL.factor;
				};

				scope.setScreenWidth = function() {
					scope.screenWidth = scope.$root.getScreenWidth();
					scope.$apply(function() {
						if(scope.listas) {
							var screenSmall = scope.isScreenSmall();
							for(var i in scope.listas)
								if(!screenSmall)
									scope.listas[i].visible = true;
						}
					});
				};

				scope.screenWidth = scope.$root.getScreenWidth();

				// Identifica o tipo de modelo, se complexo ou simples
				scope.fetchModel = function(owner, attr) {
					if(scope.complexAttr !== undefined)
						return scope.complexAttr;
					if(attr.indexOf('.') === -1)
						return scope.complexAttr = false;
					scope.attrSegs = attr.split('.');
					scope.attrLast = scope.attrSegs.pop();
					scope.attrSegs = scope.attrSegs.join('.');
					return scope.complexAttr = true;
				};

				// Calcula se botao consta em botoesLista
				scope.inclui = function(botoesLista, botao) {
					return scope.$root.logicAnd(botoesLista, botao);
				};

				// Valores de botões para mostrar
				scope.Botao = {
					VALIDAR: 1,
					INVALIDAR: 2
				};

				// Listas de categorias de termos
				scope.listas = {
					notEvaluated: {
	                	 title: 'Não avaliados',
	                	 botoes: scope.Botao.VALIDAR | scope.Botao.INVALIDAR,
	                	 termos: [],
	                	 valido: null,
	                	 themeClass: 'panel-default',
	                	 proportion: 'col-md-4 col-sm-12',
	                	 visible: true
	                 },
	                 valid: {
	                	 title: 'Válidos',
	                	 botoes: scope.Botao.INVALIDAR,
	                	 termos: [],
	                	 valido: true,
	                	 themeClass: 'panel-success',
	                	 proportion: 'col-md-4 col-sm-6',
	                	 visible: true
	                 },
	                 notValid: {
	                	 title: 'Inválidos',
	                	 botoes: scope.Botao.VALIDAR,
	                	 termos: [],
	                	 valido: false,
	                	 themeClass: 'panel-danger',
	                	 proportion: 'col-md-4 col-sm-6',
	                	 visible: true
	                 }
				};

				// Divide a lista total de termos nas 3 listas baseado na validade do termo
				scope.filter = function() {
					angular.forEach(scope.listas, function(lista) {
						lista.termos = $filter('orderBy')(
							$filter('filter')(scope.ngModel, function(actual) {
								return actual[scope.attrValid] === lista.valido;
							}),
							scope.attrDescription
						);
					});
				};

				// Altera a validade do termo e chama a funcao definida
				scope.setValid = function(termo, valid, fn) {
					termo.pending = true;

					fn(termo)
						.then(function() {
							scope.$root.setComplexValue(termo, scope.attrValid, valid);
							termo.pending = false;
							scope.filter();
						});
				};

				// Altera a descricao do termo
				scope.update = function(termo) {
					termo.pending = true;

					scope.fnUpdate(termo)
						.then(function() {
							termo.pending = false;
							termo.changed = false;
							scope.filter();
						});
				};

				// Escuta alteracoes no ngModel para filtrar os termos
				scope.$watch('ngModel', scope.filter);

				angular.element($window).bind('resize', scope.setScreenWidth);

			}
		}
	}
}
