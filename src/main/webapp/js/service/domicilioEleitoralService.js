angular
	.module('app')
	.service('domicilioEleitoralService', ['$http', '$q', domicilioEleitoralService]);

function domicilioEleitoralService($http, $q) {

	this.findAll = findAll;
	this.findByCity = findByCity;

	function findAll() {
		var deferred = $q.defer();
		$http
			.get('./domicilioeleitoral/all')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function findByCity(cityId) {
		var deferred = $q.defer();
		$http
			.get('./domicilioeleitoral/porcidade/' + cityId)
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

}
