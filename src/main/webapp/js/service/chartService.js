angular
	.module('app')
	.service('chartService', ['$timeout', chartService]);

function chartService($timeout) {
	this.generateChart = generateChart;

	function generateChart(attr, data, title, fnGetDisplayData) {
		return {
			options: {
				chart: {
		            plotBackgroundColor: null,
		            plotBorderWidth: null,
		            plotShadow: false,
		            type: 'pie'
		        },
		        title: {
		            text: title || attr
		        },
		        tooltip: {
		            pointFormat: '<b>{point.percentage:.1f}%</b>'
		        },
		        plotOptions: {
		            pie: {
		                cursor: 'default',
		                dataLabels: {
		                    enabled: true,
		                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
		                    style: {
		                        color: Highcharts.theme && Highcharts.theme.contrastTextColor || 'black'
		                    }
		                }
		            }
		        }
			},
			series: [
			     {
				     colorByPoints: true,
				     data: getData(attr, data, fnGetDisplayData)
			     }
			],
			func: function(chart) {
				$timeout(function() {
					chart.reflow();
				}, 0);
			}
		}
	}

	function getData(attr, list, fnGetDisplayData) {
		var values = [];
		var data = [];
		var obj;
		for(var i in list) {
			obj = list[i];
			var value = getComplexValue(obj, attr);

			if(value == null) {
				if (values['Não informado']) {
					values['Não informado'].y++;
				} else {
					values['Não informado'] = {
							y: 1,
							name: 'Não informado'
					};
				}
			} else if (values[value]) {
				values[value].y++;
			} else {
				values[value] = {
					y: 1,
					name: fnGetDisplayData && fnGetDisplayData(obj) || value
				};
			}
		}
		for(var i in values) {
			data.push(values[i]);
		}
		return data;
	}

	function getFieldFromSource(value, displayData) {
		if(displayData) {
			var source = displayData.source;
			var compareAttr = displayData.compareAttr;
			for(var i in source) {
				var obj = source[i];
				if(value == obj[compareAttr]) {
					return obj[displayData.displayAttr];
				}
			}
		}
		return value;
	}

}
