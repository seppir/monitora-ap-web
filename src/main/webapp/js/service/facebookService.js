angular
	.module('app')
	.service('facebookService', ['$http', '$q', 'FB', facebookService]);

function facebookService($http, $q, FB) {

	const PERMISSIONS = 'public_profile,email,user_hometown,user_location,user_birthday,user_education_history,user_relationships';

	this.authenticate = authenticate;
	this.navigate = navigate;

	function authenticate() {
		var deferred = $q.defer();
		FB
			.login(function(response) {
				if(!response.error) {
					FB
						.api('/me?fields=name,gender,email,birthday,relationship_status,family{relationship},picture{url},education{type,year},hometown,location', function(data) {
							if(!data.error) {
								deferred.resolve(data);
							} else {
								deferred.reject(data);
							}
						});
				} else {
					deferred.reject(response);
				}
			}, {
				scope: PERMISSIONS
			});
		return deferred.promise;
	}

	function navigate(graphEndpoint) {
		var deferred = $q.defer();
		$http
			.get(graphEndpoint)
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

}
