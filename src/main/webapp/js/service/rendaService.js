angular
	.module('app')
	.service('rendaService', ['$http', '$q', rendaService]);

function rendaService($http, $q) {
	this.findAll = findAll;

	function findAll() {
		var deferred = $q.defer();
		$http
			.get('./renda/all')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

}
