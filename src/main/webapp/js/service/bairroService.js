angular
	.module('app')
	.service('bairroService', ['$http', '$q', bairroService]);

function bairroService($http, $q) {

	this.findAll = findAll;
	this.findByCity = findByCity;
	this.findOneWithAllData = findOneWithAllData;
	this.findByCityName = findByCityName;

	function findAll() {
		var deferred = $q.defer();
		$http
			.get('./bairro/all')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function findByCity(cityId) {
		var deferred = $q.defer();
		$http
			.get('./bairro/porcidade/' + cityId)
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function findOneWithAllData(id) {
		var deferred = $q.defer();
		$http
			.get('./bairro/withalldata/' + id)
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function findByCityName(cityName) {
		var deferred = $q.defer();
		$http
			.get('./bairro/nomecidade/' + escape(cityName))
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

}
