angular
	.module('app')
	.service('statusService', ['$http', '$q', statusService]);

function statusService($http, $q) {
	this.findAll = findAll;
	this.findOne = findOne;

	function findAll() {
		var deferred = $q.defer();
		$http
			.get('./status/all')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function findOne(id) {
		var deferred = $q.defer();
		$http
			.get('./status/' + id)
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

}
