angular
	.module('app')
	.service('tematicaService', ['$http', '$q', tematicaService]);

function tematicaService($http, $q) {
	this.findAll = findAll;

	function findAll() {
		var deferred = $q.defer();
		$http
			.get('./tematica/all')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

}

