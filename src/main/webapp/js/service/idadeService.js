angular
	.module('app')
	.service('idadeService', ['$http', '$q', idadeService]);

function idadeService($http, $q) {
	this.findAll = findAll;

	function findAll() {
		var deferred = $q.defer();
		$http
			.get('./idade/all')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

}
