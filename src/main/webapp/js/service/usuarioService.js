angular
	.module('app')
	.service('usuarioService', ['$http', '$q', usuarioService]);

function usuarioService($http, $q) {

	this.findOne = findOne;
	this.savePolitico = savePolitico;
	this.findByFacebookId = findByFacebookId;
	this.bindFacebook = bindFacebook;

	function findOne(id) {
		var deferred = $q.defer();
		$http
			.get('./usuarios/' + id)
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function savePolitico(user) {
		if(user.autenticacao && user.autenticacao2) {
			user.autenticacao = btoa(user.autenticacao);
			user.autenticacao2 = btoa(user.autenticacao2);
		} else {
			delete user.autenticacao;
			delete user.autenticacao2;
			if(user.autenticacaoAtual && user.autenticacaoTroca1 && user.autenticacaoTroca2) {
				user.autenticacaoAtual = btoa(user.autenticacaoAtual);
				user.autenticacaoTroca1 = btoa(user.autenticacaoTroca1);
				user.autenticacaoTroca2 = btoa(user.autenticacaoTroca2);
			} else {
				delete user.autenticacaoAtual;
				delete user.autenticacaoTroca1;
				delete user.autenticacaoTroca2;
			}
		}

		delete user.dataUltimaAtualizacao;

		var deferred = $q.defer();
		user['@type'] = 'Politico';
		if(user.hasOwnProperty('id')) {
			$http
				.put('./usuarios/' + user.id, user)
				.success(function(data) {
					deferred.resolve(data);
				});
		} else {
			$http
				.post('./usuarios/add', user)
				.success(function(data) {
					deferred.resolve(data);
				});
		}
		return deferred.promise;
	}

	function findByFacebookId(facebookId) {
		var deferred = $q.defer();
		$http
			.get('./usuarios/facebook/' + facebookId)
			.success(function(data) {
				deferred.resolve(data);
			})
			.error(function(data) {
				deferred.reject(data);
			});
		return deferred.promise;
	}

	function bindFacebook(facebookId) {
		var deferred = $q.defer();
		$http
			.put('./usuarios/facebook', facebookId)
			.success(function(data) {
				deferred.resolve(data);
			})
			.error(function(data) {
				deferred.reject(data);
			});
		return deferred.promise;
	}

}
