angular
	.module('app')
	.service('tagService', ['$http', '$q', tagService]);

function tagService($http, $q) {
	this.findAll = findAll;

	function findAll() {
		var deferred = $q.defer();
		$http
			.get('./tag/all')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

}
