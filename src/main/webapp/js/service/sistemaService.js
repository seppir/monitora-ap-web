angular
	.module('app')
	.service('sistemaService', ['$http', '$q', sistemaService]);

function sistemaService($http, $q) {
	this.getData = getData;

	function getData() {
		var deferred = $q.defer();
		$http
			.get('./sistema/dados')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

}
