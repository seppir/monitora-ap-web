angular
	.module('app')
	.service('proponenteService', ['$http', '$q', proponenteService]);

function proponenteService($http, $q) {

	this.save = save;
	this.findAll = findAll;
	this.saveFromExcel = saveFromExcel;

	function save(proponente) {
		if(proponente.autenticacao && proponente.autenticacao2) {
			proponente.autenticacao = btoa(proponente.autenticacao);
			proponente.autenticacao2 = btoa(proponente.autenticacao2);
		} else {
			delete proponente.autenticacao;
			delete proponente.autenticacao2;
			if(proponente.autenticacaoAtual && proponente.autenticacaoTroca1 && proponente.autenticacaoTroca2) {
				proponente.autenticacaoAtual = btoa(proponente.autenticacaoAtual);
				proponente.autenticacaoTroca1 = btoa(proponente.autenticacaoTroca1);
				proponente.autenticacaoTroca2 = btoa(proponente.autenticacaoTroca2);
			} else {
				delete proponente.autenticacaoAtual;
				delete proponente.autenticacaoTroca1;
				delete proponente.autenticacaoTroca2;
			}
		}

		delete proponente.dataUltimaAtualizacao;

		var deferred = $q.defer();
		proponente['@type'] = 'Proponente';
		if(proponente.hasOwnProperty('id') && proponente.id) {
			$http
				.put('./proponentes/' + proponente.id, proponente)
				.success(function(data) {
					deferred.resolve(data);
				}).error(function(data) {
					deferred.reject(data);
				});
		} else {
			$http
				.post('./proponentes/add', proponente)
				.success(function(data) {
					deferred.resolve(data);
				}).error(function(data) {
					deferred.reject(data);
				});
		}
		return deferred.promise;
	}

	function findAll() {
		var deferred = $q.defer();
		$http
			.get('./proponentes/all')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}
	
	function saveFromExcel (idProponente, dataCadastro , documento){
		var deferred = $q.defer();
		$http
		.post('./proponentes/savefromexcel', documento)
		.success(function(data) {
			deferred.resolve(data);
		}).error(function(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	}
	
	function saveFromExcel (documento){
		var deferred = $q.defer();
		$http
		.post('./proponentes/savefromexcel', documento)
		.success(function(data) {
			deferred.resolve(data);
		}).error(function(data) {
			deferred.reject(data);
		});
		return deferred.promise;
	}

}
