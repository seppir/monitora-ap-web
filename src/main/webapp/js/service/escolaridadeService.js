angular
	.module('app')
	.service('escolaridadeService', ['$http', '$q', escolaridadeService]);

function escolaridadeService($http, $q) {
	this.findAll = findAll;

	function findAll() {
		var deferred = $q.defer();
		$http
			.get('./escolaridade/all')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

}
