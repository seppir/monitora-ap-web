angular
	.module('app')
	.service('videoYoutubeService', ['$http', '$q', videoYoutubeService]);

function videoYoutubeService($http, $q) {

	const API_KEY = 'AIzaSyCoSSbYmZc2-t9wBNDs34GxakF1RpYsxpc';
	const URL_WATCH = 'https://www.youtube.com/watch?v=';
	const URL_EMBED = 'http://www.youtube.com/embed/';

	this.getData = getData;

	function getData(url) {
		var deferred = $q.defer();
		var id = getId(url);
		$http
			.get('https://www.googleapis.com/youtube/v3/videos?part=snippet&key=' + API_KEY + '&id=' + id)
			.success(function(data) {
				if(data.pageInfo.totalResults === 1) {
					var video = data.items[0].snippet;
					deferred.resolve({
						id: id,
						title: video.localized.title,
						channelTitle: video.channelTitle,
						watchUrl: URL_WATCH + id,
						embedUrl: URL_EMBED + id,
						thumbUrl: video.thumbnails.default.url
					});
				} else {
					deferred.reject();
				}
			});
		return deferred.promise;
	}

	function getId(url) {
		var params = getUrlParams(url);
		if(params) {
			return params.v;
		}
		if(url.indexOf('/') !== -1) {
			var parts = url.split('/');
			return parts[parts.length - 1];
		}
		return url;
	}

}
