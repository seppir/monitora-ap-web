angular
	.module('app')
	.service('videoService', ['$http', '$q', videoService]);

function videoService($http, $q) {

	this.remove = remove;

	function remove(id) {
		var deferred = $q.defer();
		$http
			.delete('./video/' + id)
			.success(function(data) {
				deferred.resolve(data);
			})
			.error(function(error) {
				deferred.reject(error);
			});
		return deferred.promise;
	}

}
