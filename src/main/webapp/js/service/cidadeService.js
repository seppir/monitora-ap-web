angular
	.module('app')
	.service('cidadeService', ['$http', '$q', cidadeService]);

function cidadeService($http, $q) {

	this.findAll = findAll;
	this.findByState = findByState;
	this.findByCEP = findByCEP;
	this.findByName = findByName;

	function findAll() {
		var deferred = $q.defer();
		$http
			.get('./cidade/all')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function findByState(stateId) {
		var deferred = $q.defer();
		$http
			.get('./cidade/porestado/' + stateId)
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function findByCEP(cep) {
		var deferred = $q.defer();
		$http
			.get('https://viacep.com.br/ws/'+cep+'/json/')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function findByName(name) {
		var deferred = $q.defer();
		$http
			.get('./cidade/pornome/' + escape(name))
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

}
