angular
	.module('app')
	.service('permissionService', ['$http', '$q', permissionService]);

function permissionService($http, $q) {
	this.findAll = findAll;

	function findAll() {
		var deferred = $q.defer();
		$http
			.get('./permissoes/all')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

}
