angular
	.module('app')
	.service('logService', ['$http', '$q', logService]);

function logService($http, $q) {

	this.reloadConfig = reloadConfig;

	function reloadConfig() {
		var deferred = $q.defer();
		$http
			.patch('./log/reloadconfig')
			.then(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

}
