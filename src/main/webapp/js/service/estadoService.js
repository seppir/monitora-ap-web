angular
	.module('app')
	.service('estadoService', ['$http', '$q', estadoService]);

function estadoService($http, $q) {

	this.findAll = findAll;
	this.findByCity = findByCity;

	function findAll() {
		var deferred = $q.defer();
		$http
			.get('./estado/all')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function findByCity(idCidade) {
		var deferred = $q.defer();
		$http
			.get('./estado/porcidade/' + idCidade)
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

}
