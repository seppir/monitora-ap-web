angular
	.module('app')
	.service('termosService', ['$http', '$q', termosService]);

function termosService($http, $q) {

	this.findAll = findAll;
	this.findByAp = findByAp;
	this.updateValid = updateValid;
	this.updateValidAp = updateValidAp;
	this.updateDescription = updateDescription;
	this.updateDescriptionAp = updateDescriptionAp;

	function findAll() {
		var deferred = $q.defer();
		$http
			.get('./termo/all')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function findByAp(idAp) {
		var deferred = $q.defer();
		$http
			.get('./termo/ap/' + idAp)
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function updateValid(idTermo, valido) {
		var deferred = $q.defer();
		$http
			.put('./termo/' + idTermo + '/valido', valido)
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function updateValidAp(idAp, idTermo, validoParaCp) {
		var deferred = $q.defer();
		$http
			.put('./termo/ap/' + idAp + '/' + idTermo + '/valido', validoParaCp)
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function updateDescription(idTermo, description) {
		var deferred = $q.defer();
		$http
			.put('./termo/' + idTermo + '/atualizar', description)
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function updateDescriptionAp(idAp, idTermo, description) {
		var deferred = $q.defer();
		$http
			.put('./termo/ap/' + idAp + '/' + idTermo + '/atualizar', description)
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}


}
