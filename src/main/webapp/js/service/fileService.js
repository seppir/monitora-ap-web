angular
	.module('app')
	.service('fileService', ['$http', '$q', '_', FileService]);

function FileService($http, $q, _) {

	// Limite = 2GB
	const MAX_FILE_SIZE = 1024 * 1024 * 1024 * 2;

	// Limite para arquivo ser lido inteiro = 500MB
	const MAX_WHOLE_FILE_SIZE = MAX_FILE_SIZE / 4;

	this.readFile = readFile;
	this.uploadVideos = uploadVideos;

	function readFile(unloadedFileOrFiles, type) {
		var deferred = $q.defer();
		if(unloadedFileOrFiles) {
			if(_.isArray(unloadedFileOrFiles)) {
				if(unloadedFileOrFiles.length) {
					var loadedFiles = [];
					unloadedFileOrFiles.forEach(function(file, idx) {
						readSingleFile(file).then(function(f) {
								loadedFiles.push({
									'@type': type,
									nome: f.nome,
									conteudo: f.conteudo,
									tamanho: f.tamanho
								});
								if(idx === unloadedFileOrFiles.length - 1) {
									deferred.resolve(loadedFiles);
								}
							});
					});
				} else {
					deferred.reject();
				}
			} else {
				readSingleFile(unloadedFileOrFiles).then(function(f) {
					deferred.resolve({
						'@type': type,
						nome: f.nome,
						conteudo: f.conteudo,
						tamanho: f.tamanho
					});
				});
			}
		} else {
			deferred.reject();
		}

		return deferred.promise;
	}

	function readSingleFile(file) {
		if(file.size > MAX_FILE_SIZE)
			return;

		if(file.size <= MAX_WHOLE_FILE_SIZE)
			return readWholeFile(file);

		return readSingleFileAsChunks(file);
	}

	function readWholeFile(file) {
		var deferred = $q.defer();

		var reader = new FileReader();
		reader.onload = function(f) {
			return function(e) {
				deferred.resolve({
					nome: f.name,
					conteudo: strToBytes(e.target.result),
					tamanho: f.size
				});
			};
		}(file);
		reader.onerror = reader.onabort = function() {
			deferred.reject();
		};
		reader.readAsBinaryString(file);

		return deferred.promise;
	}

	function readSingleFileAsChunks(file) {
		// http://stackoverflow.com/a/25813769/5468952
		var deferred = $q.defer();

		var CHUNK_SIZE = 1024 * 1024 * 100;
		var offset = 0;
		var fr = new FileReader();
		var completeFile = [];
		fr.onload = function() {
			var view = new Uint8Array(fr.result);
		    completeFile = completeFile.concat(view);
		    offset += CHUNK_SIZE;
		    seek();
		};
		fr.onerror = fr.onabort = function() {
			deferred.reject();
		};
		seek();

		return deferred.promise;

		function seek() {
		    if (offset > file.size + CHUNK_SIZE) {
		    	// Fim da leitura do arquivo
		        deferred.resolve({
		        	nome: file.name,
					conteudo: completeFile,
					tamanho: file.size
		        });
		        return;
		    }
		    var slice = file.slice(offset, offset + CHUNK_SIZE);
		    fr.readAsArrayBuffer(slice);
		    deferred.notify({
		    	nome: file.name,
		    	progress: offset / file.size * 52
		    });
		}
	}

	function uploadVideos(files, apId) {
		var deferred = $q.defer();
		var done = 0;
		for(var i in files) {
			var fd = new FormData();
	        fd.append('file', files[i]);
	        $http
		        .post('./video/upload/' + apId, fd, {
		            transformRequest: angular.identity,
		            headers: {
		            	'Content-Type': undefined
		            }
		        })
		        .success(function() {
		        	deferred.notify({
		        		sucess: true
		        	});
		        	if(++done === files.length) {
		        		deferred.resolve();
		        	}
		        })
		        .error(function(error) {
		        	deferred.reject(error);
		        });
		}
		return deferred.promise;
	}

}
