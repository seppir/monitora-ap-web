angular
	.module('app')
	.service('tipoImovelService', ['$http', '$q', tipoImovelService]);

function tipoImovelService($http, $q) {
	this.findAll = findAll;

	function findAll() {
		var deferred = $q.defer();
		$http
			.get('./tipoimovel/all')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

}
