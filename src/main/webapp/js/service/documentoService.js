angular
	.module('app')
	.service('documentoService', ['$http', '$q', documentoService]);

function documentoService($http, $q) {

	this.remove = remove;

	function remove(id) {
		var deferred = $q.defer();
		$http
			.delete('./documento/' + id)
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

}
