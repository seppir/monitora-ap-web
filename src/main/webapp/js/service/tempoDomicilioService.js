angular
	.module('app')
	.service('tempoDomicilioService', ['$http', '$q', tempoDomicilioService]);

function tempoDomicilioService($http, $q) {
	this.findAll = findAll;

	function findAll() {
		var deferred = $q.defer();
		$http
			.get('./tempodomicilio/all')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

}
