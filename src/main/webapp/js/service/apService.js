angular
	.module('app')
	.service('apService', ['$http', '$q', apService]);

function apService($http, $q) {

	this.findAll = findAll;
	this.findOne = findOne;
	this.save = save;
	this.remove = remove;
	this.findMine = findMine;
	this.findAllWithCreator = findAllWithCreator;
	this.findOneWithCreator = findOneWithCreator;
	this.findAllWithPropositions = findAllWithPropositions;
	this.getKeywordsUrl = getKeywordsUrl;

	function findAll() {
		var deferred = $q.defer();
		$http
			.get('./ap/all')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function findOne(id) {
		var deferred = $q.defer();
		$http
			.get('./ap/' + id)
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function findOneWithCreator(id) {
		var deferred = $q.defer();
		$http
			.get('./ap/creator/' + id)
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function save(ap) {
		var deferred = $q.defer();
		delete ap.videos;
		if(ap.hasOwnProperty('id')) {
			$http
				.put('./ap/updateap/' + ap.id, ap)
				.success(function(data) {
					deferred.resolve(data);
				});
		} else {
			$http
				.post('./ap/addap', ap)
				.success(function(data) {
					deferred.resolve(data);
				});
		}
		return deferred.promise;
	}

	function remove(id) {
		var deferred = $q.defer();
		$http
			.delete('./ap/' + id)
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function findMine() {
		var deferred = $q.defer();
		$http
			.get('./ap/mine')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function findAllWithCreator() {
		var deferred = $q.defer();
		$http
			.get('./ap/allwithcreator')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function findAllWithPropositions() {
		var deferred = $q.defer();
		$http
			.get('./ap/allwithpropositions')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function getKeywordsUrl (apIds){
		var baseUrl = '/apsearch/APService';
		var url = baseUrl + '?option=histogram&number=5';

		if(apIds) {
			url += '&filters=' + writeFilterJson({idAp: (apIds instanceof Array && apIds.length > 1 ? apIds.join() : apIds[0])});
		}

		return url;

		function writeFilterJson(filtros) {
			var str = '{';
			for(var prop in filtros) {
				var f = filtros[prop];
				str += '"' + prop + '":"';
				if(f.hasOwnProperty('id')) {
					str += f.id + '"';
				} else {
					str += f + '"';
				}
				str += ',';
			}
			return str.substring(0, str.length - 1) + '}';
		}
	};

}
