angular
	.module('app')
	.service('estadoCivilService', ['$http', '$q', estadoCivilService]);

function estadoCivilService($http, $q) {
	this.findAll = findAll;

	function findAll() {
		var deferred = $q.defer();
		$http
			.get('./estadocivil/all')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

}
