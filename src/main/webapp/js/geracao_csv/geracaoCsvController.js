angular
	.module('app')
	.controller('GeracaoCsvController', ['$scope', '$window', '$modal', 'idadeService', 'cidadeService', 'bairroService', 'proposicaoService',
	                                     'entradaCsvService', GeracaoCsvController]);

function GeracaoCsvController($scope, $window, $modal, idadeService, cidadeService, bairroService, proposicaoService, entradaCsvService) {

	var LOGIN = 'admin';
	var PASSWORD = 'adminfloripamelhor';

	$scope.loading = function(visible) {
		angular.element('#preloader, .loader')
			.css('display', visible ? 'block' : 'none');
	};

	$scope.user = {};
	$scope.resposta = {};
	$scope.data = {};

	$scope.login = function() {
		if($scope.user) {
			$scope.user.authenticated = ($scope.user.username === LOGIN && $scope.user.password === PASSWORD);
			$scope.listarEntradas();

			angular.element('body.push-body-toleft').removeClass('push-body-toleft');
			angular.element('section.menu-open').removeClass('menu-open');
		}
	};

	$scope.logout = function() {
		$scope.user = {};
	};

	$scope.listarEntradas = function() {
		entradaCsvService
			.findAll()
			.then($scope.assign('entradas'));
	};

	$scope.exportToExcel = function() {
		var modal = $modal.open({
			templateUrl: './views/geracao_csv/codigos_aps.tpl.html',
			controller: 'ExportarRespostasController'
		});
		modal.result.then(function(codigos) {
			var proposicoes = [];
			for(var i in codigos) {
				var respostas = [];
				for(var j in $scope.entradas) {
					respostas.push($scope.entradas[j]['resposta' + (parseInt(i) + 1)])
				}
				proposicoes[i] = {
					codigo: codigos[i],
					respostas: respostas
				};
			}

			proposicaoService
				.exportToExcel(proposicoes)
				.then(function(id) {
					$window.open('./documento-pendente/download/' + id, '_blank');
				})
				.catch(function(error) {
					$scope.alert(error.message);
				});
		});
		modal.rendered.then(function() {
			angular.element('.modal-backdrop')
				.prop('ng-style', null)
				.css('z-index', 8000);
			angular.element('.modal')
				.prop('ng-style', null)
				.css('z-index', 8001);
		});
	};

	$scope.podeAvancar = function() {
		return $scope.resposta && $scope.resposta.resposta1 && $scope.resposta.resposta2
			&& $scope.resposta.resposta1.trim() != '' && $scope.resposta.resposta2.trim() != '';
	};

	$scope.salvar = function() {
		$scope.data.pending = true;
		entradaCsvService
			.save($scope.resposta)
			.then(function(data) {
				$scope.alert('Obrigado pela sua opinião! Um email de confirmação foi enviado a <b>' + $scope.resposta.email + '</b>.', function() {
					$window.location.reload();
				});
			})
			.catch(function(error) {
				$scope.data.pending = false;
				$scope.alert(error.message);
			});
	};

	var FLORIANOPOLIS = 'Florianópolis';
	$scope.ID_SANTA_CATARINA = 24;

	$scope.listarBairros = function() {
		if($scope.data.idCidade) {
			bairroService
				.findByCity($scope.data.idCidade)
				.then($scope.assign('bairros'));
		} else {
			$scope.bairros = null;
		}
	};

	idadeService
		.findAll()
		.then($scope.assign('idades'));

	cidadeService
		.findByState($scope.ID_SANTA_CATARINA)
		.then(function(data) {
			$scope.cidades = data;

			var cidade;
			for(var i in $scope.cidades) {
				cidade = $scope.cidades[i];
				if(cidade.nome == FLORIANOPOLIS) {
					$scope.data.idCidade = cidade.id;
					bairroService
						.findByCityName(FLORIANOPOLIS)
						.then($scope.assign('bairros'));
					return;
				}
			}
		});

}
