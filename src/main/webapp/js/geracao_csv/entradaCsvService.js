angular
	.module('app')
	.service('entradaCsvService', ['$http', '$q', entradaCsvService]);

function entradaCsvService($http, $q) {

	this.findAll = findAll;
	this.save = save;

	function findAll() {
		var deferred = $q.defer();
		$http
			.get('./entradacsv/all')
			.success(function(data) {
				deferred.resolve(data);
			});
		return deferred.promise;
	}

	function save(entradaCsv) {
		var deferred = $q.defer();
		if(entradaCsv.hasOwnProperty('id') && entradaCsv.id) {
			throw 'NÃO PODE EDITAR!';
		} else {
			$http
				.post('./entradacsv/add', entradaCsv)
				.success(function(data) {
					deferred.resolve(data);
				}).error(function(data) {
					deferred.reject(data);
				});
		}
		return deferred.promise;
	}

}
