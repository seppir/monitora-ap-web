angular
	.module('app')
	.controller('ExportarRespostasController', ['$scope', '$modalInstance', 'proposicaoService', ExportarRespostasController]);

function ExportarRespostasController($scope, $modalInstance, proposicaoService) {

	$scope.data = {};

	$scope.exportar = function() {
		$modalInstance.close($scope.data.codigos);
	};

}
