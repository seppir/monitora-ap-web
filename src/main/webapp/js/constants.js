const ScreenWidth = {
	LARGE: {
		code: 'lg',
		factor: 8
	},
	MEDIUM: {
		code: 'md',
		factor: 4
	},
	SMALL: {
		code: 'sm',
		factor: 2
	},
	EXTRA_SMALL: {
		code: 'xs',
		factor: 1
	}
};

const Permissao = {
	ADMIN: 1,
	POLITICO: 2,
	PROPONENTE: 3
};

const Sexo = {
	MASCULINO: {
		value: 'MASCULINO',
		label: 'Masculino',
		resumo: 'M',
		fbDescription: 'male'
	},
	FEMININO: {
		value: 'FEMININO',
		label: 'Feminino',
		resumo: 'F',
		fbDescription: 'female'
	}
};

const OpcoesBoolean = {
	SIM: {
		value: true,
		label: 'Sim'
	},
	NAO: {
		value: false,
		label: 'Não'
	}
};

const Escolaridade = {
	MED_INCOMPLETO: 4,
	MED_COMPLETO: 5,
	SUP_INCOMPLETO: 6,
	SUP_COMPLETO: 7,
	POS_INCOMPLETO: 8,
	POS_COMPLETO: 9
};

const EstadoCivil = {
	CASADO: {
		id: 1,
		fbDescription: 'Married'
	},
	SOLTEIRO: {
		id: 2,
		fbDescription: 'Single'
	},
	SEPARADO: {
		id: 3,
		fbDescription: ['Separated', 'Divorced']
	},
	CIVIL_UNION: {
		id: 4,
		fbDescription: 'In a civil union'
	},
	WIDOWED: {
		id: 5,
		fbDescription: 'Widowed'
	}
};
