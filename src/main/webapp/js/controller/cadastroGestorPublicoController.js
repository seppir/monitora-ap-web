angular
	.module('app')
	.controller('CadastroGestorPublicoController', ['$scope', '$filter','$modalInstance', 'user', 'usuarioService', CadastroGestorPublicoController]);

function CadastroGestorPublicoController($scope, $filter, $modalInstance, user, usuarioService) {

	$scope.salvar = function() {
		if($scope.form.$valid) {
			usuarioService
				.savePolitico($scope.p)
				.then(function() {
					$modalInstance.close({
						username: $scope.p.email,
						password: atob($scope.p.autenticacao)
					});
				}).catch(function(error) {
					$scope.alert(error.message);
				});
		} else {
			alert('Formulário inválido');
		}
	}

	$scope.cancelar = function() {
		$modalInstance.dismiss('cancel');
	}

	$scope.p = {
		autenticacao: '',
		autenticacao2: ''
	};

	if(user) {
		$scope.p.facebookId = user.id;
		$scope.p.nome = user.name;
		$scope.p.email = user.email;
	}

}
