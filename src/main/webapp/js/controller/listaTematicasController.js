angular
	.module('app')
	.controller('ListaTematicasController', ['$scope', '$stateParams', '_', '$interval', '$sce', 'proposicaoService', 'cidadeService', 'bairroService',
	                                         'tempoDomicilioService', 'tipoImovelService', 'estadoCivilService', 'escolaridadeService',
	                                         'idadeService', 'rendaService', 'domicilioEleitoralService', ListaTematicasController]);

function ListaTematicasController($scope, $stateParams, _, $interval, $sce, proposicaoService, cidadeService, bairroService, tempoDomicilioService,
	tipoImovelService, estadoCivilService, escolaridadeService, idadeService, rendaService, domicilioEleitoralService) {

	$scope.ITEMS_PER_PAGE = 10;
	$scope.indexArray = [];
	$scope.propositionsNodes = [];
	$scope.loading(true);

	$scope.resumoFiltro = function() {
		var filtros = $scope.Filtro;
		var ativos = [];
		for(var i in filtros) {
			var f = filtros[i];
			if(f.filterCondition()) {
				ativos.push(f.title);
			}
		}
		return ativos.length ? '(' + ativos + ')' : '';
	}

	$scope.valoresFiltros = function() {
		var filtros = $scope.Filtro;
		var resumo = '';
		for(var i in filtros) {
			var f = filtros[i];
			if(f.filterCondition()) {
				resumo += '<b>' + f.title + ': </b>' + f.getDisplayData() + '<br />';
			}
		}
		return $sce.trustAsHtml(resumo);
	}

	$scope.Filtro = {
		BAIRRO: {
			id: 1,
			property: 'bairro.id',
			title: 'Bairro',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.bairro && $scope.filtro.bairro.id;
			},
			getDisplayData: function() {
				return $scope.filtro.bairro.nomeApresentacao;
			}
		},
		TEMPO_DOMICILIO: {
			id: 2,
			property: 'tempoDomicilio.id',
			title: 'Tempo de Domicílio',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.tempoDomicilio && $scope.filtro.tempoDomicilio.id;
			},
			getDisplayData: function() {
				return $scope.filtro.tempoDomicilio.descricao;
			}
		},
		TIPO_IMOVEL: {
			id: 3,
			property: 'tipoImovel.id',
			title: 'Tipo de Imóvel',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.tipoImovel && $scope.filtro.tipoImovel.id;
			},
			getDisplayData: function() {
				return $scope.filtro.tipoImovel.descricao;
			}
		},
		CIDADE_NATAL: {
			id: 4,
			property: 'cidadeNatal.id',
			title: 'Cidade natal',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.cidadeNatal && $scope.filtro.cidadeNatal.id;
			},
			getDisplayData: function() {
				return $scope.filtro.cidadeNatal.nome;
			}
		},
		SEXO: {
			id: 5,
			property: 'sexo',
			title: 'Sexo',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.sexo;
			},
			getDisplayData: function() {
				var sexo = $scope.filtro.sexo;
				var s = $scope.Sexo;
				for(var i in s) {
					if(s[i].value == sexo) {
						return s[i].label;
					}
				}
				return sexo;
			}
		},
		ESCOLARIDADE: {
			id: 6,
			property: 'escolaridade.id',
			title: 'Escolaridade',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.escolaridade && $scope.filtro.escolaridade.id;
			},
			getDisplayData: function() {
				return $scope.filtro.escolaridade.descricao;
			}
		},
		IDADE: {
			id: 7,
			property: 'idade.id',
			title: 'Faixa Etária',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.idade && $scope.filtro.idade.id;
			},
			getDisplayData: function() {
				return $scope.filtro.idade.descricao;
			}
		},
		ESTADO_CIVIL: {
			id: 8,
			property: 'estadoCivil.id',
			title: 'Estado Civil',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.estadoCivil && $scope.filtro.estadoCivil.id;
			},
			getDisplayData: function() {
				return $scope.filtro.estadoCivil.descricao;
			}
		},
		RENDA: {
			id: 9,
			property: 'renda.id',
			title: 'Renda',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.renda && $scope.filtro.renda.id;
			},
			getDisplayData: function() {
				return $scope.filtro.renda.descricao;
			}
		},
		NUMERO_FILHOS: {
			id: 10,
			property: 'numFilhos',
			title: 'Número de Filhos',
			filterCondition: function() {
				return $scope.filtro.hasOwnProperty('numFilhos');
			},
			getDisplayData: function() {
				return $scope.filtro.numFilhos;
			}
		},
		NUMERO_DEPENDENTES_DIRETOS: {
			id: 11,
			property: 'numDependentesDiretos',
			title: 'Número de Dependentes Diretos',
			filterCondition: function() {
				return $scope.filtro.hasOwnProperty('numDependentesDiretos');
			},
			getDisplayData: function() {
				return $scope.filtro.numDependentesDiretos;
			}
		},
		DOMICILIO_ELEITORAL: {
			id: 12,
			property: 'domicilioEleitoral.id',
			title: 'Domicílio Eleitoral',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.domicilioEleitoral && $scope.filtro.domicilioEleitoral.id;
			},
			getDisplayData: function() {
				return $scope.filtro.domicilioEleitoral.zona;
			}
		},
		SERVIDOR_PUBLICO: {
			id: 13,
			property: 'servidorPublico',
			title: 'Servidor Público',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.servidorPublico;
			},
			getDisplayData: function() {
				var opcao = $scope.filtro.servidorPublico;
				var o = $scope.OpcoesBoolean;
				for(var i in o) {
					if(o[i].value == opcao) {
						return o[i].label;
					}
				}
				return opcao;
			}
		},
		POSSUI_CPF: {
			id: 14,
			property: 'possuiCPF',
			title: 'Possui CPF',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.possuiCPF;
			},
			getDisplayData: function(proponente) {
				var opcao = $scope.filtro.possuiCPF;
				var o = $scope.OpcoesBoolean;
				for(var i in o) {
					if(o[i].value == opcao) {
						return o[i].label;
					}
				}
				return opcao;
			}
		}
	};

	$scope.limit = function(p) {
		if(p) {
			return p.limit || (p.limit = 80);
		}
	}

	$scope.toggleLimit = function(p) {
		if(p) {
			if(p.limit === Infinity) {
				p.limit = 80;
			} else {
				p.limit = Infinity;
			}
		}
	}

	$scope.getIcon = function(p) {
		return p.limit == Infinity ?
			'glyphicon-minus' : 'glyphicon-plus';
	}

	$scope.getGender = function(value) {
		var genders = $scope.Sexo;
		for(var i in genders) {
			if(value === genders[i].value) {
				return genders[i].resumo;
			}
		}
		return '';
	}

	$scope.getScore = function(value) {
		if(value && $scope.resposta){
			return value / $scope.resposta.response.maxScore ;
		}
		return '';
	}

	$scope.getTexto = function(value) {
		return decodeURIComponent(value);
	}

	$scope.getBoolean = function(value) {
		return simpleSearch($scope.OpcoesBoolean, value, 'value').label;
	}

	$scope.getNodes = function(value){
		var nodes = _.findWhere($scope.propositionsNodes, {id: value});

		if(!nodes){
			proposicaoService.retrieveNodes(value).then(function(data) {
				var strNodes = "";
				data.map(function(item){
					strNodes += item + ", ";
				});
				strNodes = strNodes.substring(0, strNodes.length-2);

				$scope.propositionsNodes.push({id: value, text: strNodes});
				return strNodes;
			});
		} else {
			return nodes.text;
		}
	}

	$scope.status = {
		firstOpen: true,
		secondOpen: false
	}

	if($stateParams.hasOwnProperty('filtro') && $stateParams.filtro) {
		var f = JSON.parse($stateParams.filtro);
		if(Object.keys(f).length) {
			$scope.filtro = f;
		}
	}

	$scope.aps = JSON.parse($stateParams.aps);
	$scope.apIds = [];
	for(var i in $scope.aps) {
		$scope.apIds.push($scope.aps[i].i);
	}
	$scope.data = {
		palavraChave: JSON.parse($stateParams.palavraChave)
	};

	var loadingPropositions = false;
	function load() {
		if(!loadingPropositions) {
			loadingPropositions = true;
			proposicaoService
				.findBySubject($scope.data.palavraChave, $scope.filtro, $scope.apIds)
				.then(function(resposta) {
					$scope.resposta = resposta;
					$scope.indexArray = $scope.emptyArray($scope.resposta.response.docs.length/ $scope.ITEMS_PER_PAGE);
					$scope.loading(false);
				});
			loadingPropositions = false;
		}
	}

	load();

	$scope.emptyArray = function(length) {
		return new Array(Math.ceil(length));
	};

	$scope.beginList = function(currentPage, showAll) {
		return showAll ? 0 : currentPage * $scope.ITEMS_PER_PAGE;
	};

	$scope.endList = function(items, showAll) {
		return showAll ? items.length : $scope.ITEMS_PER_PAGE;
	};

	$scope.updateSorting = function(field) {
		$scope.desc = $scope.ordination == field ? !$scope.desc : false;
		$scope.ordination = field;
	};

	$scope.updateSortIcon = function(field) {
		if($scope.ordination == field) {
			return $scope.desc ?
				'glyphicon-chevron-down' : 'glyphicon-chevron-up';
		}
	};
}
