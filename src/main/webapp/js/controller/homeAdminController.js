angular
	.module('app')
	.controller('HomeAdminController', ['logService', HomeAdminController]);

function HomeAdminController(logService) {

	var vm = this;

	vm.reloadLogConfig = function() {
		logService
			.reloadConfig()
			then(function() {
				vm.alert('Configurações recarregadas!');
			});
	};

}
