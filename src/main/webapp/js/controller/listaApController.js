angular
	.module('app')
	.controller('ListaApController', ['$scope', '$filter', '$state', 'apService', 'statusService', 'tagService' , ListaApController]);

function ListaApController($scope, $filter,  $state, apService, statusService, tagService) {

	$scope.loading(true);

	$scope.apsParaVisualizar = [];
	$scope.filtro = {};
	$scope.inicio = {};
	$scope.encerramento = {};
	$scope.tags = [];
	$scope.aps = [];
	$scope.ITEMS_PER_PAGE = 10;
	$scope.indexArray = [];
	$scope.ordination = 'dataInicio';
	$scope.desc = true;

	$scope.selecionaAp = function(ap) {
		if(ap.selected) {
			$scope.apsParaVisualizar.splice($scope.apsParaVisualizar.indexOf(ap.id), 1);
		} else {
			$scope.apsParaVisualizar.push(ap.id);
		}
		ap.selected = !ap.selected;
	}

	$scope.podeAvancar = function() {
		return $scope.apsParaVisualizar.length > 0;
	}

	$scope.avancar = function() {
		var ids = $scope.apsParaVisualizar;
		if(ids.length) {
			$state.go('ap.profiles', {ids: ids});
		}
	}

	$scope.verificaRemoverFiltro = function(filtro, complexField) {
		if(complexField) {
			if(!getComplexValue($scope.filtro, complexField)) {
				delete $scope.filtro[filtro];
			}
		} else if(!$scope.filtro[filtro]) {
			delete $scope.filtro[filtro];
		}
	}

	$scope.emptyArray = function(length) {
		return new Array(Math.ceil(length));
	};

	$scope.beginList = function(currentPage, showAll) {
		return showAll ? 0 : currentPage * $scope.ITEMS_PER_PAGE;
	};

	$scope.endList = function(items, showAll) {
		return showAll ? items.length : $scope.ITEMS_PER_PAGE;
	};

	$scope.updateSorting = function(field) {
		$scope.desc = $scope.ordination == field ? !$scope.desc : false;
		$scope.ordination = field;
	};

	$scope.updateSortIcon = function(field) {
		if($scope.ordination == field) {
			return $scope.desc ?
				'glyphicon-chevron-down' : 'glyphicon-chevron-up';
		}
	};

	$scope.removerAp = function(id) {
		$scope.confirm(
			'Excluir Consulta Pública',
			'Tem certeza de que deseja excluir esta Consulta Pública? Isto não pode ser desfeito!',
			function() {
				apService
					.remove(id)
					.then(function() {
						$scope.loading(true);
						$scope.searchAps();
					});
			}
		)
	};

	$scope.searchAps = function() {
		apService
			.findMine()
			.then(function(data) {
				$scope.aps = data;
				$scope.filteredAps= data;
				$scope.indexArray = $scope.emptyArray($scope.filteredAps.length / $scope.ITEMS_PER_PAGE);
				$scope.loading(false);
			});
	};

	statusService
		.findAll()
		.then($scope.assign('statuses'));

	tagService
		.findAll()
		.then($scope.assign('todasTags'));

	$scope.searchAps();

}
