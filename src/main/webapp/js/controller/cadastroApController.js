angular
	.module('app')
	.controller('CadastroApController', ['$scope', '$state', '$stateParams', 'apService', 'statusService', 'tagService',
	                                     'fileService', 'videoService', 'documentoService', 'videoYoutubeService', CadastroApController]);

function CadastroApController($scope, $state, $stateParams, apService, statusService, tagService, fileService, videoService, documentoService, videoYoutubeService) {

	$scope.loading(true);

	$scope.salvar = function() {
		$scope.loading(true);

		var documentos = $scope.manageIdentifiedItems($scope.status.documentos, true);
		if(documentos.length) {
			fileService
				.readFile(documentos, 'Documento')
				.then(function(docs) {
					$scope.ap.documentos = $scope.manageIdentifiedItems($scope.status.documentos.filter, false).concat(docs);
					save();
				})
				.catch(function() {
					$scope.alert('Erro ao processar os documentos!');
				});
		} else {
			save();
		}

		function save() {
			apService
				.save($scope.ap)
				.then(function(id) {
					var videos = $scope.manageIdentifiedItems($scope.status.videos, true);
					if(videos.length) {
						fileService
							.uploadVideos(videos, id)
							.then(function() {
								$scope.alert('Consulta Pública cadastrada com sucesso!', goRoot);
							}, function(error) {
								$scope.alert(error.message + '<br />Detalhe: ' + error.exception.message, goRoot);
							});

					} else {
						$scope.alert('Consulta Pública cadastrada com sucesso!', goRoot);
					}

					function goRoot() {
						$scope.$apply(function() {
							$scope.loading(false);
							$state.go('root');
						});
					}
				});
		}
	};

	$scope.removeFile = function(sourceArray, $index, file) {
		if(file.id) {
			$scope.confirm('Excluir arquivo', 'Este arquivo está sincronizado na nuvem. Você realmente deseja excluí-lo?', function() {
				var service = sourceArray === $scope.status.documentos ? documentoService : videoService;
				service
					.remove(file.id)
					.then(function() {
						sourceArray.splice($index, 1);
					})
					.catch(function(error) {
						$scope.alert(error.message);
					});
			});
		} else {
			sourceArray.splice($index, 1);
		}
	};

	$scope.addVideo = function() {
		videoYoutubeService
			.getData($scope.status.urlToAdd)
			.then(function(video) {
				$scope.status.urlToAdd = '';
				$scope.ap.videosYoutube.push(video.id);
				$scope.status.videosYoutube.push(video);
			}).catch(function() {
				$scope.status.erroVideoYoutube = true;
			});
	};

	$scope.removeVideo = function($index, $event) {
		$event.preventDefault();
		$scope.ap.videosYoutube.splice($index, 1);
		$scope.status.videosYoutube.splice($index, 1);
	};

	$scope.initListener = function(type) {
		angular.element('.input-' + type)
			.on('change', function(e) {
				var elm = e.target;
				if(elm.files.length) {
					$scope.$apply(function() {
						var dest = angular.element(elm).data('destination');
						$scope.status[dest].push(elm.files[0]);
					});
				}
			});
	};

	$scope.manageIdentifiedItems = function(array, removeIdentified) {
		if(removeIdentified)
			return array.filter(function(item) {
				return !(item.hasOwnProperty('id') && item.id);
			});
		return array.filter(function(item) {
			return item.hasOwnProperty('id') && item.id;
		});
	};

	$scope.status = {
		attachmentsOpen: $scope.getScreenWidth().factor >= $scope.ScreenWidth.MEDIUM.factor,
		videosOpen: $scope.getScreenWidth().factor >= $scope.ScreenWidth.MEDIUM.factor,
		videosYoutubeOpen: $scope.getScreenWidth().factor >= $scope.ScreenWidth.MEDIUM.factor,
		documentos: [],
		videos: [],
		videosYoutube: [],
		erroVideoYoutube: false
	};

	$scope.ap = {
		tags: [],
		documentos: [],
		videosYoutube: []
	};

	if($stateParams.apId) {
		apService
			.findOne($stateParams.apId)
			.then(function(data) {
				$scope.ap = data;
				if(!$scope.ap.videosYoutube)
					$scope.ap.videosYoutube = [];
				$scope.status.documentos = angular.copy($scope.ap.documentos);
				$scope.status.videos = angular.copy($scope.ap.videos);
				for(var i in $scope.ap.videosYoutube) {
					videoYoutubeService
						.getData($scope.ap.videosYoutube[i])
						.then(function(video) {
							$scope.status.videosYoutube.push(video);
						});
				}
				$scope.loading(false);
			});
	} else {
		$scope.loading(false);
	}

	statusService
		.findAll()
		.then($scope.assign('statuses'));

	tagService
		.findAll()
		.then($scope.assign('tags'));

}
