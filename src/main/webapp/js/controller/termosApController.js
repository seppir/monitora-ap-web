angular
	.module('app')
	.controller('TermosApController', ['$scope', '$stateParams', 'termosService', TermosApController]);

function TermosApController($scope, $stateParams, termosService) {

	$scope.loading(true);

	$scope.setValid = function(valid) {
		return function(termo) {
			return termosService.updateValidAp(
				termo.termoAudienciaPublicaPk.idAudienciaPublica,
				termo.termoAudienciaPublicaPk.termo.id,
				valid
			);
		}
	};

	$scope.updateDescription = function(termo) {
		return termosService.updateDescriptionAp(
			termo.termoAudienciaPublicaPk.idAudienciaPublica,
			termo.termoAudienciaPublicaPk.termo.id,
			termo.termoAudienciaPublicaPk.termo.descricao
		);
	};

	termosService
		.findByAp($stateParams.apId)
		.then(function(data) {
			$scope.termos = data;
			$scope.loading(false);
		});

}
