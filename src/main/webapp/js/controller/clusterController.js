angular
	.module('app')
	.controller('ClusterController', ['$scope', '$rootScope', '$state', '$stateParams', '$sce', 'd3', 'proposicaoService', ClusterController]);

function ClusterController($scope, $rootScope, $state, $stateParams, $sce, d3, proposicaoService) {

	$scope.loading(true);
	$scope.mostrarAlerta = false;

	$scope.cluster = {
		palavraChave: ''
	};
	$scope.$palavraChave = angular.element(document.getElementById('palavra_chave'));
	$scope.$zoomScale = angular.element(document.getElementById('zoom-scale'));

	$scope.resumoFiltro = function() {
		var filtros = $scope.Filtro;
		var ativos = [];
		for(var i in filtros) {
			var f = filtros[i];
			if(f.filterCondition()) {
				ativos.push(f.title);
			}
		}
		return ativos.length ? '(' + ativos + ')' : '';
	}

	$scope.valoresFiltros = function() {
		var filtros = $scope.Filtro;
		var resumo = '';
		for(var i in filtros) {
			var f = filtros[i];
			if(f.filterCondition()) {
				resumo += '<b>' + f.title + ': </b>' + f.getDisplayData() + '<br />';
			}
		}
		return $sce.trustAsHtml(resumo);
	}

	$scope.Filtro = {
		BAIRRO: {
			id: 1,
			property: 'bairro.id',
			title: 'Bairro',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.bairro && $scope.filtro.bairro.id;
			},
			getDisplayData: function() {
				return $scope.filtro.bairro.nomeApresentacao;
			}
		},
		TEMPO_DOMICILIO: {
			id: 2,
			property: 'tempoDomicilio.id',
			title: 'Tempo de Domicílio',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.tempoDomicilio && $scope.filtro.tempoDomicilio.id;
			},
			getDisplayData: function() {
				return $scope.filtro.tempoDomicilio.descricao;
			}
		},
		TIPO_IMOVEL: {
			id: 3,
			property: 'tipoImovel.id',
			title: 'Tipo de Imóvel',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.tipoImovel && $scope.filtro.tipoImovel.id;
			},
			getDisplayData: function() {
				return $scope.filtro.tipoImovel.descricao;
			}
		},
		CIDADE_NATAL: {
			id: 4,
			property: 'cidadeNatal.id',
			title: 'Cidade natal',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.cidadeNatal && $scope.filtro.cidadeNatal.id;
			},
			getDisplayData: function() {
				return $scope.filtro.cidadeNatal.nome;
			}
		},
		SEXO: {
			id: 5,
			property: 'sexo',
			title: 'Sexo',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.sexo;
			},
			getDisplayData: function() {
				var sexo = $scope.filtro.sexo;
				var s = $scope.Sexo;
				for(var i in s) {
					if(s[i].value == sexo) {
						return s[i].label;
					}
				}
				return sexo;
			}
		},
		ESCOLARIDADE: {
			id: 6,
			property: 'escolaridade.id',
			title: 'Escolaridade',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.escolaridade && $scope.filtro.escolaridade.id;
			},
			getDisplayData: function() {
				return $scope.filtro.escolaridade.descricao;
			}
		},
		IDADE: {
			id: 7,
			property: 'idade.id',
			title: 'Faixa Etária',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.idade && $scope.filtro.idade.id;
			},
			getDisplayData: function() {
				return $scope.filtro.idade.descricao;
			}
		},
		ESTADO_CIVIL: {
			id: 8,
			property: 'estadoCivil.id',
			title: 'Estado Civil',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.estadoCivil && $scope.filtro.estadoCivil.id;
			},
			getDisplayData: function() {
				return $scope.filtro.estadoCivil.descricao;
			}
		},
		RENDA: {
			id: 9,
			property: 'renda.id',
			title: 'Renda',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.renda && $scope.filtro.renda.id;
			},
			getDisplayData: function() {
				return $scope.filtro.renda.descricao;
			}
		},
		NUMERO_FILHOS: {
			id: 10,
			property: 'numFilhos',
			title: 'Número de Filhos',
			filterCondition: function() {
				return $scope.filtro.hasOwnProperty('numFilhos');
			},
			getDisplayData: function() {
				return $scope.filtro.numFilhos;
			}
		},
		NUMERO_DEPENDENTES_DIRETOS: {
			id: 11,
			property: 'numDependentesDiretos',
			title: 'Número de Dependentes Diretos',
			filterCondition: function() {
				return $scope.filtro.hasOwnProperty('numDependentesDiretos');
			},
			getDisplayData: function() {
				return $scope.filtro.numDependentesDiretos;
			}
		},
		DOMICILIO_ELEITORAL: {
			id: 12,
			property: 'domicilioEleitoral.id',
			title: 'Domicílio Eleitoral',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.domicilioEleitoral && $scope.filtro.domicilioEleitoral.id;
			},
			getDisplayData: function() {
				return $scope.filtro.domicilioEleitoral.zona;
			}
		},
		SERVIDOR_PUBLICO: {
			id: 13,
			property: 'servidorPublico',
			title: 'Servidor Público',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.servidorPublico;
			},
			getDisplayData: function() {
				var opcao = $scope.filtro.servidorPublico;
				var o = $scope.OpcoesBoolean;
				for(var i in o) {
					if(o[i].value == opcao) {
						return o[i].label;
					}
				}
				return opcao;
			}
		}
	};

	$scope.status = {
		firstOpen: true,
		secondOpen: false
	}

	if($stateParams.hasOwnProperty('filtro') && $stateParams.filtro) {
		var f = JSON.parse($stateParams.filtro);
		if(Object.keys(f).length) {
			$scope.filtro = f;
		}
	}

	$scope.aps = JSON.parse($stateParams.aps);
	$scope.apIds = [];
	var p = '';
	var ap;
	for(var i in $scope.aps) {
		ap = $scope.aps[i];
		$scope.apIds.push(ap.i);
		if(p.search(ap.p) === -1) {
			p += ap.p + ' ';
		}
	}

	$scope.search = function(keyword) {
		$scope.loading(true);
		$scope.cluster.palavraChave = keyword;
		$scope.$palavraChave.html(keyword ? '"' + keyword + '"' : '(Nenhuma)');
		d3.json(proposicaoService.getClusterUrl(keyword, 'cluster', $scope.filtro, $scope.apIds, null), function(error, graph) {
			if(graph) {
				if(graph.nodes.length == 1 && graph.nodes[0].name == 'Consulta Púbica') {
					$scope.mostrarAlerta = true;
				} else {
					$scope.drawGraph(graph);
				}
			}

			$scope.loading(false);
			$rootScope.$apply();
		});
	}

	$scope.listaTematicas = function() {
		$state.go('ap.data', {
			aps: JSON.stringify($scope.aps),
			palavraChave: JSON.stringify($scope.cluster.palavraChave),
			filtro: JSON.stringify($scope.filtro || {})
		});
	};

	const NODE_FONT_SIZE = 14;
	const ZOOM_STEP = 0.1;
	const ZOOM_MIN = 0.4;
	const ZOOM_MAX = 1.60;

	$scope.$mapa = angular.element(document.getElementById('mapa'));
	$scope.currentScale = 1;
	$scope.width = $scope.$mapa.width();
	$scope.height = 500;
	$scope.$zoom = d3.behavior.zoom();

	$scope.color = d3.scale.category20();

	$scope.force = d3.layout.force()
	    .charge(-500)
	    .linkDistance(100)
	    .size([$scope.width, $scope.height]);

	$scope.drawGraph = function(graph) {
		$scope.$zoom
			.on('zoom', null)
			.scale(1)
			.translate([0, 0]);
		$scope.currentScale = 1;
		$scope.$zoomScale.html('100%');
		var mapa = d3.select('#mapa');
		mapa.selectAll('svg').remove();
		var svg = mapa
			.append('svg')
		    .attr('width', $scope.width)
		    .attr('height', $scope.height)
		    .call($scope.$zoom
		    	.on('zoom', function() {
					d3.selectAll('#mapa text').attr('font-size', parseInt(NODE_FONT_SIZE / d3.event.scale) + 'px');
					svg.attr('transform', 'translate(' + d3.event.translate + ') scale(' + d3.event.scale + ')');
				})
			)
			.on('mousedown.zoom', null)
			.on('mousemove.zoom', null)
			.on('dblclick.zoom', null)
			.on('touchstart.zoom', null)
			.on('wheel.zoom', null)
			.on('mousewheel.zoom', null)
			.on('MozMousePixelScroll.zoom', null)
			.append('g');

		$scope.force
			.nodes(graph.nodes)
			.links(graph.links)
			.start();

  		var link = svg.selectAll('.link')
  			.data(graph.links)
  			.enter().append('line')
  			.attr('class', 'link')
  			.style('stroke-width', function(d) { return Math.sqrt(d.value); });

		var gnodes = svg.selectAll('g.gnode')
			.data(graph.nodes)
			.enter()
			.append('g')
			.classed('gnode', true);

		var node = gnodes.append('circle')
			.attr('class', 'node')
			.attr('r', function(d) { return d.r; })
			.style('fill', function(d) { return $scope.color(d.group); })
			.on('dblclick', $scope.dblclick)
			.call($scope.force.drag);

		var labels = gnodes
			.append('text')
			.attr('text-anchor', 'middle')
			.attr('dy', '0.3em')
			.attr('font-size', NODE_FONT_SIZE + 'px')
			.text(function(d) { return d.name; });

		$scope.force.on('tick', function() {
  			link.attr('x1', function(d) { return d.source.x; })
  				.attr('y1', function(d) { return d.source.y; })
  				.attr('x2', function(d) { return d.target.x; })
  				.attr('y2', function(d) { return d.target.y; });

			gnodes.attr('transform', function(d) {
    			return 'translate(' + [d.x, d.y] + ')';
    		});
 		});
	}

	$scope.zoom = function(way) {
		// http://bl.ocks.org/linssen/7352810
		var direction = 0,
			targetZoom = 1,
	        center = [$scope.width / 2, $scope.height / 2],
	        translate = $scope.$zoom.translate(),
	        translate0 = [],
	        l = [],
	        view = {
				x: translate[0],
				y: translate[1],
				k: $scope.currentScale
			};

		if(way == 'center') {
			targetZoom = 1;
		} else {
			direction = (way == 'in') ? 1 : -1;
			targetZoom = $scope.currentScale + ZOOM_STEP * direction;
		}

	    if (targetZoom < ZOOM_MIN || targetZoom > ZOOM_MAX)
	    	return false;

	    translate0 = [(center[0] - view.x) / view.k, (center[1] - view.y) / view.k];
	    view.k = targetZoom;
	    l = [translate0[0] * view.k + view.x, translate0[1] * view.k + view.y];

	    view.x += center[0] - l[0];
	    view.y += center[1] - l[1];

	    setZoom([view.x, view.y], view.k);

		function setZoom(translate, scale) {
			if(scale > ZOOM_MIN && scale < ZOOM_MAX) {
				$scope.currentScale = scale;
				$scope.$zoom
					.scale($scope.currentScale)
					.translate(translate);
				$scope.$zoomScale.html(parseInt($scope.currentScale * 100) + '%');
				$scope.$zoom.event(d3.select('#mapa svg g').transition(200));
			}
		}
	}

	$scope.dblclick = function(d) {
		$scope.loading(true);
		$scope.search(d.name);
	}

	$scope.search(p.trim());

}
