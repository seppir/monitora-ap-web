angular
	.module('app')
	.controller('CadastroProponenteController', ['$scope', '$filter','$modalInstance', '_', 'user', 'estadoService', 'cidadeService', 'bairroService',
	                                             'tempoDomicilioService', 'tipoImovelService', 'estadoCivilService', 'escolaridadeService', 'idadeService',
	                                             'rendaService', 'domicilioEleitoralService', 'proponenteService', 'facebookService', CadastroProponenteController]);

function CadastroProponenteController($scope, $filter, $modalInstance, _, user, estadoService, cidadeService, bairroService, tempoDomicilioService, tipoImovelService,
	estadoCivilService, escolaridadeService, idadeService, rendaService, domicilioEleitoralService, proponenteService, facebookService) {

	$scope.salvar = function() {
		if($scope.form.$valid) {
			proponenteService
				.save($scope.p)
				.then(function() {
					$modalInstance.close({
						username: $scope.p.email,
						password: atob($scope.p.autenticacao)
					});
				}).catch(function(error) {
					$scope.alert(error.message);
				});
		} else {
			$scope.alert('Formulário inválido');
		}
	}

	$scope.cancelar = function() {
		$modalInstance.dismiss('cancel');
	}

	$scope.listarCidadesResidencial = function() {
		if($scope.idEstadoResidencial) {
			cidadeService
				.findByState($scope.idEstadoResidencial)
				.then(function(data){
					$scope.cidadesResidencial = data;

					if($scope.dataCep.localidade){
						var cidades = $filter('startsWith')($scope.cidadesResidencial, $scope.dataCep.localidade, 'nome');
						if(cidades.length == 1){
							$scope.idCidadeResidencial = cidades[0].id;
							$scope.listarBairrosResidencial();
						}
					}
				});
		} else {
			$scope.cidadesResidencial = null;
		}
	}

	$scope.listarBairrosResidencial = function() {
		if($scope.idCidadeResidencial) {
			bairroService
				.findByCity($scope.idCidadeResidencial)
				.then(function(data){
					$scope.bairrosResidencial = data;

					if($scope.dataCep.bairro){
						var bairros = $filter('startsWith')($scope.bairrosResidencial, $scope.dataCep.bairro, 'nome');
						if(bairros.length == 1){
							$scope.p.bairro = bairros[0];
						}
					}
				});
		} else {
			$scope.bairrosResidencial = null;
		}
	}

	$scope.listarCidadesNaturalidade = function() {
		if($scope.idEstadoNaturalidade) {
			cidadeService
				.findByState($scope.idEstadoNaturalidade)
				.then($scope.assign('cidadesNaturalidade'));
		} else {
			$scope.cidadesNaturalidade = null;
		}
	}

	$scope.listarCidadesDomicilioEleitoral = function() {
		if($scope.idEstadoDomicilioEleitoral) {
			cidadeService
				.findByState($scope.idEstadoDomicilioEleitoral)
				.then($scope.assign('cidadesDomicilioEleitoral'));
		} else {
			$scope.cidadesDomicilioEleitoral = null;
		}
	}

	$scope.listarDomicilios = function() {
		if($scope.idCidadeDomicilioEleitoral) {
			domicilioEleitoralService
				.findByCity($scope.idCidadeDomicilioEleitoral)
				.then($scope.assign('domicilios'));
		} else {
			$scope.domicilios = null;
		}
	}

	$scope.p = {
		autenticacao: '',
		autenticacao2: ''
	};

	$scope.findByCEP = function(){
		if($scope.p.cep) {

			var cep = $scope.p.cep.replace('.', '').replace('-', '');

			cidadeService
				.findByCEP(cep)
				.then(function(data){
					$scope.dataCep = data;

					var filtro = { sigla: data.uf};
					var estados = $filter('filter')($scope.estados, filtro);
					if(estados.length == 1){
						$scope.idEstadoResidencial = estados[0].id;
						$scope.p.logradouro = $scope.dataCep.logradouro;
						$scope.listarCidadesResidencial();
					}
				});
		}
	}

	if(user) {
		if(user.birthday) {
			var today = new Date(),
				birthday = new Date(user.birthday),
				timeDiff = Math.abs(today.getTime() - birthday.getTime());

			user.age = Math.floor(timeDiff / (1000 * 3600 * 24 * 365));

			$scope.p.facebookId = user.id;
			$scope.p.nome = user.name;
			$scope.p.email = user.email;
		}

		if(user.gender) {
			_.each($scope.Sexo, function(sexo) {
				if(sexo.fbDescription === user.gender) {
					$scope.p.sexo = sexo.value;
				}
			});
		}

		if(user.family) {
			var family = user.family.data;
			if(user.family.paging.next) {
				loadFamily(user.family.paging.next);

				function loadFamily(next) {
					facebookService
						.navigate(next)
						.then(function(data) {
							family = family.concat(data.data);
							if(data.paging.next) {
								loadFamily(data.paging.next);
							} else {
								var sons = 0;
								_.each(family, function(member) {
									if(member.relationship === 'son')
										sons++;
								});
								$scope.p.numeroFilhos = sons;
							}
						});
				}
			}
		}
	}

	estadoService
		.findAll()
		.then(function(data) {
			$scope.estados = data;

			if(user) {
				if(user.location) {
					var cityName = user.location.name.replace(/,.*/, '');
					cidadeService
						.findByName(cityName)
						.then(function(cidade) {
							$scope.idEstadoResidencial = cidade.estado.id;
							$scope.idCidadeResidencial = cidade.id;
							$scope.listarCidadesResidencial();
							$scope.listarBairrosResidencial();
						});
				}

				if(user.hometown) {
					var hometownName = user.hometown.name.replace(/,.*/, '');
					cidadeService
						.findByName(hometownName)
						.then(function(cidade) {
							$scope.idEstadoNaturalidade = cidade.estado.id;
							$scope.p.cidadeNatal = {
								id: cidade.id
							};
							$scope.listarCidadesNaturalidade();
						});
				}
			}
		});

	tempoDomicilioService
		.findAll()
		.then($scope.assign('temposDomicilio'));

	tipoImovelService
		.findAll()
		.then($scope.assign('tiposImovel'));

	escolaridadeService
		.findAll()
		.then(function(data) {
			$scope.escolaridades = data;

			if(user && user.education) {
				var educations = user.education.sort(function(a, b) {
					switch(a.type) {
	                    case 'High School':
		                    return -1;

	                    case 'College':
	                    	return b.type === 'Graduate School' ? -1 : 1;

	                    case 'Graduate School':
	                    	return 1;

	                    default:
		                    return 0;
                    }
				});
				var education = educations[educations.length - 1];
				switch(education.type) {
                    case 'High School':
                    	$scope.p.escolaridade = {
                    		id: education.year ? $scope.Escolaridade.MED_COMPLETO : $scope.Escolaridade.MED_INCOMPLETO
                    	}
                    	break;

                    case 'College':
                    	$scope.p.escolaridade = {
	                		id: education.year ? $scope.Escolaridade.SUP_COMPLETO : $scope.Escolaridade.SUP_INCOMPLETO
	                	}
	                	break;

                    case 'Graduate School':
                    	$scope.p.escolaridade = {
	                    	id: education.year ? $scope.Escolaridade.POS_COMPLETO : $scope.Escolaridade.POS_INCOMPLETO
	                    }
                    	break;

                    default:
	                    break;
                }
			}
		});

	idadeService
		.findAll()
		.then(function(data) {
			$scope.idades = data;

			if(user && user.age) {
				for(var i in $scope.idades) {
					var idade = $scope.idades[i];
					if(((idade.minIdade && user.age >= idade.minIdade) || !idade.minIdade) && ((idade.maxIdade && user.age <= idade.maxIdade) || !idade.maxIdade)) {
						$scope.p.idade = {
							id: idade.id
						};
						return;
					}
				}
			}
		});

	estadoCivilService
		.findAll()
		.then(function(data) {
			$scope.estadosCivis = data;

			if(user && user.relationship_status) {
				_.each($scope.EstadoCivil, function(estadoCivil) {
					if(_.isArray(estadoCivil.fbDescription)) {
						_.each(estadoCivil.fbDescription, function(description) {
							if(description === user.relationship_status) {
								$scope.p.estadoCivil = {
									id: estadoCivil.id
								};
								return;
							}
						});
					} else if(estadoCivil.fbDescription === user.relationship_status) {
						$scope.p.estadoCivil = {
							id: estadoCivil.id
						};
						return;
					}
				});
			}
		});

	rendaService
		.findAll()
		.then($scope.assign('rendas'));

}
