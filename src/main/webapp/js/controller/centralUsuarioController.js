angular
	.module('app')
	.controller('CentralUsuarioController', ['$scope', '$state', '$filter', 'usuarioService', 'proponenteService', 'estadoService', 'cidadeService', 'bairroService',
	                                         'tempoDomicilioService', 'tipoImovelService', 'estadoCivilService', 'escolaridadeService', 'idadeService', 'rendaService',
	                                         'domicilioEleitoralService', 'facebookService', CentralUsuarioController]);

function CentralUsuarioController($scope, $state, $filter, usuarioService, proponenteService, estadoService, cidadeService, bairroService, tempoDomicilioService,
	tipoImovelService, estadoCivilService, escolaridadeService, idadeService, rendaService, domicilioEleitoralService, facebookService) {

	$scope.isCidadao = $scope.hasPermission($scope.Permissao.PROPONENTE);
	$scope.u = {};

	angular.extend($scope.u, $scope.currentUser);
	angular.element('input[type=password]').val('');

	$scope.salvar = function() {
		var s = $scope.isCidadao ? proponenteService : usuarioService;
		s
			.save($scope.u)
			.then(function() {
				alert('Salvo com sucesso');
				$state.go('root');
			})
			.catch(function(error) {
				alert(error.message);
			});
	}

	$scope.cancelar = function() {
		$state.go('root');
	}

	$scope.vincularFacebook = function() {
		facebookService
			.authenticate()
			.then(function(fbUser) {
				usuarioService
					.findByFacebookId(fbUser.id)
					.then(function(user) {
						$scope.alert('Esta conta Facebook já está vinculada a um usuário!');
					})
					.catch(function() {
						usuarioService
							.bindFacebook(fbUser.id)
							.then(function() {
								$scope.alert('Facebook vinculado com sucesso!');
								$scope.u.autenticadoFacebook = true;
							})
							.catch(function(error) {
								$scope.alert(error.message);
							});
					});
			})
			.catch(function() {
				$scope.loading(false);
			});
	}

	if($scope.isCidadao) {
		$scope.listarCidadesResidencial = function() {
			if($scope.idEstadoResidencial) {
				cidadeService
					.findByState($scope.idEstadoResidencial)
					.then( function(data){
						$scope.cidadesResidencial = data;

						if($scope.dataCep.localidade){
							var cidades = $filter('startsWith')($scope.cidadesResidencial, $scope.dataCep.localidade, 'nome');
							if(cidades.length == 1){
								$scope.idCidadeResidencial = cidades[0].id;
								$scope.listarBairrosResidencial();
							}
						}
					});
			} else {
				$scope.cidadesResidencial = null;
			}
		}

		$scope.listarBairrosResidencial = function() {
			if($scope.idCidadeResidencial) {
				bairroService
					.findByCity($scope.idCidadeResidencial)
					.then(function(data){
						$scope.bairrosResidencial = data;

						if($scope.dataCep.bairro){
							var bairros = $filter('startsWith')($scope.bairrosResidencial, $scope.dataCep.bairro, 'nome');
							if(bairros.length == 1){
								$scope.p.bairro = bairros[0];
							}
						}
					});
			} else {
				$scope.bairrosResidencial = null;
			}
		}

		$scope.listarCidadesNaturalidade = function() {
			if($scope.idEstadoNaturalidade) {
				cidadeService
					.findByState($scope.idEstadoNaturalidade)
					.then($scope.assign('cidadesNaturalidade'));
			} else {
				$scope.cidadesNaturalidade = null;
			}
		}

		$scope.listarCidadesDomicilioEleitoral = function() {
			if($scope.idEstadoDomicilioEleitoral) {
				cidadeService
					.findByState($scope.idEstadoDomicilioEleitoral)
					.then($scope.assign('cidadesDomicilioEleitoral'));
			} else {
				$scope.cidadesDomicilioEleitoral = null;
			}
		}

		$scope.listarDomicilios = function() {
			if($scope.idCidadeDomicilioEleitoral) {
				domicilioEleitoralService
					.findByCity($scope.idCidadeDomicilioEleitoral)
					.then($scope.assign('domicilios'));
			} else {
				$scope.domicilios = null;
			}
		}

		$scope.findByCEP = function(){
			if($scope.u.cep) {

				var cep = $scope.u.cep.replace('.', "").replace('-', "");

				cidadeService
					.findByCEP(cep)
					.then(function(data){
						$scope.dataCep = data;

						var filtro = { sigla: data.uf};
						var estados = $filter('filter')($scope.estados, filtro);
						if(estados.length == 1){
							$scope.idEstadoResidencial = estados[0].id;
							$scope.u.logradouro = $scope.dataCep.logradouro;
							$scope.listarCidadesResidencial();
						}
					});
			}
		}

		estadoService
			.findAll()
			.then($scope.assign('estados'));

		bairroService
			.findOneWithAllData($scope.u.bairro.id)
			.then(function(bairro) {
				$scope.idEstadoResidencial = bairro.cidade.estado.id;
				cidadeService
					.findByState($scope.idEstadoResidencial)
					.then(function(cidades) {
						$scope.cidadesResidencial = cidades;
						$scope.idCidadeResidencial = bairro.cidade.id;
						bairroService
							.findByCity($scope.idCidadeResidencial)
							.then($scope.assign('bairrosResidencial'))
					});
			});

		estadoService
			.findByCity($scope.u.cidadeNatal.id)
			.then(function(estado) {
				$scope.idEstadoNaturalidade = estado.id;
				cidadeService
					.findByState(estado.id)
					.then($scope.assign('cidadesNaturalidade'));
			});

		estadoService
			.findByCity($scope.u.domicilioEleitoral.idCidade)
			.then(function(estado) {
				$scope.idEstadoDomicilioEleitoral = estado.id;
				$scope.idCidadeDomicilioEleitoral = $scope.u.domicilioEleitoral.idCidade;
				cidadeService
					.findByState(estado.id)
					.then(function(cidades) {
						$scope.cidadesDomicilioEleitoral = cidades;
						domicilioEleitoralService
							.findByCity($scope.u.domicilioEleitoral.idCidade)
							.then($scope.assign('domicilios'))
					})
			});

		tempoDomicilioService
			.findAll()
			.then($scope.assign('temposDomicilio'));

		tipoImovelService
			.findAll()
			.then($scope.assign('tiposImovel'));

		escolaridadeService
			.findAll()
			.then($scope.assign('escolaridades'));

		idadeService
			.findAll()
			.then($scope.assign('idades'));

		estadoCivilService
			.findAll()
			.then($scope.assign('estadosCivis'));

		rendaService
			.findAll()
			.then($scope.assign('rendas'));
	}

}
