angular
	.module('app')
	.controller('LoginController', ['$scope', '$rootScope', '$http', '$modal', '$state', 'facebookService', 'usuarioService', LoginController]);

function LoginController($scope, $rootScope, $http, $modal, $state, facebookService, usuarioService) {

	$scope.credentials = {};

	$scope.login = function() {
		$scope.loading(true);
		$scope.authenticate($scope.credentials);
		$scope.loading(false);
	};

	$scope.loginFacebook = function() {
		$scope.loading(true);
		facebookService
			.authenticate()
			.then(function(fbUser) {
				usuarioService
					.findByFacebookId(fbUser.id)
					.then(function(user) {
						var facebookCredentials = {
							username: user.email,
							password: fbUser.id
						};
						$scope.authenticate(facebookCredentials);
						$scope.loading(false);
					})
					.catch(function() {
						$scope.loading(false);
						$scope.ask('Qual tipo de usuário você deseja cadastrar?', {
							'Gestor Público': {
								callback: function() {
									$scope.novoGestorPublico(fbUser);
								}
							},
							'Cidadão': {
								callback: function() {
									$scope.novoUsuario(fbUser);
								}
							}
						});
					});
			})
			.catch(function() {
				$scope.loading(false);
			});
	};

	$scope.novoUsuario = function(userTemplate) {
		$modal.open({
			templateUrl: './views/partials/templates/cadastro_proponente.tpl.html',
			controller: 'CadastroProponenteController',
			size: 'lg',
			resolve: {
				user: function() {
					return userTemplate;
				}
			}
		}).result.then($scope.loginAfterRegister);
	};

	$scope.novoGestorPublico = function(userTemplate) {
		$modal.open({
			templateUrl: './views/partials/templates/cadastro_gestor_publico.tpl.html',
			controller: 'CadastroGestorPublicoController',
			resolve: {
				user: function() {
					return userTemplate;
				}
			}
		}).result.then($scope.loginAfterRegister);
	};

	$scope.loginAfterRegister = function(credentials) {
		$scope.credentials.username = credentials.username;
		$scope.credentials.password = credentials.password;
		$scope.login();
	};

	$scope.authenticate = function(credentials) {
		var headers = credentials ? {
			Authorization: 'Basic ' + btoa(credentials.username + ':' + credentials.password)
		} : {};

		$http
			.get('usuarios/authenticate', {
				headers: headers
			}).success(function(data) {
				if(data && typeof data !== 'string') {
					$rootScope.currentUser = data;
					angular.merge($http.defaults.headers.post, headers);
					angular.merge($http.defaults.headers.put, headers);
					$state.reload();
				} else {
					$rootScope.currentUser = false;
					angular.merge($http.defaults.headers.post, {Authorization: null});
					angular.merge($http.defaults.headers.put, {Authorization: null});
				}
				verifyLoginError();
			}).error(function() {
				$rootScope.currentUser = false;
				verifyLoginError();
			});

		function verifyLoginError() {
			if($rootScope.currentUser) {
				$scope.error = false;
			} else {
				$scope.error = true;
			}
		}
	}


}
