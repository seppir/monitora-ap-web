angular
	.module('app')
	.controller('ListaPerfilController', ['$scope', '$filter', '$state', '$stateParams', '$timeout', 'apService', 'proposicaoService', 'estadoService', 'cidadeService',
	                                      'bairroService', 'tempoDomicilioService', 'tipoImovelService', 'estadoCivilService', 'escolaridadeService',
	                                      'idadeService', 'rendaService', 'domicilioEleitoralService', 'chartService', ListaPerfilController]);

function ListaPerfilController($scope, $filter, $state, $stateParams, $timeout, apService, proposicaoService, estadoService, cidadeService, bairroService, tempoDomicilioService,
	tipoImovelService, estadoCivilService, escolaridadeService, idadeService, rendaService, domicilioEleitoralService, chartService) {

	$scope.loading(true);

	$scope.podeMostrarCluster = function() {
		return typeof $scope.ids == 'object' && $scope.ids.length == $scope.aps.length;
	}

	$scope.mostrarCluster = function() {
		var aps = [];
		var ap;
		removeNullProperties($scope.filtro.proponente);

		$scope.selectedKeywords = $scope.selectedKeywords.map(function(item) {
			return item.text;
		});

		for(var i in $scope.aps) {
			ap = $scope.aps[i];
			aps.push({
				i: ap.id,
				n: ap.nome,
				p: $scope.selectedKeywords
			});
		}
		$state.go('ap.cluster', {
			aps: JSON.stringify(aps),
			filtro: JSON.stringify($scope.filtro.proponente)
		});
	}

	$scope.currentFilterUpdateOperation = false;

	$scope.updateCharts = function(straight) {
		if(straight) {
			$timeout.cancel($scope.currentFilterUpdateOperation);
			$scope.currentFilterUpdateOperation = false;
			fnUpdate();
		} else {
			if($scope.currentFilterUpdateOperation) {
				$timeout.cancel($scope.currentFilterUpdateOperation);
				$scope.currentFilterUpdateOperation = false;
			}
			$scope.currentFilterUpdateOperation = $timeout(fnUpdate, 1000);
		}

		function fnUpdate() {
			$scope.currentFilterUpdateOperation = false;
			if($scope.charts.show) {
				for(var f in $scope.Filtro) {
					var filter = $scope.Filtro[f];
					updateChart(filter);
				}
			}
			buildChartPairs();
		}
	}

	function updateChart(filter) {
		var chart = $scope.charts[filter.id];
		var show = !filter.filterCondition();
		chart.show = show;
		if(show) {
			var proponentes = [];
			for(var i in $scope.proposicoes) {
				proponentes.push($scope.proposicoes[i].proponente);
			}

			removeNullProperties($scope.filtro.proponente);
			proponentes = $filter('filter')(proponentes, $scope.filtro.proponente);
			chart.config = chartService.generateChart(filter.property, proponentes, filter.chartTitle, filter.getDisplayData);
		}
	}

	function removeNullProperties(obj) {
		for(var i in obj)
		  if(obj[i] === null || obj[i] === undefined)
		    delete obj[i];
	}

	$scope.unbindFilter = function(filter, elementToClean) {
		var $elem = false;
		if(elementToClean) {
			$elem = angular.element(document.getElementById(elementToClean));
		}
		if($scope.filtro && !filter.filterCondition()) {
			if($elem) {
				$elem.val(null);
			}
			var prop = filter.property;
			if(prop.indexOf('.') != -1) {
				delete $scope.filtro.proponente[prop.split('.')[0]];
			} else {
				delete $scope.filtro.proponente[prop];
			}
		}
		$scope.updateCharts();
	}

	$scope.listarCidadesResidencial = function() {
		if($scope.data.idEstadoResidencial) {
			cidadeService
				.findByState($scope.data.idEstadoResidencial)
				.then($scope.assign('cidadesResidencial'));
		} else {
			$scope.cidadesResidencial = null;
		}
	}

	$scope.listarBairrosResidencial = function() {
		if($scope.data.idCidadeResidencial) {
			bairroService
				.findByCity($scope.data.idCidadeResidencial)
				.then($scope.assign('bairrosResidencial'));
		} else {
			$scope.bairrosResidencial = null;
		}
	}

	$scope.listarCidadesNaturalidade = function() {
		if($scope.data.idEstadoNaturalidade) {
			cidadeService
				.findByState($scope.data.idEstadoNaturalidade)
				.then($scope.assign('cidadesNaturalidade'));
		} else {
			$scope.cidadesNaturalidade = null;
		}
	}

	$scope.listarCidadesDomicilioEleitoral = function() {
		if($scope.data.idEstadoDomicilioEleitoral) {
			cidadeService
				.findByState($scope.data.idEstadoDomicilioEleitoral)
				.then($scope.assign('cidadesDomicilioEleitoral'));
		} else {
			$scope.cidadesDomicilioEleitoral = null;
		}
	}

	$scope.listarDomicilios = function() {
		if($scope.data.idCidadeDomicilioEleitoral) {
			domicilioEleitoralService
				.findByCity($scope.data.idCidadeDomicilioEleitoral)
				.then($scope.assign('domicilios'));
		} else {
			$scope.domicilios = null;
		}
	}

	$scope.resumoFiltro = function() {
		var filtros = $scope.Filtro;
		var ativos = [];
		for(var i in filtros) {
			var f = filtros[i];
			if(f.filterCondition()) {
				ativos.push(f.chartTitle);
			}
		}
		return ativos.length ? '(' + ativos + ')' : '';
	}

	$scope.Filtro = {
		BAIRRO: {
			id: 1,
			property: 'bairro.id',
			chartTitle: 'Bairro',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.proponente.bairro && $scope.filtro.proponente.bairro.id;
			},
			getDisplayData: function(proponente) {
				return proponente.bairro.nome;
			}
		},
		TEMPO_DOMICILIO: {
			id: 2,
			property: 'tempoDomicilio.id',
			chartTitle: 'Tempo de Domicílio',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.proponente.tempoDomicilio && $scope.filtro.proponente.tempoDomicilio.id;
			},
			getDisplayData: function(proponente) {
				return proponente.tempoDomicilio.descricao;
			}
		},
		TIPO_IMOVEL: {
			id: 3,
			property: 'tipoImovel.id',
			chartTitle: 'Tipo de Imóvel',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.proponente.tipoImovel && $scope.filtro.proponente.tipoImovel.id;
			},
			getDisplayData: function(proponente) {
				return proponente.tipoImovel.descricao;
			}
		},
		CIDADE_NATAL: {
			id: 4,
			property: 'cidadeNatal.id',
			chartTitle: 'Cidade natal',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.proponente.cidadeNatal && $scope.filtro.proponente.cidadeNatal.id;
			},
			getDisplayData: function(proponente) {
				return proponente.cidadeNatal.nome;
			}
		},
		SEXO: {
			id: 5,
			property: 'sexo',
			chartTitle: 'Sexo',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.proponente.sexo;
			},
			getDisplayData: function(proponente) {
				var sexo = proponente.sexo;
				var s = $scope.Sexo;
				for(var i in s) {
					if(s[i].value == sexo) {
						return s[i].label;
					}
				}
				return sexo;
			}
		},
		ESCOLARIDADE: {
			id: 6,
			property: 'escolaridade.id',
			chartTitle: 'Escolaridade',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.proponente.escolaridade && $scope.filtro.proponente.escolaridade.id;
			},
			getDisplayData: function(proponente) {
				return proponente.escolaridade.descricao;
			}
		},
		IDADE: {
			id: 7,
			property: 'idade.id',
			chartTitle: 'Faixa Etária',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.proponente.idade && $scope.filtro.proponente.idade.id;
			},
			getDisplayData: function(proponente) {
				return proponente.idade.descricao;
			}
		},
		ESTADO_CIVIL: {
			id: 8,
			property: 'estadoCivil.id',
			chartTitle: 'Estado Civil',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.proponente.estadoCivil && $scope.filtro.proponente.estadoCivil.id;
			},
			getDisplayData: function(proponente) {
				return proponente.estadoCivil.descricao;
			}
		},
		RENDA: {
			id: 9,
			property: 'renda.id',
			chartTitle: 'Renda',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.proponente.renda && $scope.filtro.proponente.renda.id;
			},
			getDisplayData: function(proponente) {
				return proponente.renda.descricao;
			}
		},
		NUMERO_FILHOS: {
			id: 10,
			property: 'numFilhos',
			chartTitle: 'Número de Filhos',
			filterCondition: function() {
				return $scope.data.filtrarFilhos;
			},
			getDisplayData: function() {}
		},
		NUMERO_DEPENDENTES_DIRETOS: {
			id: 11,
			property: 'numDependentesDiretos',
			chartTitle: 'Número de Dependentes Diretos',
			filterCondition: function() {
				return $scope.data.filtrarDependentesDiretos;
			},
			getDisplayData: function() {}
		},
		DOMICILIO_ELEITORAL: {
			id: 12,
			property: 'domicilioEleitoral.id',
			chartTitle: 'Domicílio Eleitoral',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.proponente.domicilioEleitoral && $scope.filtro.proponente.domicilioEleitoral.id;
			},
			getDisplayData: function(proponente) {
				return proponente.domicilioEleitoral.zona;
			}
		},
		SERVIDOR_PUBLICO: {
			id: 13,
			property: 'servidorPublico',
			chartTitle: 'Servidor Público',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.proponente.servidorPublico;
			},
			getDisplayData: function(proponente) {
				var opcao = proponente.servidorPublico;
				var o = $scope.OpcoesBoolean;
				for(var i in o) {
					if(o[i].value == opcao) {
						return o[i].label;
					}
				}
				return opcao;
			}
		},
		POSSUI_CPF: {
			id: 14,
			property: 'possuiCPF',
			chartTitle: 'Possui CPF',
			filterCondition: function() {
				return $scope.filtro && $scope.filtro.proponente.possuiCPF;
			},
			getDisplayData: function(proponente) {
				var opcao = proponente.possuiCPF;
				var o = $scope.OpcoesBoolean;
				for(var i in o) {
					if(o[i].value == opcao) {
						return o[i].label;
					}
				}
				return opcao;
			}
		}
	};

	$scope.status = {};
	$scope.data = {};
	$scope.filtro = {
		proponente: {}
	};
	$scope.aps = [];
	$scope.proposicoes = [];

	estadoService
		.findAll()
		.then($scope.assign('estados'));

	tempoDomicilioService
		.findAll()
		.then($scope.assign('temposDomicilio'));

	tipoImovelService
		.findAll()
		.then($scope.assign('tiposImovel'));

	escolaridadeService
		.findAll()
		.then($scope.assign('escolaridades'));

	idadeService
		.findAll()
		.then($scope.assign('idades'));

	estadoCivilService
		.findAll()
		.then($scope.assign('estadosCivis'));

	rendaService
		.findAll()
		.then($scope.assign('rendas'));

	var totalAps;
	var loadedAps = 0;
	$scope.ids = $stateParams.ids;
	if(typeof $scope.ids === 'string') {
		$scope.ids = [$scope.ids];
	}
	totalAps = $scope.ids.length;
	for(var i in $scope.ids) {
		var id = $scope.ids[i];
		apService
			.findOne(id)
			.then(function(ap) {
				$scope.aps.push(ap);
				$scope.aps.sort(function(a, b) {
					return a.id - b.id;
				});
				proposicaoService
					.findByAp(ap.id)
					.then(function(props) {
						$scope.proposicoes = $scope.proposicoes.concat(props);
						loadedAps++;
						initChart();
					});
			});
	}

	$scope.selectedKeywords=[];
	$scope.keywords = null;

	$scope.searchKeywords = function() {
		d3.json(apService.getKeywordsUrl($stateParams.ids), function(error, data) {
			if(data) {
				$scope.keywords = data.map(function(item) {
					return {text: item};
				});
			} else {
				$scope.keywords = [];
			}
		});
	};

	$scope.searchKeywords();

	$scope.status.firstOpen = true;
	$scope.loading(false);
	$scope.$watch('charts.show', $scope.updateCharts);

	function initChart() {
		if(loadedAps === totalAps) {
			$scope.charts = {
				show: false
			};
			for(var f in $scope.Filtro) {
				var filtro = $scope.Filtro[f];
				$scope.charts[filtro.id] = {
					config: null,
					show: false
				};
			}
			buildChartPairs();
		}
	}

	function buildChartPairs() {
		$scope.chartsInPairs = [];
		var i = 0;
		var chart;
		for(var id in $scope.charts) {
			chart = $scope.charts[id];
				if(chart.show) {
				if(!$scope.chartsInPairs[i]) {
					$scope.chartsInPairs[i] = [chart];
				} else {
					$scope.chartsInPairs[i++].push(chart);
				}
			}
		}
	}

}
