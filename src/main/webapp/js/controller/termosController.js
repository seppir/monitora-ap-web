angular
	.module('app')
	.controller('TermosController', ['$scope', 'termosService', TermosController]);

function TermosController($scope, termosService) {

	$scope.loading(true);

	$scope.setValid = function(valid) {
		return function(termo) {
			return termosService.updateValid(termo.id, valid);
		}
	};

	$scope.updateDescription = function(termo) {
		return termosService.updateDescription(termo.id, termo.descricao);
	};

	termosService
		.findAll()
		.then(function(data) {
			$scope.termos = data;
			$scope.loading(false);
		});

}
