angular
	.module('app')
	.controller('ImportaUsuariosController', ['$scope', 'escolaridadeService', 'tipoImovelService', 'proponenteService', 'fileService', '$window', ImportaUsuariosController]);

function ImportaUsuariosController($scope, escolaridadeService, tipoImovelService, proponenteService, fileService, $window) {

	$scope.saveFile = function() {
		$scope.loading(true);
		fileService
			.readFile(document.getElementById('inputFiles').files[0])
			.then(function(file) {
				var doc = {
					'@type': 'Documento',
					nome: file.name,
					conteudo: file.content,
					tamanho: file.size
				};
				proponenteService
					.saveFromExcel(doc)
					.then(function() {
						$scope.loading(false);
						$scope.alert('Usuários importados com sucesso!', function() {
							$state.go('root');
						});
					}).catch(function(error) {
						$scope.loading(false);
						$scope.alert(error.message, function() {
							if(error.urlDownloadFile)
								$window.open(error.urlDownloadFile, '_blank');
						});
					});
			})
			.catch(function() {
				$scope.loading(false);
				$scope.alert('Um arquivo deve ser selecionado!');
			});
	}

	tipoImovelService
		.findAll()
		.then($scope.assign('tiposImovel'));

	escolaridadeService
		.findAll()
		.then($scope.assign('escolaridades'));

}
