angular
	.module('app')
	.controller('ImportaProposicoesController', ['$scope', '$state', 'proposicaoService', 'proponenteService', 'fileService', '$window', ImportaProposicoesController]);

function ImportaProposicoesController($scope, $state, proposicaoService, proponenteService, fileService, $window) {

	$scope.loading(true);

	$scope.saveFile = function() {
		$scope.loading(true);
		fileService
			.readFile(document.getElementById('inputFiles').files[0], 'Documento')
			.then(function(file) {
				proposicaoService
					.saveFromExcel($scope.idProponente, $scope.dataCadastro, file)
					.then(function() {
						$scope.loading(false);
						$scope.alert('Proposições importadas com sucesso!', function() {
							$state.go('root');
						});
					}).catch(function(error) {
						$scope.loading(false);
						$scope.alert(error.message, function() {
							if(error.urlDownloadFile)
								$window.open(error.urlDownloadFile, '_blank');
						});
					});
			})
			.catch(function() {
				$scope.loading(false);
				$scope.alert('Um arquivo deve ser selecionado!');
			});
	};

	proponenteService
		.findAll()
		.then(function(data) {
			$scope.proponentes = data;
			$scope.loading(false);
		});

}
