angular
	.module('app')
	.controller('HomeController', ['$scope', '$sce', '$state', 'apService', 'proposicaoService', 'statusService', 'tematicaService', HomeController]);

function HomeController($scope, $sce, $state, apService, proposicaoService, statusService, tematicaService) {

	$scope.filterDate = {
			dataInicio: new Date().setHours(0,0,0,0),
			dataEncerramento: null
		};
	$scope.loading(true);

	$scope.myInterval = 3000;
	$scope.noWrapSlides = false;
	var slides = $scope.slides = [{image:'./img/home_cidadao_img1.jpg' ,text:' '},
	                              {image:'./img/home_cidadao_img2.jpg' ,text:' '}];

	$scope.getDayClass = function(date, mode) {
		if (mode === 'day') {
			var dayToCheck = new Date(date).setHours(0,0,0,0);

			for (var i = 0; i < $scope.aps.length; i++) {
				if (dayToCheck === $scope.aps[i].dataInicio) {
					return 'full';
				}
			}
		}
		return '';
	};

	$scope.filtro = {
		// Em andamento
		status: {id: 2}
	};

	apService
		.findAllWithCreator()
		.then(function(data) {
			$scope.aps = data;
			$scope.loading(false);
		});

	var url_videos = 'http://www.youtube.com/embed/';
	var url_tumbs = 'http://img.youtube.com/vi/';

	$scope.videos = [{
						url_video: $sce.trustAsResourceUrl(url_videos+'0QtQqZ6Q5-o'),
						url_tumb: url_tumbs+'0QtQqZ6Q5-o/0.jpg'},
	                 {
						url_video: $sce.trustAsResourceUrl(url_videos+'DXPFPhpnAvo') ,
						url_tumb: url_tumbs+'DXPFPhpnAvo/0.jpg'},
	                 {
						url_video: $sce.trustAsResourceUrl(url_videos+'UNHCAYx7_YY') ,
						url_tumb: url_tumbs+'UNHCAYx7_YY/0.jpg'},
	                 {
						url_video: $sce.trustAsResourceUrl(url_videos+'-hoo_dIOP8k') ,
						url_tumb: url_tumbs+'-hoo_dIOP8k/0.jpg'},
	                 {
						url_video: $sce.trustAsResourceUrl(url_videos+'TVX8XhiR1UY') ,
						url_tumb: url_tumbs+'TVX8XhiR1UY/0.jpg'
					}];

	$scope.current_video = {
								url_video: $sce.trustAsResourceUrl(url_videos+'U8LM4C1l70U') ,
								url_tumb: url_tumbs+'U8LM4C1l70U/0.jpg'
							};

	$scope.selecionaVideo = function(data){
		var id = $scope.videos.indexOf(data)
		var video_aux = $scope.videos.splice(id, 1);
		$scope.videos.push($scope.current_video);
		$scope.current_video = video_aux[0];
	};

	$scope.apSelecionada = false;

}
