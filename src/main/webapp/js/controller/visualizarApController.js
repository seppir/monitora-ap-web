angular
	.module('app')
	.controller('VisualizarApController', ['$scope', '$modal', '$filter', '$state', '$stateParams', 'apService', 'proposicaoService', 'tematicaService', VisualizarApController]);

function VisualizarApController($scope, $modal, $filter, $state, $stateParams, apService, proposicaoService, tematicaService) {

	$scope.loading(true);
	$scope.apSelecionada = false;

	$scope.novaProposicao = function() {
		$modal.open({
			templateUrl: './views/partials/templates/cadastro_proposicao.tpl.html',
			controller: 'CadastroProposicaoController',
			size: 'lg',
			resolve: {
				apId: function() {
					return $scope.apSelecionada.id;
				}
			}
		}).result.then(carregaProposicoesAp);
	};

	tematicaService
		.findAll()
		.then($scope.assign('tematicas'));

	apService
		.findOneWithCreator($stateParams.apId)
		.then(function(data) {
			$scope.apSelecionada = data;
			carregaProposicoesAp();
		});

	function carregaProposicoesAp() {
		proposicaoService
			.findByUserAndAp($scope.currentUser.id, $scope.apSelecionada.id)
			.then($scope.assign('proposicoes'));
		$scope.loading(false);
	}

}
