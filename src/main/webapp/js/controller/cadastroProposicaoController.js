angular
	.module('app')
	.controller('CadastroProposicaoController', ['$scope','$modalInstance', 'apId', '$filter', 'proposicaoService', 'tematicaService', CadastroProposicaoController]);

function CadastroProposicaoController($scope, $modalInstance, apId, $filter, proposicaoService, tematicaService) {

	$scope.loading(true);

	$scope.proposicao = {
		idAudienciaPublica: apId,
		sugestaoTematicas: []
	};

	$scope.salvarProposicao = function() {
		proposicaoService
			.save($scope.proposicao)
			.then(function() {
				$scope.proposicao = false;
				$modalInstance.close();
			}).catch(function(error) {
				alert(error.message);
			});
	};

	$scope.cancelar = function() {
		$scope.proposicao = false;
		$modalInstance.dismiss('cancel');
	};

	tematicaService
		.findAll()
		.then(function(data) {
			$scope.tematicas = data;
			$scope.loading(false);
		});

}
