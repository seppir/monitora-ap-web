angular
	.module('app', ['ui.router', 'ngAnimate', 'highcharts-ng', 'ngMask', 'ui.bootstrap', 'ngTagsInput', 'reCAPTCHA'])
	.config(['$stateProvider', '$urlRouterProvider', 'tagsInputConfigProvider', '$httpProvider', 'reCAPTCHAProvider',
	         function($stateProvider, $urlRouterProvider, tagsInputConfigProvider, $httpProvider, reCAPTCHAProvider) {

		$urlRouterProvider.otherwise('/');

		$stateProvider
			.state('root', {
				url: '/',
				templateUrl: './views/partials/root.html'
			})
			.state('error', {
				url: '/erro',
				templateUrl: './views/partials/erro.html'
			})
			.state('home', {
				url: '/home',
				templateUrl: './views/partials/home_logado.html',
				params: {
					permissions: Permissao.PROPONENTE
				}
			})
			.state('me', {
				url: '/eu',
				templateUrl: './views/partials/central_usuario.html'
			})
			.state('ap', {
				url: '/ap',
				template: '<div ui-view></div>',
				params: {
					permissions: Permissao.POLITICO
				}
			})
			.state('ap.details', {
				url: '/detalhes/{apId}',
				templateUrl: './views/partials/lista_proposicoes.html',
				params: {
					permissions: Permissao.PROPONENTE
				}
			})
			.state('ap.list', {
				url: '/listar',
				templateUrl: './views/partials/lista_ap.html'
			})
			.state('ap.profiles', {
				url: '/perfil?ids',
				templateUrl: './views/partials/lista_perfil.html',
				controller: 'ListaPerfilController'
			})
			.state('ap.cluster', {
				url: '/cluster?aps&filtro',
				templateUrl: './views/partials/cluster.html',
				controller: 'ClusterController'
			})
			.state('ap.data', {
				url: '/tematicas?aps&palavraChave&filtro',
				templateUrl: './views/partials/lista_tematicas.html',
				controller: 'ListaTematicasController'
			})
			.state('ap.register', {
				url: '/cadastro/{apId}',
				templateUrl: './views/partials/cadastro_ap.html',
				controller: 'CadastroApController'
			})
			.state('ap.dictionary', {
				url: '/termos/{apId}',
				templateUrl: './views/partials/termos_ap.html',
				controller: 'TermosApController'
			})
			.state('adm', {
				url: '/adm',
				template: '<div ui-view></div>',
				params: {
					permissions: Permissao.ADMIN
				}
			})
			.state('adm.import', {
				url: '/importar',
				template: '<div ui-view></div>'
			})
			.state('adm.import.propositions', {
				url: '/proposicoes',
				templateUrl: './views/partials/importa_proposicoes.html'
			})
			.state('adm.import.users', {
				url: '/usuarios',
				templateUrl: './views/partials/importa_usuarios.html'
			})
			.state('adm.dictionary', {
				url: '/termos',
				templateUrl: './views/partials/termos.html',
				controller: 'TermosController'
			});

		tagsInputConfigProvider
			.setDefaults('tagsInput', {
				placeholder: 'Adicione uma tag...',
				replaceSpacesWithDashes: false,
				addOnComma: false,
				addOnBlur: false
			})
			.setDefaults('autoComplete', {
				debounceDelay: 500,
				loadOnDownArrow: true,
				selectFirstMatch: false
			});

		delete $httpProvider.defaults.headers.put['Content-Type'];

		reCAPTCHAProvider.setPublicKey('6Leu_RgTAAAAAHBbw5nMHdcW_p2Ve6rJ_ByQrT40');

	}])
	.run(['$rootScope', '$window', '$state', '$q', '$filter', 'sistemaService', 'bootbox', '_',
	      function($rootScope, $window, $state, $q, $filter, sistemaService, bootbox, _) {

		$rootScope.ScreenWidth = ScreenWidth;
		$rootScope.Permissao = Permissao;
		$rootScope.Sexo = Sexo;
		$rootScope.OpcoesBoolean = OpcoesBoolean;
		$rootScope.Escolaridade = Escolaridade;
		$rootScope.EstadoCivil = EstadoCivil;

		$rootScope.getComplexValue = getComplexValue;
		$rootScope.setComplexValue = setComplexValue;
		$rootScope.getComplexModel = getComplexModel;
		$rootScope.strToBytes = strToBytes;
		$rootScope.logicAnd = logicAnd;

		$rootScope.getScreenWidth = function() {
			var w = $window.innerWidth;
			if(w >= 1200)
				return ScreenWidth.LARGE;
			else if(w >= 992)
				return ScreenWidth.MEDIUM;
			else if(w >= 768)
				return ScreenWidth.SMALL;
			else
				return ScreenWidth.EXTRA_SMALL;
		};

		$rootScope.filterKeys = function(attr, query) {
			return $filter('filter')(this[attr], query);
		};

		$rootScope.assign = function(attr) {
			var $scope = this;
			return function(data) {
				$scope[attr] = data;
			};
		};

		sistemaService
			.getData()
			.then(function(v) {
				$rootScope.sistema = {
					versao: v
				};
			});

		$rootScope.hasPermission = function(permission) {
			var u = $rootScope.currentUser;
			if(u) {
				for(var i in u.idPermissoes) {
					if(u.idPermissoes[i] == permission) {
						return true;
					}
				}
				return false;
			}
		};

		$rootScope.loading = function(visible) {
			$rootScope.loader.visible = !!visible;
		};

		$rootScope.ask = function(message, buttons) {
			bootbox.dialog({
				message: message,
				closeButton: false,
				buttons: buttons
			});
		};

		$rootScope.confirm = function(title, message, fnYes, fnNo) {
			bootbox.dialog({
				title: title,
				message: message,
				closeButton: false,
				buttons: {
					'Sim': {
						className: 'btn-primary',
						callback: fnYes
					},
					'Não': {
						className: 'btn-warning',
						callback: fnNo
					}
				}
			});
		};

		$rootScope.alert = function(message, callback) {
			bootbox.alert(message, callback);
		};

		$state.getPermissions = function(stateOrName, context) {
			var state = $state.get(stateOrName, context);
			if(state.abstract)
				return null;
			if(state.params && state.params.permissions)
				return state.params.permissions;
			return $state.getPermissions('^', state);
		};

		$rootScope.$on('$stateChangeStart', function(e, toState) {
			if($rootScope.currentUser) {
				var statePermission = $state.getPermissions(toState);
				if(statePermission) {
					if(_.isArray(statePermission)) {
						for(var i in statePermission)
							if($rootScope.hasPermission(statePermission[i]))
								return;
					} else if($rootScope.hasPermission(statePermission))
						return;
					e.preventDefault();
					$state.go('error');
				}
			}
		});

	}])
	.filter('dateRange', function() {
		return function(list, from, to, attr) {
			if(from && attr) {
				var filtered = [];
				var obj;

				if(to){
					for(var i in list) {
						obj = list[i];
						complexField = getComplexValue(obj, attr);

						if(complexField >= from && complexField <= to) {
							filtered.push(obj);
						}
					}
				} else {
					for(var i in list) {
						obj = list[i];
						complexField = getComplexValue(obj, attr);

						if(complexField >= from) {
							filtered.push(obj);
						}
					}
				}
				return filtered;
			}
			return list;
		}
	})
	.filter('contains', function() {
		return function(list, items, ownerAttr, itemAttr) {
			if(items && items.length && ownerAttr) {
				var filtered = [];
				var obj;
				if(itemAttr) {
					out: for(var i in list) {
						obj = list[i];
						var complexField = getComplexValue(obj, ownerAttr);
						if(complexField && angular.isArray(complexField) && complexField.length) {
							inner: for(var j in items) {
								var ownerList = complexField;
								for(var k in ownerList) {
									if(ownerList[k][itemAttr] === items[j][itemAttr]) {
										continue inner;
									}
								}
								continue out;
							}
							filtered.push(obj);
						}
					}
				} else {
					out: for(var i in list) {
						obj = list[i];
						var complexField = getComplexValue(obj, ownerAttr);
						if(complexField && angular.isArray(complexField) && complexField.length) {
							for(var j in items) {
								if(complexField.indexOf(items[j]) !== -1) {
									filtered.push(obj);
									continue out;
								}
							}
						}
					}
				}
				return filtered;
			}
			return list;
		}
	})
	.filter('startFrom', function() {
		return function(input, start) {
			start = +start; //parse to int
			return input.slice(start);
		};
	}).filter('startsWith', function() {
		return function(items, prefix, itemProperty) {
		    return items.filter(function(item) {
		      var findIn = itemProperty ? item[itemProperty] : item;
		      return findIn.toString().indexOf(prefix) === 0;
		    });
		};
	}).filter('ignoreDiacritics', function() {
		return function(list, search, attrs) {
			if(search) {
				var filtered = [];
				var query = removeDiacritics(search.trim()).toLowerCase();
				var item;

				outer: for(var i in list) {
					item = list[i];
					for(var j in attrs) {
						if(removeDiacritics(item[attrs[j]].toString().trim()).toLowerCase().indexOf(query) !== -1) {
							filtered.push(item);
							continue outer;
						}
					}
				}

				return filtered;
			}
			return list;
		}
	})
	.constant('d3', window.d3)
	.constant('bootbox', window.bootbox)
	.constant('_', window._)
	.constant('FB', window.FB);
