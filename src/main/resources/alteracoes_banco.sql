-- 2015-09-08 Utilização keyword implícitas
ALTER TABLE audiencia_publica DROP FOREIGN KEY FK_9blchsge5mew71lvyjeg4s8fm;
ALTER TABLE audiencia_publica DROP id_palavra_chave;
--

-- 2015-10-28 Opinião de conteúdos não mais obrigatória
ALTER TABLE proposicao CHANGE COLUMN opiniao_conteudos opiniao_conteudos LONGTEXT NULL;
--

-- 2015-12-09 Abreviação escolaridade
update escolaridade set abreviatura = "NA" where id = 1;
update escolaridade set abreviatura = "FUN Incompleto" where id = 2;
update escolaridade set abreviatura = "FUN Completo" where id = 3;
update escolaridade set abreviatura = "MED Incompleto" where id = 4;
update escolaridade set abreviatura = "MED Completo" where id = 5;
update escolaridade set abreviatura = "SUP Incompleto" where id = 6;
update escolaridade set abreviatura = "SUP Completo" where id = 7;
update escolaridade set abreviatura = "POS Incompleto" where id = 8;
update escolaridade set abreviatura = "POS Completo" where id = 9;
--

-- 2015-12-09 Limites renda
update renda set min_renda = NULL, max_renda = 1787.77 where id = 1;
update renda set min_renda = 1787.78, max_renda = 2679.29 where id = 2;
update renda set min_renda = 2679.30, max_renda = 3572.43 where id = 3;
update renda set min_renda = 3572.44, max_renda = 4463.81 where id = 4;
update renda set min_renda = 4463.81, max_renda = NULL where id = 5;
--

-- 2015-12-09 Limites tempo domicílio
update tempo_domicilio set min_tempo_domicilio= NULL, max_tempo_domicilio = 1 where id = 1;
update tempo_domicilio set min_tempo_domicilio = 1, max_tempo_domicilio = 3  where id = 2;
update tempo_domicilio set min_tempo_domicilio = 3, max_tempo_domicilio = 5 where id = 3;
update tempo_domicilio set min_tempo_domicilio = 5, max_tempo_domicilio= NULL where id = 4;
--

-- 2015-12-09 Limites idade
update idade set min_idade = NULL, max_idade = 22 where id = 1;
update idade set min_idade = 23, max_idade = 45 where id = 2;
update idade set min_idade = 46, max_idade = 60 where id = 3;
update idade set min_idade = 61, max_idade = 70 where id = 4;
update idade set min_idade = 71, max_idade = NULL where id = 5;
--

-- 2016-06-02 Videos apontando para arquivos locais
alter table video drop column conteudo;
--

-- 2016-06-10 Tabela palavra_chave nao mais necessaria
drop table palavra_chave;
--
