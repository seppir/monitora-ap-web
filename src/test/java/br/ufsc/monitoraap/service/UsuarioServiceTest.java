package br.ufsc.monitoraap.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;

import br.ufsc.monitoraap.exception.impl.FacebookJaVinculadoException;
import br.ufsc.monitoraap.exception.impl.SenhaAtualIncorretaException;
import br.ufsc.monitoraap.exception.impl.SenhasDiferentesException;
import br.ufsc.monitoraap.exception.impl.UsuarioNaoExisteException;
import br.ufsc.monitoraap.model.Administrador;
import br.ufsc.monitoraap.model.Usuario;
import br.ufsc.monitoraap.repository.UsuarioRepository;
import br.ufsc.monitoraap.service.impl.UsuarioServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class UsuarioServiceTest {

	@Mock
	private static UsuarioRepository usuarioRepository;

	@Mock
	private static PermissaoService permissaoService;

	@Mock
	private static PasswordEncoder passwordEncoder;

	@InjectMocks
	private static UsuarioService usuarioService = new UsuarioServiceImpl(null);

	@Test(expected = SenhasDiferentesException.class)
	public void usuarioNaoPodeSerCadastradoComSenhasDiferentes() {
		final Usuario u = new Administrador();
		u.setSenha("abc");
		u.setSenha2("123");

		usuarioService.save(u);
	}

	@Test(expected = SenhaAtualIncorretaException.class)
	public void usuarioNaoPodeAlterarSenhaSemValidarSenhaAtual() {
		final String senhaSalvaNoBanco = "senhaSalvaNoBanco";
		final String senhaIncorreta = "senhaIncorreta";
		final String senhaTroca = "senhaTroca";

		final Usuario u = new Administrador();
		u.setId(1);
		u.setSenha(senhaSalvaNoBanco);
		u.setSenhaAtualTroca(senhaIncorreta);
		u.setSenhaTroca1(senhaTroca);
		u.setSenhaTroca2(senhaTroca);

		Mockito.when(passwordEncoder.matches(senhaIncorreta, senhaSalvaNoBanco)).thenReturn(false);

		usuarioService.save(u);
	}

	@Test(expected = SenhasDiferentesException.class)
	public void usuarioNaoPodeAlterarSenhaSeSenhasInformadasSaoDiferentes() {
		final String senhaSalvaNoBanco = "senhaSalvaNoBanco";
		final String senhaTroca1 = "senhaTroca1";
		final String senha2Troca2 = "senhaTroca2";

		final Usuario u = new Administrador();
		u.setId(1);
		u.setSenha(senhaSalvaNoBanco);
		u.setSenhaAtualTroca(senhaSalvaNoBanco);
		u.setSenhaTroca1(senhaTroca1);
		u.setSenhaTroca2(senha2Troca2);

		Mockito.when(passwordEncoder.matches(senhaSalvaNoBanco, senhaSalvaNoBanco)).thenReturn(true);

		usuarioService.save(u);
	}

	@Test(expected = UsuarioNaoExisteException.class)
	public void usuarioInexistenteNaoPodeVincularContaDoFacebook() {
		final Integer idExistente = 1;
		final Integer idInexistente = 2;
		final String facebookIdQualquer = "123456789123456789";

		Mockito.when(usuarioRepository.exists(idExistente)).thenReturn(true);
		Mockito.when(usuarioRepository.exists(idInexistente)).thenReturn(false);

		try {
			usuarioService.bindFacebookId(idExistente, facebookIdQualquer);
		} catch(final UsuarioNaoExisteException e) {
			Assert.fail();
		}
		usuarioService.bindFacebookId(idInexistente, facebookIdQualquer);
	}

	@Test(expected = FacebookJaVinculadoException.class)
	public void aMesmaContaFacebookNaoPodeEstarAutenticadoEmMaisDeUmUsuarioAoMesmoTempo() {
		final String facebookId = "123456789123456789";
		final int id1 = 1;
		final int id2 = 2;

		final Usuario u1 = new Administrador();
		u1.setId(id1);
		u1.setFacebookId(facebookId);

		final Usuario u2 = new Administrador();
		u2.setId(id2);
		u2.setFacebookId(facebookId);

		Mockito.when(usuarioRepository.exists(id1)).thenReturn(true);
		Mockito.when(usuarioRepository.exists(id2)).thenReturn(true);

		Mockito.when(usuarioRepository.existsFacebookId(facebookId)).thenReturn(false);
		try {
			usuarioService.bindFacebookId(id1, facebookId);
		} catch(final FacebookJaVinculadoException e) {
			Assert.fail();
		}

		Mockito.when(usuarioRepository.existsFacebookId(facebookId)).thenReturn(true);
		usuarioService.bindFacebookId(id2, facebookId);
	}

}
