package br.ufsc.monitoraap.service;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import br.ufsc.monitoraap.exception.impl.DescricaoTermoInvalidaException;
import br.ufsc.monitoraap.model.Termo;
import br.ufsc.monitoraap.model.TermoAudienciaPublica;
import br.ufsc.monitoraap.repository.TermoAudienciaPublicaRepository;
import br.ufsc.monitoraap.repository.TermoRepository;
import br.ufsc.monitoraap.service.impl.TermoServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class TermoServiceTest {

	@Mock
	private static TermoRepository termoRepository;

	@Mock
	private static TermoAudienciaPublicaRepository termoAudienciaPublicaRepository;

	private static TermoService termoService;

	@Before
	public void setUp() {
		termoService = new TermoServiceImpl(termoRepository, termoAudienciaPublicaRepository);
	}

	@Test(expected = DescricaoTermoInvalidaException.class)
	public void aDescricaoDoTermoNaoPodeSerUmaStringVazia() {
		final int idTermo = 1;
		final String descricaoNova = "";
		final String descricaoOriginal = "Descricao original";

		final Termo termo = new Termo();
		termo.setId(idTermo);
		termo.setDescricao(descricaoOriginal);

		Mockito.when(termoRepository.findOne(idTermo)).thenReturn(termo);

		termoService.setDescricao(idTermo, descricaoNova);
	}

	@Test(expected = DescricaoTermoInvalidaException.class)
	public void aDescricaoDoTermoNaoPodeSerApenasEspacos() {
		final int idTermo = 1;
		final String descricaoNova = " ";
		final String descricaoOriginal = "Descricao original";

		final Termo termo = new Termo();
		termo.setId(idTermo);
		termo.setDescricao(descricaoOriginal);

		Mockito.when(termoRepository.findOne(idTermo)).thenReturn(termo);

		termoService.setDescricao(idTermo, descricaoNova);
	}

	@Test(expected = DescricaoTermoInvalidaException.class)
	public void aDescricaoDoTermoNaoPodeSerNull() {
		final int idTermo = 1;
		final String descricaoNova = null;
		final String descricaoOriginal = "Descricao original";

		final Termo termo = new Termo();
		termo.setId(idTermo);
		termo.setDescricao(descricaoOriginal);

		Mockito.when(termoRepository.findOne(idTermo)).thenReturn(termo);

		termoService.setDescricao(idTermo, descricaoNova);
	}

	@Test(expected = DescricaoTermoInvalidaException.class)
	public void aDescricaoDoTermoAudienciaPublicaNaoPodeSerUmaStringVazia() {
		final int idTermo = 1;
		final int idAudienciaPublica = 1;
		final String descricaoNova = "";
		final String descricaoOriginal = "Descricao original";

		final Termo termo = new Termo();
		termo.setId(idTermo);
		termo.setDescricao(descricaoOriginal);

		final TermoAudienciaPublica.TermoAudienciaPublicaPK termoAudienciaPublicaPK = new TermoAudienciaPublica.TermoAudienciaPublicaPK();
		termoAudienciaPublicaPK.setIdAudienciaPublica(idAudienciaPublica);
		termoAudienciaPublicaPK.setTermo(termo);
		final TermoAudienciaPublica termoAudienciaPublica = new TermoAudienciaPublica();
		termoAudienciaPublica.setTermoAudienciaPublicaPk(termoAudienciaPublicaPK);

		Mockito.when(termoRepository.findOne(idTermo)).thenReturn(termo);
		Mockito.when(termoAudienciaPublicaRepository.findOne(termoAudienciaPublicaPK)).thenReturn(termoAudienciaPublica);

		termoService.setDescricaoAudienciaPublica(idAudienciaPublica, idTermo,
			descricaoNova);
	}

	@Test(expected = DescricaoTermoInvalidaException.class)
	public void aDescricaoDoTermoAudienciaPublicaNaoPodeSerApenasEspacos() {
		final int idTermo = 1;
		final int idAudienciaPublica = 1;
		final String descricaoNova = " ";
		final String descricaoOriginal = "Descricao original";

		final Termo termo = new Termo();
		termo.setId(idTermo);
		termo.setDescricao(descricaoOriginal);

		final TermoAudienciaPublica.TermoAudienciaPublicaPK termoAudienciaPublicaPK = new TermoAudienciaPublica.TermoAudienciaPublicaPK();
		termoAudienciaPublicaPK.setIdAudienciaPublica(idAudienciaPublica);
		termoAudienciaPublicaPK.setTermo(termo);
		final TermoAudienciaPublica termoAudienciaPublica = new TermoAudienciaPublica();
		termoAudienciaPublica.setTermoAudienciaPublicaPk(termoAudienciaPublicaPK);

		Mockito.when(termoRepository.findOne(idTermo)).thenReturn(termo);
		Mockito.when(termoAudienciaPublicaRepository.findOne(termoAudienciaPublicaPK)).thenReturn(termoAudienciaPublica);

		termoService.setDescricaoAudienciaPublica(idAudienciaPublica, idTermo,
			descricaoNova);
	}

	@Test(expected = DescricaoTermoInvalidaException.class)
	public void aDescricaoDoTermoAudienciaPublicaNaoPodeNull() {
		final int idTermo = 1;
		final int idAudienciaPublica = 1;
		final String descricaoNova = null;
		final String descricaoOriginal = "Descricao original";

		final Termo termo = new Termo();
		termo.setId(idTermo);
		termo.setDescricao(descricaoOriginal);

		final TermoAudienciaPublica.TermoAudienciaPublicaPK termoAudienciaPublicaPK = new TermoAudienciaPublica.TermoAudienciaPublicaPK();
		termoAudienciaPublicaPK.setIdAudienciaPublica(idAudienciaPublica);
		termoAudienciaPublicaPK.setTermo(termo);
		final TermoAudienciaPublica termoAudienciaPublica = new TermoAudienciaPublica();
		termoAudienciaPublica.setTermoAudienciaPublicaPk(termoAudienciaPublicaPK);

		Mockito.when(termoRepository.findOne(idTermo)).thenReturn(termo);
		Mockito.when(termoAudienciaPublicaRepository.findOne(termoAudienciaPublicaPK)).thenReturn(termoAudienciaPublica);

		termoService.setDescricaoAudienciaPublica(idAudienciaPublica, idTermo,
			descricaoNova);
	}

	@Test
	public void propagarValidacaoDeAdminParaTermoAudienciaPublicaAindaNaoValidados() {
		final int idTermo = 1;
		final Termo termo = new Termo();
		termo.setId(idTermo);

		final TermoAudienciaPublica termoAudienciaPublica1 = new TermoAudienciaPublica();
		final TermoAudienciaPublica.TermoAudienciaPublicaPK pk1 = new TermoAudienciaPublica.TermoAudienciaPublicaPK();
		pk1.setIdAudienciaPublica(1);
		pk1.setTermo(termo);
		termoAudienciaPublica1.setTermoAudienciaPublicaPk(pk1);

		final TermoAudienciaPublica termoAudienciaPublica2 = new TermoAudienciaPublica();
		final TermoAudienciaPublica.TermoAudienciaPublicaPK pk2 = new TermoAudienciaPublica.TermoAudienciaPublicaPK();
		pk2.setIdAudienciaPublica(2);
		pk2.setTermo(termo);
		termoAudienciaPublica2.setTermoAudienciaPublicaPk(pk2);
		termoAudienciaPublica2.setValidoParaCp(true);

		final TermoAudienciaPublica termoAudienciaPublica3 = new TermoAudienciaPublica();
		final TermoAudienciaPublica.TermoAudienciaPublicaPK pk3 = new TermoAudienciaPublica.TermoAudienciaPublicaPK();
		pk3.setIdAudienciaPublica(3);
		pk3.setTermo(termo);
		termoAudienciaPublica3.setTermoAudienciaPublicaPk(pk3);
		termoAudienciaPublica3.setValidoParaCp(false);

		Mockito.when(termoRepository.findOne(idTermo)).thenReturn(termo);
		final List<TermoAudienciaPublica> termosAp = Arrays.asList(termoAudienciaPublica1, termoAudienciaPublica2, termoAudienciaPublica3);
		Mockito.when(termoAudienciaPublicaRepository.findByTermo(idTermo)).thenReturn(termosAp);

		termoService.setValidade(idTermo, true);

		Assert.assertTrue(termo.getValido());
		Assert.assertTrue(termoAudienciaPublica1.getValidoParaCp());
		Assert.assertTrue(termoAudienciaPublica2.getValidoParaCp());
		Assert.assertFalse(termoAudienciaPublica3.getValidoParaCp());
	}

	@Test
	public void propagarValidacaoDeApParaTermoAindaNaoValidado() {
		final int idTermo = 1;
		final int idAudienciaPublica = 1;
		final Termo termo = new Termo();
		termo.setId(idTermo);

		final TermoAudienciaPublica termoAudienciaPublica = new TermoAudienciaPublica();
		final TermoAudienciaPublica.TermoAudienciaPublicaPK pk = new TermoAudienciaPublica.TermoAudienciaPublicaPK();
		pk.setIdAudienciaPublica(idAudienciaPublica);
		pk.setTermo(termo);
		termoAudienciaPublica.setTermoAudienciaPublicaPk(pk);

		Mockito.when(termoRepository.findOne(idTermo)).thenReturn(termo);
		Mockito.when(termoAudienciaPublicaRepository.findOne(pk)).thenReturn(termoAudienciaPublica);

		termoService.setValidadeAudienciaPublica(idAudienciaPublica, idTermo, true);

		Assert.assertTrue(termoAudienciaPublica.getValidoParaCp());
		Assert.assertTrue(termo.getValido());
	}

}
