package br.ufsc.monitoraap.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import br.ufsc.monitoraap.exception.impl.MaximoProposicoesPorUsuarioExcedidoException;
import br.ufsc.monitoraap.exception.impl.UsuarioNaoIdentificadoException;
import br.ufsc.monitoraap.model.AudienciaPublica;
import br.ufsc.monitoraap.model.Proponente;
import br.ufsc.monitoraap.model.Proposicao;
import br.ufsc.monitoraap.repository.ProposicaoRepository;
import br.ufsc.monitoraap.service.impl.ProposicaoServiceImpl;
import br.ufsc.monitoraap.util.persistence.aftercommit.AfterCommitExecutor;

@RunWith(MockitoJUnitRunner.class)
public class ProposicaoServiceTest {

	@Mock
	private static TematicaService tematicaService;

	@Mock
	private static AudienciaPublicaService audienciaPublicaService;

	@Mock
	private static ProposicaoRepository proposicaoRepository;

	@Mock
	private static AfterCommitExecutor afterCommitExecutor;

	@Mock
	private static SolrService solrService;

	@InjectMocks
	private static ProposicaoService proposicaoService = new ProposicaoServiceImpl(null);

	@Test(expected = UsuarioNaoIdentificadoException.class)
	public void usuarioSemCpfNaoPodeCadastrarProposicaoEmApQueObrigaUsuarioTerCpf() {
		final AudienciaPublica audienciaPublica = new AudienciaPublica();
		audienciaPublica.setId(1);
		audienciaPublica.setUsuariosIdentificados(true);

		Mockito.when(audienciaPublicaService.findOne(1)).thenReturn(audienciaPublica);

		final Proposicao proposicao = new Proposicao();
		proposicao.setIdAudienciaPublica(audienciaPublica.getId());
		final Proponente p = new Proponente();
		p.setCpf(null);
		proposicao.setProponente(p);

		proposicaoService.save(proposicao);
	}

	@Test
	public void usuarioPodeCadastrarProposicaoSeAindaNaoCadastrouNenhuma() {
		final AudienciaPublica a = new AudienciaPublica();
		a.setId(1);
		a.setMaxProposicoesUsuario(3);

		Mockito.when(audienciaPublicaService.findOne(1)).thenReturn(a);

		final Proponente p = new Proponente();
		p.setId(1);

		final Proposicao pr = new Proposicao();
		pr.setIdAudienciaPublica(a.getId());
		pr.setProponente(p);
		pr.setTexto("Teste");

		Mockito.when(proposicaoRepository.countByUserAndAp(p.getId(), a.getId())).thenReturn(0);

		try {
			proposicaoService.save(pr);
		} catch(final MaximoProposicoesPorUsuarioExcedidoException e) {
			Assert.fail();
		}
	}

	@Test
	public void usuarioPodeCadastrarProposicaoSeJahCadastrouMasNaoExcedeuLimite() {
		final AudienciaPublica a = new AudienciaPublica();
		a.setId(1);
		a.setMaxProposicoesUsuario(3);

		Mockito.when(audienciaPublicaService.findOne(1)).thenReturn(a);

		final Proponente p = new Proponente();
		p.setId(1);

		final Proposicao pr = new Proposicao();
		pr.setIdAudienciaPublica(a.getId());
		pr.setProponente(p);
		pr.setTexto("Teste 1");

		Mockito.when(proposicaoRepository.countByUserAndAp(p.getId(), a.getId())).thenReturn(2);

		try {
			proposicaoService.save(pr);
		} catch(final MaximoProposicoesPorUsuarioExcedidoException e) {
			Assert.fail();
		}
	}

	@Test(expected = MaximoProposicoesPorUsuarioExcedidoException.class)
	public void usuarioNaoPodeCadastrarProposicaoSeExcedeuLimite() {
		final AudienciaPublica a = new AudienciaPublica();
		a.setId(1);
		a.setMaxProposicoesUsuario(3);

		Mockito.when(audienciaPublicaService.findOne(1)).thenReturn(a);

		final Proponente p = new Proponente();
		p.setId(1);

		final Proposicao pr = new Proposicao();
		pr.setIdAudienciaPublica(a.getId());
		pr.setProponente(p);
		pr.setTexto("Teste 1");

		Mockito.when(proposicaoRepository.countByUserAndAp(p.getId(), a.getId())).thenReturn(3);

		proposicaoService.save(pr);
	}

}
