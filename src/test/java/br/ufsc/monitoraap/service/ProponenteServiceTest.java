package br.ufsc.monitoraap.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import br.ufsc.monitoraap.exception.impl.CpfInvalidoException;
import br.ufsc.monitoraap.model.Proponente;
import br.ufsc.monitoraap.repository.ProponenteRepository;
import br.ufsc.monitoraap.service.impl.ProponenteServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class ProponenteServiceTest {

	private static final String CPF_VALIDO = "422.341.721-52";
	private static final String CPF_INVALIDO = "425.331.726-53";

	@Mock
	private static ProponenteRepository proponenteRepository;

	@Mock
	private static PermissaoService permissaoService;

	@InjectMocks
	private static ProponenteService proponenteService = new ProponenteServiceImpl(null);

	@Test
	public void usuarioNaoPrecisaTerCpf() {
		final Proponente p = new Proponente();

		try {
			proponenteService.save(p);
		} catch(final CpfInvalidoException e) {
			Assert.fail();
		}
	}

	@Test(expected = CpfInvalidoException.class)
	public void usuarioPrecisaTerCpfValido() {
		final Proponente p = new Proponente();
		p.setCpf(CPF_INVALIDO);

		proponenteService.save(p);
	}

	@Test
	public void usuarioPodeSalvarComCpfValido() {
		final Proponente p = new Proponente();
		p.setCpf(CPF_VALIDO);

		try {
			proponenteService.save(p);
		} catch(final CpfInvalidoException e) {
			Assert.fail();
		}
	}

}
