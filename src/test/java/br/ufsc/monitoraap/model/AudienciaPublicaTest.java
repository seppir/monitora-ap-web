package br.ufsc.monitoraap.model;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import br.ufsc.monitoraap.enumeration.Status;
import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.class)
public class AudienciaPublicaTest {

	@Test
	public void auditoriaCancelada() {
		final Calendar cal = Calendar.getInstance();

		cal.add(Calendar.MONTH, -1);

		final Date dataInicio = cal.getTime();
		cal.add(Calendar.MONTH, 2);

		final Date dataEncerramento = cal.getTime();

		final AudienciaPublica ap = new AudienciaPublica();
		ap.setDataInicio(dataInicio);
		ap.setDataEncerramento(dataEncerramento);
		ap.setCancelada(true);

		Assert.assertEquals(ap.getStatus(), Status.CANCELADA);
	}

	@Test
	public void auditoriaAgendada() {
		final Calendar cal = Calendar.getInstance();

		cal.add(Calendar.MONTH, 1);

		final Date dataInicio = cal.getTime();
		cal.add(Calendar.MONTH, 2);

		final Date dataEncerramento = cal.getTime();

		final AudienciaPublica ap = new AudienciaPublica();
		ap.setDataInicio(dataInicio);
		ap.setDataEncerramento(dataEncerramento);
		ap.setCancelada(false);

		Assert.assertEquals(ap.getStatus(), Status.AGENDADA);
	}

	@Test
	public void auditoriaEmAndamento() {
		final Calendar cal = Calendar.getInstance();

		cal.add(Calendar.MONTH, -1);

		final Date dataInicio = cal.getTime();
		cal.add(Calendar.MONTH, 2);

		final Date dataEncerramento = cal.getTime();

		final AudienciaPublica ap = new AudienciaPublica();
		ap.setDataInicio(dataInicio);
		ap.setDataEncerramento(dataEncerramento);
		ap.setCancelada(false);

		Assert.assertEquals(ap.getStatus(), Status.EM_ANDAMENTO);
	}

	@Test
	public void auditoriaEncerrada() {
		final Calendar cal = Calendar.getInstance();

		cal.add(Calendar.MONTH, -2);

		final Date dataInicio = cal.getTime();
		cal.add(Calendar.MONTH, -1);

		final Date dataEncerramento = cal.getTime();

		final AudienciaPublica ap = new AudienciaPublica();
		ap.setDataInicio(dataInicio);
		ap.setDataEncerramento(dataEncerramento);
		ap.setCancelada(false);

		Assert.assertEquals(ap.getStatus(), Status.ENCERRADA);
	}

}
